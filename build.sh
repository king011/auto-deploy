#!/bin/bash
#Program:
#       golang 自動編譯 腳本
#History:
#       2018-03-28 king first release
#		2018-08-30 tag commit date
#Email:
#       zuiwuchang@gmail.com

DirRoot=`cd $(dirname $BASH_SOURCE) && pwd`

# 定義的 各種 輔助 函數
MkDir(){
	mkdir -p "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}
MkOrClear(){
	if test -d "$1";then
		declare path="$1"
		path+="/*"
		rm "$path" -rf
		if [ "$?" != 0 ] ;then
			exit 1
		fi
	else
		MkDir $1
	fi
}
NewFile(){
	echo "$2" > "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}
WriteFile(){
	echo "$2" >> "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}


CreateGoVersion(){
	MkDir version
	filename="version/version.go"
	package="version"

	# 返回 git 信息 時間
	tag=`git describe`
	if [ "$tag" == '' ];then
		tag="[unknown tag]"
	fi

	commit=`git rev-parse HEAD`
	if [ "$commit" == '' ];then
		commit="[unknow commit]"
	fi
	
	date=`date +'%Y-%m-%d %H:%M:%S'`

	# 打印 信息
	echo ${tag} $commit
	echo $date


	# 自動 創建 go 代碼
	NewFile $filename	"package $package"
	WriteFile $filename	''
	WriteFile $filename	'// Tag git tag'
	WriteFile $filename	"const Tag = \`$tag\`"
	WriteFile $filename	'// Commit git commit'
	WriteFile $filename	"const Commit = \`$commit\`"
	WriteFile $filename	'// Date build datetime'
	WriteFile $filename	"const Date = \`$date\`"
}


# 顯示幫助信息
function ShowHelp(){
	echo "help                : show help"
    echo "l/linux   [7/7z]    : build for linux"
    echo "w/windows [7/7z]    : build for windows" 
	echo "g/grpc              : build proto to grpc" 
}
check(){
	if [ "$1" != 0 ] ;then
		exit $1
	fi
}
function goGRPC(){
	dir=$(cd $(dirname $BASH_SOURCE)/../../../ && pwd)
	if [[ "$OSTYPE" == "msys" ]]; then
		output=${dir:1:1}:${dir:2}/
	else
		output=$dir/
	fi

	"$DirRoot/grpc.sh" go pb/ $output
}
case $1 in
	g|grpc)
		goGRPC
	;;

	l|linux)
		export GOOS=linux
		CreateGoVersion

		go build -o bin/auto-deploy-server -ldflags "-s -w" gitlab.com/king011/auto-deploy/server
		check $?
		go build -o bin/auto-deploy-service -ldflags "-s -w" gitlab.com/king011/auto-deploy/service
		check $?

		if [[ $2 == 7 || $2 == 7z ]]; then
			dst=linux.amd64.7z
			if [[ $GOARCH == 386 ]];then
				dst=linux.386.7z
			fi
			if [ -f "$DirRoot/bin/$dst" ]; then
				rm "$DirRoot/bin/$dst"
			fi

			cd "$DirRoot/bin" && 7z a "$dst" \
				auto-deploy-server auto-deploy-service \
				etc	\
				completion-auto-deploy.sh path-auto-deploy.sh \
				auto-deploy-server.service auto-deploy-service.service
		fi
	;;

	w|windows)
		export GOOS=windows
		CreateGoVersion

		go build -o bin/auto-deploy-server.exe -ldflags "-s -w" gitlab.com/king011/auto-deploy/server
		check $?
		go build -o bin/auto-deploy-service.exe -ldflags "-s -w" gitlab.com/king011/auto-deploy/service
		check $?

		if [[ $2 == 7 || $2 == 7z ]]; then

			dst=windows.amd64.7z
			if [[ $GOARCH == 386 ]];then
				dst=windows.386.7z
			fi
			if [ -f "$DirRoot/bin/$dst" ]; then
				rm "$DirRoot/bin/$dst"
			fi

			cd "$DirRoot/bin" && 7z a "$dst" \
				auto-deploy-server.exe auto-deploy-service.exe \
				etc	\
				auto-deploy-server.service auto-deploy-service.service \
				completion-auto-deploy.sh path-auto-deploy.sh \
				windows
		fi
	;;

	*)
		ShowHelp
		exit 0
	;;
esac
