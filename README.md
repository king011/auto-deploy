# auto deploy

一個兼容linux win32的軟體套件自動部署平臺

* server 套件服務器 提供了 套件管理 下載 等功能
* service 客戶端服務 和 server 通信 負責 客戶端的 套件 安裝/更新/運行/停止



# 快速導航

* 使用說明
   * [套件倉庫](#user-content-套件倉庫)
      1. [創建一個 套件倉庫](#user-content-創建一個-套件倉庫)
	  1. [管理 套件](#user-content-管理-套件)
	  1. [管理 版本](#user-content-管理-版本)
   * [套件管理服務](#user-content-套件管理服務)
      1. [運行套件管理服務](#user-content-運行套件管理服務)
      1. [管理套件](#user-content-管理套件)
* 編譯與安裝
	* [安裝](#user-content-安裝)
	* 編譯
	   1. [linux](#user-content-build-on-linux)
	   1. [windows](#user-content-build-on-win32)


# <span id="server">套件倉庫</span>

* 套件倉庫 是一個 公共的 套件服務器 上面存儲了 各種的 套件 以及 套件的 不同版本

* 客戶可以 提供 使用 套件管理服務 從 套件倉庫中 安裝/下載/更新 套件

* 套件倉庫的 套件 可以設定權限 是否允許指定用戶 下載套件 (默認 允許 任何人下載)

* auto-deploy-server 是套件倉庫的 主服務 以及 管理工具

## <span id="server-run">創建一個 套件倉庫</span>

`auto-deploy-server daemon` 指令 將運行一個 倉庫 -c 指定其配置 檔案 

* 默認的 配置檔案是 auto-deploy-server所在檔案夾 + /etc/auto-deploy/server.jsonnet

```sh
$ ./auto-deploy-server daemon -h
run server as daemon

Usage:
  server daemon [flags]

Flags:
  -c, --configure string   configure file (default "/etc/auto-deploy/server.jsonnet")
  -h, --help               help for daemon
```

下面是一個 linux下的 配置
```jsonnet

// 定義 根目錄位置
local Root = "";
//local ShowSQL = true;
local ShowSQL = false;
{
	// 服務器 配置
	Server:{
		// 服務器 工作地址
		LAddr:"127.0.0.1:60000",
		// ssl證書 位置 (同時設置了 Cert Key 才啓用 tls)
		//Cert:Root + "etc/service.crt",
		// ssl證書 key 位置
		//Key:Root + "etc/service.key",
		// 服務器 管理密碼
		Password:"12345678",
	},
	// 套件 倉庫 位置
	Store:Root + "var/store",
	// 數據庫設置
	DB:{
		// 連接字符串
		Str:"AutoDeploy:12345678@/AutoDeploy?charset=utf8",

		// 是否要顯示sql指令
		Show:ShowSQL,

		// ping 數據庫 間隔 單位 分鐘
		Ping:60,

		// 連接池 最大連接 < 1 無限制
		MaxOpen:1000,
		// 連接池 最大 空閒 連接 < 1 不允許空閒連接
		MaxIdle:10,

		// 如果為0 使用 默認值 如果小於0 將不啟用緩存
		Cache:1000,
	},
	// Logger 日誌 配置
	Logger:{
		// 日誌 http 如果爲空 則不啓動 http
		//HTTP:"localhost:20000",
		// 日誌 檔案名
		//Filename:Root + "var/log/server.log",
		// 單個日誌檔案 大小上限 MB
		MaxSize:    100, 
		// 保存 多少個 日誌 檔案
		MaxBackups: 3,
		// 保存 多少天內的 日誌
		MaxAge:     28,
		// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
		Level :"debug",
	},
}
```

> * Store 中 會 保存 上傳的 套件 安裝數據
>
> * Server 配置了 服務管理器 如何 和 套件倉庫 通信
>
> * Server.Password 和 服務管理器 無關 當使用 其它 server 指令時 通過此密碼 驗證 是否 可以操作 當前 倉庫

## <span id="server-admin">管理 套件</span>
運行了 倉庫後 就可以在 倉庫中 創建/刪除 套件 爲套件設置下載權限

`auto-deploy-server package` 指令 會向 一個 倉庫 下達 套件 相關的 指令

```sh
$ ./auto-deploy-server package -h
package management
  # create a new package named npkg
  server package -i -n npkg -d "init example"

  # update package description
  server package -i -u npkg -d "update example"

  # search package name like ?
  server package -s -n %pkg%"

  # remove package 
  server package -r -n npkg

  # disable everyone download package npkg
  server package -n npkg --delete *
  server package -n npkg --add "-"

Usage:
  server package [flags]

Flags:
      --add string           add a key allow download package key
  -c, --configure string     configure file (default "/etc/auto-deploy/server.jsonnet")
      --delete string        delete all match key
  -d, --description string   if (init) package description else if (add) key description
  -g, --get                  get package info
  -h, --help                 help for package
  -i, --init                 init a new package
  -n, --name string          package name
  -r, --remove               remove a package
  -s, --search               search package name like ?
  -u, --update               update package description
```

# <span id="server-version">管理 版本</span>

套件 至少需要 存在一個 版本 套件管理服務 才可以將其 下載到 客戶端

`auto-deploy-server version` 指令 會向 一個 倉庫 下達 版本管理 相關的 指令

```sh
$ ./auto-deploy-server version -h
package version management
  # create a new package version on current directory
  version -i

  # push new version to server
  version -p

  # update version info to server
  version -u

  # remove version 
  version -r vX.X.X --package pkg

  # list package all version
  version -l pkg

Usage:
  server version [flags]

Flags:
  -c, --configure string   configure file (default "/etc/auto-deploy/server.jsonnet")
  -h, --help               help for version
  -i, --init               create a new package version on current directory
  -l, --list string        list package all version
      --package string     package name olny use on remove
  -p, --push               push new version to server
  -r, --remove string      remove a package version
  -u, --update             update version info to server
```

# <span id="service">套件管理服務</span>

* 套件管理服務 是運行在客戶端上的一個 服務程式 通過她 可以 完成 從 套件倉庫 下載安裝 套件的 功能
* 套件管理服務 提供了 套件的 自動 更新功能
* 套件管理服務 提供了 套件的 自動運行 和 包裝套件持續運行的 功能

## <span id="service-run">運行套件管理服務</span>

`auto-deploy-service daemon` 運行一個 管理服務 類似 server 其接收一個 -c 參數 指定 配置檔案

```jsonnet
// 定義一些時間常量
local Millisecond = 1000000;
local Second = 1000 * Millisecond;
local Minute = 60 * Second;
local Hour = 60 * Minute;
local Day = 24 * Hour;
// 定義 根目錄位置
local Root = "";
{
	// 服務配置
	Service:{
		// 服務器 工作地址
		LAddr:"127.0.0.1:61000",
		// ssl證書 位置 (同時設置了 Cert Key 才啓用 tls)
		//Cert:Root + "etc/service.crt",
		// ssl證書 key 位置
		//Key:Root + "etc/service.key",
		// 服務 管理密碼
		Password:"12345678",
	},
	// 套件 服務器 配置
	Server:{
		// 服務器 工作地址
		Addr:"xsd.king011.com:60000",
		// 是否使用 ssl 加密
		SSL:false,
	},
	// 套件 設置
	Apps:{
		// 倉庫 位置
		//Store:Root + "var/app",
		// 停止進程 超時時間
		Kill:Minute,
		// 套件 自動更新 時間 如果爲空則 不自動更新
		// #分鐘		小時		號數		月份		周星期
		// #[0,59]		[0,23]		[1,31]		[1~12]		[0,7] 0==7==星期天
		Update:[
			"* 2 * * *",    // 每天 晚上 2點 更新
			"* 2 * * 0",    // 每週 星期天 晚上 2點 更新
			"* 2 1 * *",    // 每月 1 號 晚上 2點 更新

			// 無效的定義會被直接 忽略
			"",
		]
	},
	// Logger 日誌 配置
	Logger:{
		// 日誌 http 如果爲空 則不啓動 http
		//HTTP:"localhost:20001",
		// 日誌 檔案名 如果爲空 則輸出到 控制檯
		//Filename:Root + "var/log/service.log",
		// 單個日誌檔案 大小上限 MB
		MaxSize:    100, 
		// 保存 多少個 日誌 檔案
		MaxBackups: 3,
		// 保存 多少天內的 日誌
		MaxAge:     28,
		// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
		Level :"debug",
	},
}
```

## <span id="service-admin">管理套件</span>

套件 管理 命令 模仿 了 ubuntu 的 apt 操作基本類似

```sh
 ./auto-deploy-service -h
auto-deploy service

Usage:
  service [flags]
  service [command]

Available Commands:
  daemon      run service as a daemon
  download    download packages
  exit        notification service exit
  help        Help about any command
  install     install packages
  keys        return keys
  list        list all installed package
  merge       test merge script
  mode        change application run mode or update mode
  remove      remove packages
  search      search package
  show        display detailed information about a package
  start       start package
  stop        stop package
  update      update package

Flags:
  -h, --help      help for service
  -v, --version   show version

Use "service [command] --help" for more information about a command.
```

```sh
# 查找/安裝/刪除/更新 套件
auto-deploy-service search gcc cmake
auto-deploy-service install gcc cmake
auto-deploy-service remove gcc cmake
auto-deploy-service update gcc cmake

# 停止/運行 套件
auto-deploy-service start gcc
auto-deploy-service stop gcc

# 現在 已安裝 套件
auto-deploy-service list
auto-deploy-service show gcc cmake

# 改變 套件 運行模式/更新模式
auto-deploy-service mode -r manual -p gcc
auto-deploy-service mode -u auto -p gcc

# 通知 管理服務 停止所有套件 並退出
auto-deploy-service exit
```


## auto-deploy-service key
`auto-deploy-service key` 指令會 返回網卡指紋 可以在 倉庫中設置 key 來 只允許 指定客戶 下載套件


# <span id="install">安裝</span>
1. 下載編譯好的/或參照說明自行編譯 auto-deploy-server 和 auto-deploy-service
2. 對於 service 需要 安裝好 mariadb 或 mysql
3. auto-deploy-service daemon 運行 倉庫
4. auto-deploy-server daemon 在客戶端 運行 管理服務

# <span id="build-linux">build on linux</span>

1. 安裝好 grpc 環境
1. 下載 源碼 `go get https://gitlab.com/king011/auto-deploy.git`
1. 生成 grpc 代碼 `./build-pb.sh`
1. 編譯 `./build-go.sh linux`

# <span id="build-win32">build on win32</span>
1. 安裝好 msys2 gcc 環境
1. 安裝好 grpc 環境
1. 下載 源碼 `go get https://gitlab.com/king011/auto-deploy.git`
1. 生成 grpc 代碼 `./build-pb.sh`
1. 編譯 在msys2中執行 `./build-go.sh windows`

## 爲什麼 msys2-pb.sh 不能和 build-pb.sh 一樣直接 生成 grpc 代碼

google提供的 grpc工具 在 msys2下 不知道爲何 生成的 grpc代碼 有問題 必須要在 win32 的提示符下才能生成 正確代碼 

至於爲什麼 不手寫 msys2-pb.bat 查找 grpc 定義文件 理由很簡單 我不會 也不用 更不想學 win32的狗屎命令 **如果不是要用到 公司的win32項目 這個程式 我都不會想支持 win32**


# auto-deploy.json

此檔案 定義了 套件的 版本和 描述信息

# process.js

此 腳本 需要 導出一個 exports.Service 數組 數組的每個元素 定義了如何 啓動和結束一個進程 如下示例

```js

// this scripts define package's process

// variable __dirname is package installed path
// named __dirname for cosplay nodejs, but environment not nodejs, it's duktape
// https://duktape.org/api.html
var dir = __dirname;

// exports cosplay nodejs
// export Service array, it's process group.
exports.Service = [
	// every element is a process
	{
		// how to start process
		Start: [dir + "/main.js"],
		//Start: [dir + "/main.js" , "daemon"],
		// how to stop process, if invalid will kill process
		Stop: undefined,
		//Stop: [dir + "/main.js" , "kill"],
		// work directory, if invalid will use parent's work directory.
		Dir: dir,
	},
]
```

* __dirname 變量 是 套件的 安裝位置 你需要使用 這個變量 找到你自己的 可執行檔案
* __service 變量 在以 服務 形式 運行時 爲 true 否則爲 false
* Start 是必須的 它是一個 字符串 數組 第一個元素 是可執行檔 路徑 其它是 傳入的 參數
* Stop 是可選的 如果 它是 一個 字符串 數組 則執行此程序 結束進程 否則 強制關閉進程
* Dir 的 Start 進程的 工作目錄 通常你應該指定爲 __dirname

> __dirname 和 exports.Service 名字是 爲了 符合 nodejs 的習慣 但js並不運行在 nodejs下 而是運行在 duktape 的golang 環境中
>
> nodejs 對於此項目來說 太大 並且 不容易集成到 golang中 以及方便的 跨平臺
>

# install.js merge.js

## install.js

install.js 是可選的 安裝腳本 需要 導出一個 exports.Install 函數

* 當 安裝一個 套件時 會自動 調用 此函數
* 如果 此函數 throw 異常 則會導致 安裝失敗
* install.js 中 有一個 全局的 utils 對象 提供了一些 輔助 升級的 函數

## merge.js
merge.js 是可選的 升級腳本 需要 導出一個 exports.Merge 函數

* 當 升級一個 套件時 會自動 調用 此函數
* 如果 此函數 throw 異常 則會導致 升級失敗
* merge.js 中 有一個 全局的 utils 對象 提供了一些 輔助 升級的 函數

utils/test/dst 中 包含數個 *.js 是一些 測試示例

## utils
install.js 和 merge.js 通過 require("auto-deploy-utils") 可以 加載 一個 系統 模塊 其提供了一些 功能 方便 自定義 安裝/升級 套

```
npm i --save auto-deploy-utils
```

```
var utils = require(auto-deploy-utils).Utils
```

> 爲了兼容 舊版本 require("auto-deploy-utils") 模塊 可以 通過 全局變量 utils 直接訪問
>
> 全局變量 utils 只是爲了 兼容 已經部署的 項目 以後可能會被刪除 新項目 請使用 require("auto-deploy-utils") 

```txt
// if ture, test *.js
prop DEV:bool

// windows linux darwin freebsd
prop OS:string
// 386 amd64 arm
prop ARCH:string

// 返回 檔案 是否 存在
function IsFileExist(filename:string):boolean
	throw e

// 返回 檔案夾 是否 存在
function IsDirExist(filename:string):boolean
	throw e

// IsFileExist | IsDirExist
function IsExist(filename:string):boolean
	throw e

// 返回 filename 的最短 名稱
function Clean(filename:string):string
	throw e

// 返回 是否是絕對路徑
function IsAbs(filename:string):boolean
	throw e

// 返回 絕對路徑 Abs 會在結果前 調用 Clean
function Abs(filename:string):string
	throw e


// 返回 路徑的 檔案夾名稱
function Dir(filename:string):string
	throw e


// 返回 路徑的 最後一個元素
function Base(filename:string):string
	throw e

// 返回 檔案 後綴名 .txt .sh .so
function Ext(filename:string):string
	throw e

// 創建 檔案夾
function Mkdir(dirname:string,mode?:uint32,mkall:?boolean)
	throw e

// 複製 檔案/檔案夾
// 如果 all 爲ture 則 遞歸複製 檔案夾
function Copy(dst:string,src:string,all:?boolean)
	throw e

// 刪除 檔案/檔案夾
// 如果 all 爲ture 則 遞歸刪除 檔案夾
function Remove(name:string,all:?boolean)
	throw e

// 加載 檔案 並轉化爲 字符串
function LoadFile(filename:string):string
	throw e

// 保存 檔案
function SaveFile(filename:string,data:string,mode?:uint32)
	throw e

// 將 jsonnet 字符串 編譯爲 json 字符串
function CompileJSONNET(filename:string):string
	throw e

// 將 xml 字符串 編譯爲 json 字符串
function CompileXML(filename:string):string
	throw e

// 將 ini 字符串 編譯爲 json 字符串
function CompileConf(filename:string):string
	throw e

// 執行 指定進程並 等待其運行結束
function Exec(exec:string,args...:string)
	throw e

// 將 xml 字符串 轉爲 js object
function ParseXML(src:string):object
	throw e

// 將 ini 字符串 轉爲 js object
// Section = {
// 	Comment:XXX:stirng,
// 	Item:{
// 		Keyname...:{
// 			Val:XXX:stirng,
// 			Comment:XXX:stirng,
// 		},
// 		...
// 	}
// }
// object = {
// 	main:Section,
// 	section:{
// 		sessionName...:Section,
// 		...
// 	}
// }
function ParseConf(src:string):object
	throw e
```