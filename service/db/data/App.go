package data

import (
	"bytes"
	"encoding/gob"
	grpc_data "gitlab.com/king011/auto-deploy/protocol/data"
	"strconv"
)

const (
	// AppName .
	AppName = "apps"
)

// RunMode 運行模式
type RunMode int

const (
	// RunAuto 自動運行
	RunAuto = iota

	// RunKeep 保持一直運行
	RunKeep

	// RunManual 手動運行
	RunManual
)

func (i RunMode) String() (str string) {
	switch i {
	case RunAuto:
		str = "Auto"
	case RunKeep:
		str = "Keep"
	case RunManual:
		str = "Manual"
	default:
		str = strconv.Itoa(int(i))
	}
	return
}

// UpdateMode 更新模式
type UpdateMode int

const (
	// UpdateAuto 自動更新
	UpdateAuto = iota

	// UpdateDev 自動更新 並且 安裝 開發版本
	UpdateDev

	// UpdateManual 禁用 更新
	UpdateManual
)

func (i UpdateMode) String() (str string) {
	switch i {
	case UpdateAuto:
		str = "Auto"
	case UpdateDev:
		str = "Dev"
	case UpdateManual:
		str = "Manual"
	default:
		str = strconv.Itoa(int(i))
	}
	return
}

// RunStatus 運行狀態
type RunStatus int

const (
	// None 沒有運行過
	None = iota

	// Run 開始 運行
	Run
	// Running 運行中
	Running

	// Stoping 開始 停止
	Stoping
	// Exit 已經運行 並 正常退出
	Exit
	// Abort 已經運行 但 發生了錯誤 導致異常退出
	Abort
	// Kill 已經關閉
	Kill
)

func (i RunStatus) String() (str string) {
	switch i {
	case None:
		str = "None"
	case Run:
		str = "Run"
	case Running:
		str = "Running"
	case Stoping:
		str = "Stoping"
	case Exit:
		str = "Exit"
	case Abort:
		str = "Abort"
	case Kill:
		str = "Kill"
	default:
		str = strconv.Itoa(int(i))
	}
	return
}

// InstallStatus 安裝狀態
type InstallStatus int

const (
	// AppNone 未安裝
	AppNone = iota
	// AppInstalling 正在安裝中
	AppInstalling
	// AppInstallation 已經安裝
	AppInstallation
	// AppUpdating 正在進行 更新
	AppUpdating
	// AppUpdate 正在 完成 更新 最後的 安裝操作
	AppUpdate
	// AppRemoving 正在進行 移除
	AppRemoving
	// AppRemoved 數據 已移除
	AppRemoved
	// AppDamaged 已經損毀
	AppDamaged
)

func (i InstallStatus) String() (str string) {
	switch i {
	case AppNone:
		str = "None"
	case AppInstalling:
		str = "Installing"
	case AppInstallation:
		str = "Installation"
	case AppUpdating:
		str = "Updating"
	case AppUpdate:
		str = "Update"
	case AppRemoving:
		str = "Removing"
	case AppRemoved:
		str = "Removed"
	case AppDamaged:
		str = "Damaged"
	default:
		str = strconv.Itoa(int(i))
	}
	return
}

// App 一個 安裝的 套件
type App struct {
	// 套件 名稱
	Name string

	// 當前 版本
	Version string

	// 套件 功能 描述
	Description string

	// 是否爲 開發者版本
	Dev bool

	// 運行模式
	RunMode RunMode
	// 更新模式
	UpdateMode UpdateMode

	// 運行狀態
	Run RunStatus

	// 安裝狀態
	Install InstallStatus

	// 鎖定 套件 狀態
	Lock bool
}

// ConvertToBytes .
func (d *App) ConvertToBytes() (b []byte, e error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	e = enc.Encode(d)
	if e != nil {
		return
	}

	b = buf.Bytes()
	return
}

// FromBytes .
func (d *App) FromBytes(b []byte) (e error) {
	buf := bytes.NewBuffer(b)
	dec := gob.NewDecoder(buf)
	e = dec.Decode(d)
	return
}

// ConvertToPB .
func (d *App) ConvertToPB() (clone *grpc_data.App) {
	clone = &grpc_data.App{
		Package:     d.Name,
		Version:     d.Version,
		Description: d.Description,
		Dev:         d.Dev,

		RunMode:    int32(d.RunMode),
		UpdateMode: int32(d.UpdateMode),
		Run:        int32(d.Run),
		Install:    int32(d.Install),
	}
	return
}

// FromPB .
func (d *App) FromPB(clone *grpc_data.App) {

	d.Name = clone.Package
	d.Version = clone.Version
	d.Description = clone.Description
	d.Dev = clone.Dev
	d.RunMode = RunMode(clone.RunMode)
	d.UpdateMode = UpdateMode(clone.UpdateMode)
	d.Run = RunStatus(clone.Run)
	d.Install = InstallStatus(clone.Install)

	return
}
