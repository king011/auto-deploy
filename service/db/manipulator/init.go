package manipulator

import (
	"github.com/boltdb/bolt"
	"gitlab.com/king011/auto-deploy/service/configure"
	"gitlab.com/king011/auto-deploy/service/db/data"
	"gitlab.com/king011/auto-deploy/service/logger"
	"gitlab.com/king011/king-go/os/fileperm"
	"go.uber.org/zap"
	"log"
	"os"
	"regexp"
	"time"
)

var matchVersionName = regexp.MustCompile(`^v\d+\.\d+\.\d+$`)

var _DB *bolt.DB
var _Store string

// Init 初始化 數據庫信息
func Init() {
	var e error
	// 創建數據庫
	cnf := configure.Single()
	_Store = cnf.Apps.Store
	os.MkdirAll(_Store, fileperm.Directory)
	log.Println(_Store)
	_DB, e = bolt.Open(_Store+"/app.db", 0600, &bolt.Options{Timeout: time.Second * 5})
	if e != nil {
		logger.Fatal("connect database error",
			zap.Error(e),
		)
	}

	// 創建 數據 倉庫
	e = _DB.Update(func(t *bolt.Tx) (err error) {
		var bucket *bolt.Bucket
		bucket, err = t.CreateBucketIfNotExists([]byte(data.AppName))
		if err == nil {
			// 修復 錯誤 無效數據
			err = bucket.ForEach(func(k, v []byte) error {
				var app data.App
				if e := app.FromBytes(v); e != nil {
					logger.Error("data rows invalid",
						zap.Error(e),
						zap.String("package", string(k)),
					)
					if e = bucket.Delete(k); e != nil {
						return e
					}
					return nil
				} else if app.Install == data.AppNone ||
					app.Install == data.AppInstalling {
					// 安裝 錯誤 刪除
					if e := bucket.Delete(k); e != nil {
						return e
					}
				} else if app.Install == data.AppUpdating {
					// 更新失敗 調整回 未更新
					app.Install = data.AppInstallation
					app.Run = data.None
					app.Lock = false
					if b, e := app.ConvertToBytes(); e != nil {
						return e
					} else if e = bucket.Put(k, b); e != nil {
						return e
					}
				} else if app.Install == data.AppRemoving {
					// 刪除 失敗 恢復到 安裝狀態
					app.Install = data.AppInstallation
					app.Run = data.None
					app.Lock = false
					if b, e := app.ConvertToBytes(); e != nil {
						return e
					} else if e = bucket.Put(k, b); e != nil {
						return e
					}
				} else if app.Install == data.AppRemoved {
					// 刪除 錯誤 數據 損毀
					app.Install = data.AppDamaged
					app.Run = data.None
					app.Lock = false
					if b, e := app.ConvertToBytes(); e != nil {
						return e
					} else if e = bucket.Put(k, b); e != nil {
						return e
					}
				} else if app.Lock || app.Run != data.None {
					app.Run = data.None
					app.Lock = false
					if b, e := app.ConvertToBytes(); e != nil {
						return e
					} else if e = bucket.Put(k, b); e != nil {
						return e
					}
				}
				return nil
			})
			if err != nil {
				return
			}
		} else {
			// fault
			return
		}
		return
	})
	if e != nil {
		logger.Fatal("create bucket error",
			zap.Error(e),
		)
	}

	// 定時 同步數據到 磁盤
	go func() {
		for {
			time.Sleep(time.Minute * 5)
			_DB.Sync()
		}
	}()

	return
}

// InitFilesystem 初始化檔案系統
func InitFilesystem() (pkgs []string, e error) {
	// 創建 必要 檔案夾
	os.RemoveAll(_Store + "/download")
	_DB.View(func(t *bolt.Tx) (err error) {
		bucket := t.Bucket([]byte(data.AppName))
		bucket.ForEach(func(k, v []byte) error {
			os.MkdirAll(_Store+"/download/"+string(k), fileperm.Directory)
			// 刪除 損毀的 版本

			var app data.App
			if e := app.FromBytes(v); e != nil {
				return nil
			}
			if app.RunMode == data.RunAuto || app.RunMode == data.RunKeep {
				pkgs = append(pkgs, string(k))
			}
			removeDamaged(_Store+"/apps/"+string(k), app.Version)
			return nil
		})
		return
	})
	if e != nil {
		logger.Fatal("InitFilesystem error",
			zap.Error(e),
		)
		return
	}
	return
}
func removeDamaged(root, version string) {
	f, e := os.Open(root)
	if e != nil {
		logger.Warn("remove damaged error",
			zap.Error(e),
		)
		return
	}
	var names []string
	names, e = f.Readdirnames(0)
	f.Close()
	for i := 0; i < len(names); i++ {
		name := names[i]
		if !matchVersionName.MatchString(name) {
			continue
		}
		if name == version {
			continue
		}
		name = root + "/" + name
		e = os.RemoveAll(name)
		if e != nil {
			logger.Warn("remove damaged error",
				zap.Error(e),
			)
		}
	}
}

// Exit 退出 程式 關閉 數據庫
func Exit() {
	if _DB != nil {
		_DB.Sync()
		_DB.Close()
	}
}

// DownloadFilename 返回 套件 下載 檔案名
func DownloadFilename(pkg, v string) string {
	return _Store + "/download/" + pkg + "/" + v + ".tar.gz"
}

// UploadFilename 返回 套件 上傳 檔案名
func UploadFilename(pkg, v string) string {
	return _Store + "/download/" + pkg + "/" + v + ".tar.gz"
}
func mkAppDirs(pkg string) {
	os.MkdirAll(_Store+"/download/"+pkg, fileperm.Directory)
}

// AppDir 返回 app 檔案夾
func AppDir(pkg, v string) string {
	return _Store + "/apps/" + pkg + "/" + v
}

// AppRoot 返回 app 根命令
func AppRoot(pkg string) string {
	return _Store + "/apps/" + pkg
}

// Store 套件目錄
func Store() string {
	return _Store
}
