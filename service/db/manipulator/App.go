package manipulator

import (
	"errors"
	"fmt"
	"github.com/boltdb/bolt"
	"gitlab.com/king011/auto-deploy/service/db/data"
	"gitlab.com/king011/auto-deploy/service/logger"
	kStrings "gitlab.com/king011/king-go/strings"
	"go.uber.org/zap"
	"strings"
)

var errPackageEmpty = errors.New("package can't be empty")

// App .
type App struct {
}

// LockInstall 鎖定一個 安裝 套件
func (App) LockInstall(pkg string) (app *data.App, e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 安裝 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b != nil {
			tmp := &data.App{}
			e = tmp.FromBytes(b)
			if e != nil {
				logger.Error("system error",
					zap.Error(e),
				)
				return
			}
			if tmp.Install != data.AppNone {
				if tmp.Install == data.AppInstalling {
					e = fmt.Errorf("wait other install complete %s", pkg)
				} else {
					app = tmp
				}
				return
			} else if tmp.Lock {
				e = fmt.Errorf("can't lock package %s", pkg)
				return
			}
		}

		// 創建 安裝 信息 並鎖定 app
		tmp := &data.App{
			Name:    pkg,
			Install: data.AppInstalling,
			RunMode: data.RunManual,
			Lock:    true,
		}
		b, e = tmp.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}
		app = tmp
		return
	})
	if e != nil {
		return
	}
	mkAppDirs(pkg)
	return
}

// LockUpdate 鎖定一個 更新 套件
func (App) LockUpdate(pkg string) (app *data.App, e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			e = fmt.Errorf("package not found [%v]", pkg)
			return
		}
		var tmp data.App
		e = tmp.FromBytes(b)
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		if tmp.Lock {
			e = fmt.Errorf("can't lock package %s", pkg)
			return
		} else if tmp.Install != data.AppInstallation {
			e = fmt.Errorf("can't lock package %s, on %s", pkg, tmp.Install)
			return
		}
		// 創建 安裝 信息 並鎖定 app
		tmp.Install = data.AppUpdating
		tmp.Lock = true

		b, e = tmp.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}
		app = &tmp
		return
	})
	if e != nil {
		return
	}
	return
}

// SetRestart 設置 套件 重啓
func (App) SetRestart(pkg string) (e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			e = fmt.Errorf("package not found [%v]", pkg)
			return
		}
		var tmp data.App
		e = tmp.FromBytes(b)
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		if tmp.Install != data.AppUpdating {
			e = fmt.Errorf("can't lock package %s, on %s", pkg, tmp.Install)
			return
		}
		// 創建 安裝 信息 並鎖定 app
		tmp.Install = data.AppUpdate

		b, e = tmp.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// UnlockUpdate 解除 更新 鎖定
func (m App) UnlockUpdate(pkg, version string, dev bool) (e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			e = fmt.Errorf("package not found [%v]", pkg)
			return
		}
		var app data.App
		e = app.FromBytes(b)
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		app.Lock = false
		app.Version = version
		app.Install = data.AppInstallation
		app.Dev = dev
		b, e = app.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}
		return
	})
	return
}

// InstallError 安裝 套件 失敗 刪除
func (App) InstallError(pkg string) (e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 刪除 套件 信息
		k := kStrings.StringToBytes(pkg)
		e = bucket.Delete(k)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
		}
		return
	})
	return
}

// InstallSuccess 安裝完成 解鎖 並 設置套件 信息
func (App) InstallSuccess(app *data.App) (e error) {
	app.Lock = false
	app.Install = data.AppInstallation

	var b []byte
	b, e = app.ConvertToBytes()
	if e != nil {
		return
	}
	k := kStrings.StringToBytes(app.Name)
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}
		return
	})
	return
}

// List 返回 所有 已安裝的 套件
func (App) List() (app []data.App, e error) {
	arrs := make([]data.App, 0, 100)
	// 啟動 一個 只讀 事務
	_DB.View(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		bucket.ForEach(func(k, v []byte) error {
			var app data.App
			if ef := app.FromBytes(v); ef == nil {
				arrs = append(arrs, app)
			} else {
				logger.Error("db error",
					zap.Error(ef),
				)
			}
			return nil
		})
		return
	})
	if e != nil {
		return
	}
	if len(arrs) != 0 {
		app = arrs
	}
	return
}

// Show 返回 安裝 套件 詳情
func (App) Show(names []string) (app []data.App, e error) {
	m := make(map[string]bool)
	for i := 0; i < len(names); i++ {
		name := strings.TrimSpace(names[i])
		if name != "" {
			m[name] = true
		}
	}
	if len(m) == 0 {
		return
	}
	arrs := make([]data.App, 0, 100)
	// 啟動 一個 只讀 事務
	_DB.View(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		bucket.ForEach(func(k, v []byte) error {
			var app data.App
			if ef := app.FromBytes(v); ef == nil {
				if _, ok := m[app.Name]; ok {
					arrs = append(arrs, app)
				}
			} else {
				logger.Error("db error",
					zap.Error(ef),
				)
			}
			return nil
		})
		return
	})
	if e != nil {
		return
	}
	if len(arrs) != 0 {
		app = arrs
	}
	return
}

// LockRemove 鎖定一個 移除 套件
func (App) LockRemove(pkg string) (app *data.App, e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			// 返回 空 未找到 套件
			return
		}
		var tmp data.App
		e = tmp.FromBytes(b)
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		if tmp.Lock {
			e = fmt.Errorf("can't lock package %s", pkg)
			return
		}

		// 創建 安裝 信息 並鎖定 app
		tmp.Install = data.AppRemoving
		tmp.Lock = true

		b, e = tmp.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}
		app = &tmp
		return
	})
	if e != nil {
		return
	}
	return
}

// Update 更新 套件 狀態
func (App) Update(app *data.App) (e error) {
	var b []byte
	b, e = app.ConvertToBytes()
	if e != nil {
		logger.Error("system error",
			zap.Error(e),
		)
		return
	}
	k := kStrings.StringToBytes(app.Name)

	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}
		return
	})
	return
}

// RemoveSuccess 刪除 套件成功 移除 安裝信息
func (App) RemoveSuccess(pkg string) (e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 刪除 套件 信息
		k := kStrings.StringToBytes(pkg)
		e = bucket.Delete(k)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
		}
		return
	})
	return
}

// RemoveRestore 刪除 套件失敗 回滾到 安裝狀態
func (m App) RemoveRestore(app *data.App) (e error) {
	app.Lock = false
	app.Install = data.AppInstallation
	e = m.Update(app)
	return
}

// RemoveDamaged 刪除 套件失敗 並且 數據已經損毀 無法恢復
func (m App) RemoveDamaged(app *data.App) (e error) {
	app.Lock = false
	app.Install = data.AppDamaged
	e = m.Update(app)
	return
}

// ModeRun 設置 運行 模式
func (App) ModeRun(pkg string, mode data.RunMode) (e error) {
	pkg = strings.TrimSpace(pkg)
	if pkg == "" {
		e = errPackageEmpty
		return
	}
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			e = fmt.Errorf("package not found [%v]", pkg)
			return
		}
		var tmp data.App
		e = tmp.FromBytes(b)
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		if tmp.RunMode == mode {
			return
		}

		tmp.RunMode = mode
		b, e = tmp.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// ModeUpdate 設置 更新 模式
func (App) ModeUpdate(pkg string, mode data.UpdateMode) (e error) {
	pkg = strings.TrimSpace(pkg)
	if pkg == "" {
		e = errPackageEmpty
		return
	}
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			e = fmt.Errorf("package not found [%v]", pkg)
			return
		}
		var tmp data.App
		e = tmp.FromBytes(b)
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		if tmp.UpdateMode == mode {
			return
		}

		tmp.UpdateMode = mode
		b, e = tmp.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// LockRun 鎖定一個 運行 套件 返回原狀態 版本
func (App) LockRun(pkg string) (status data.RunStatus, mode data.RunMode, version string, e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			// 返回 空 未找到 套件
			e = fmt.Errorf("package not found [%s]", pkg)
			return
		}
		var tmp data.App
		e = tmp.FromBytes(b)
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}

		if tmp.Install == data.AppRemoving ||
			tmp.Install == data.AppRemoved ||
			tmp.Install == data.AppDamaged ||
			tmp.Install == data.AppNone ||
			tmp.Install == data.AppUpdate {
			// 無法 鎖定 運行
			e = fmt.Errorf("can't lock run on [%v]", tmp.Install)
			return
		}
		if tmp.Run != data.None &&
			tmp.Run != data.Exit &&
			tmp.Run != data.Abort &&
			tmp.Run != data.Kill {
			// 無法 鎖定 運行
			e = fmt.Errorf("can't lock run on [%v]", tmp.Run)
			return
		}
		// 鎖定 運行
		status = tmp.Run
		mode = tmp.RunMode
		version = tmp.Version
		tmp.Run = data.Run
		b, e = tmp.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// RunSuccess 鎖定一個 運行 套件
func (App) RunSuccess(pkg string) (e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			// 返回 空 未找到 套件
			return
		}
		var tmp data.App
		e = tmp.FromBytes(b)
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}

		if tmp.Run != data.Run {
			// 無法 鎖定 運行
			e = fmt.Errorf("app.RunSuccess but not call app.LockRun")
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		// 鎖定 運行
		tmp.Run = data.Running
		b, e = tmp.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// RunError 鎖定一個 運行 套件 失敗 重置原狀態
func (App) RunError(pkg string, status data.RunStatus) (e error) {
	// 啟動 一個 可寫 事務
	_DB.Update(func(t *bolt.Tx) (_e error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			e = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(e),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			// 返回 空 未找到 套件
			return
		}
		var tmp data.App
		e = tmp.FromBytes(b)
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}

		if tmp.Run != data.Run {
			// 無法 鎖定 運行
			e = fmt.Errorf("app.RunError but not call app.LockRun")
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		// 鎖定 運行
		tmp.Run = status
		b, e = tmp.ConvertToBytes()
		if e != nil {
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		e = bucket.Put(k, b)
		if e != nil {
			logger.Error("db error",
				zap.Error(e),
			)
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// RunExit app 退出 更新運行狀態
func (App) RunExit(pkg string, status data.RunStatus) (e error) {
	// 啟動 一個 可寫 事務
	e = _DB.Update(func(t *bolt.Tx) (err error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			err = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(err),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			err = fmt.Errorf("package not found [%v]", pkg)
			return
		}
		var tmp data.App
		err = tmp.FromBytes(b)
		if err != nil {
			logger.Error("system error",
				zap.Error(err),
			)
			return
		}

		if tmp.Run != data.Running && tmp.Run != data.Stoping {
			// 無法 鎖定 運行
			err = fmt.Errorf("app not running")
			logger.Error("system error",
				zap.Error(e),
			)
			return
		}
		// 鎖定 運行
		tmp.Run = status
		b, err = tmp.ConvertToBytes()
		if err != nil {
			logger.Error("system error",
				zap.Error(err),
			)
			return
		}
		err = bucket.Put(k, b)
		if err != nil {
			logger.Error("db error",
				zap.Error(err),
			)
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// LockStop 鎖定一個 停止 套件
func (App) LockStop(pkg string) (e error) {
	// 啟動 一個 可寫 事務
	e = _DB.Update(func(t *bolt.Tx) (err error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			err = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(err),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			// 返回 空 未找到 套件
			err = fmt.Errorf("package not found [%s]", pkg)
			return
		}
		var tmp data.App
		err = tmp.FromBytes(b)
		if err != nil {
			logger.Error("system error",
				zap.Error(err),
			)
			return
		}

		if tmp.Run != data.Running {
			// 無法 鎖定 運行
			err = fmt.Errorf("can't lock stop on [%v]", tmp.Run)
			return
		} else if tmp.Run == data.AppNone {
			err = fmt.Errorf("can't lock stop app not installed")
			return
		} else if tmp.Run == data.AppUpdate {
			err = fmt.Errorf("can't lock stop app complete updating")
			return
		}
		// 鎖定 運行
		tmp.Run = data.Stoping
		b, err = tmp.ConvertToBytes()
		if err != nil {
			logger.Error("system error",
				zap.Error(err),
			)
			return
		}
		err = bucket.Put(k, b)
		if err != nil {
			logger.Error("db error",
				zap.Error(err),
			)
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// IsStopped 返回 套件是否已經 結束
func (App) IsStopped(pkg string) (yes bool, e error) {
	// 啟動 一個 可寫 事務
	e = _DB.View(func(t *bolt.Tx) (err error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			err = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(err),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			// 返回 空 未找到 套件
			err = fmt.Errorf("package not found [%s]", pkg)
			return
		}
		var tmp data.App
		err = tmp.FromBytes(b)
		if err != nil {
			logger.Error("system error",
				zap.Error(err),
			)
			return
		}

		if tmp.Run == data.None ||
			tmp.Run == data.Exit ||
			tmp.Run == data.Abort ||
			tmp.Run == data.Kill {
			yes = true
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// SetStop 設置 套件 處於 退出狀態
func (App) SetStop(pkg string) (e error) {
	// 啟動 一個 可寫 事務
	e = _DB.Update(func(t *bolt.Tx) (err error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			err = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(err),
			)
			return
		}

		// 驗證 狀態
		k := kStrings.StringToBytes(pkg)
		b := bucket.Get(k)
		if b == nil {
			err = fmt.Errorf("package not found [%v]", pkg)
			return
		}
		var tmp data.App
		err = tmp.FromBytes(b)
		if err != nil {
			logger.Error("system error",
				zap.Error(err),
			)
			return
		}

		if tmp.Run == data.None ||
			tmp.Run == data.Stoping ||
			tmp.Run == data.Exit ||
			tmp.Run == data.Abort ||
			tmp.Run == data.Kill {
			return
		}
		// 鎖定 運行
		tmp.Run = data.Stoping
		b, err = tmp.ConvertToBytes()
		if err != nil {
			logger.Error("system error",
				zap.Error(err),
			)
			return
		}
		err = bucket.Put(k, b)
		if err != nil {
			logger.Error("db error",
				zap.Error(err),
			)
		}
		return
	})
	if e != nil {
		return
	}
	return
}

// Installed 返回 所有 安裝了的 套件名
func (App) Installed() (m map[string]bool, e error) {
	m = make(map[string]bool)
	e = _DB.View(func(t *bolt.Tx) (err error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			err = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(err),
			)
			return
		}

		// 修復 錯誤 無效數據
		bucket.ForEach(func(k, v []byte) error {
			var app data.App
			if err = app.FromBytes(v); err != nil {
				logger.Error("data rows invalid",
					zap.Error(err),
				)
				return nil
			}
			if app.Install != data.AppNone {
				m[app.Name] = true
			}
			return nil
		})
		if err != nil {
			return
		}
		return
	})
	return
}

// InstalledVersion 返回 所有 安裝了的 套件名 和 版本
func (App) InstalledVersion() (m map[string]string, e error) {
	m = make(map[string]string)
	e = _DB.View(func(t *bolt.Tx) (err error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			err = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(err),
			)
			return
		}

		// 修復 錯誤 無效數據
		bucket.ForEach(func(k, v []byte) error {
			var app data.App
			if err = app.FromBytes(v); err != nil {
				logger.Error("data rows invalid",
					zap.Error(err),
				)
				return nil
			}
			if app.Install != data.AppNone {
				m[app.Name] = app.Version
			}
			return nil
		})
		if err != nil {
			return
		}
		return
	})
	return
}

// UpdateList 返回 自動更新的 套件 列表
func (App) UpdateList() (apps []*data.App, e error) {
	e = _DB.View(func(t *bolt.Tx) (err error) {
		// 查找倉庫
		bucket := t.Bucket([]byte(data.AppName))
		if bucket == nil {
			err = fmt.Errorf("bucket not exist")
			logger.Error("db error",
				zap.Error(err),
			)
			return
		}

		// 修復 錯誤 無效數據
		arrs := make([]*data.App, 0, 100)
		bucket.ForEach(func(k, v []byte) error {
			var app data.App
			if err = app.FromBytes(v); err != nil {
				logger.Error("data rows invalid",
					zap.Error(err),
				)
				return nil
			}
			if app.UpdateMode == data.UpdateAuto || app.UpdateMode == data.UpdateDev {
				arrs = append(arrs, &app)
			}
			return nil
		})
		if err != nil {
			return
		}
		apps = arrs
		return
	})
	return
}
