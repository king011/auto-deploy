package cmd

import (
	"bufio"
	"fmt"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"golang.org/x/net/context"
	"log"
	"os"
	"strings"
)

func init() {
	var filename string
	var yes bool
	cmd := &cobra.Command{
		Use:   "remove",
		Short: "remove packages",
		Long: `remove packages

   # remove gcc cmake
   service remove gcc cmake -y
`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				return
			}
			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
			}
			defer conn.Close()
			client := grpc_service.NewServiceClient(conn)
			ok := false
			for _, name := range args {
				name = strings.TrimSpace(name)
				if name != "" {
					if !ok {
						ok = true
					}
					removeApp(client, name, yes)
				}
			}
			if !ok {
				cmd.Help()
			}
		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.BoolVarP(&yes,
		"yes", "y",
		false,
		"auto yes remove",
	)

	rootCmd.AddCommand(cmd)
}

func removeApp(client grpc_service.ServiceClient, name string, yes bool) {
	fmt.Printf("remove %s <yes/no> [yes] ", name)
	if yes {
		fmt.Println()
		removeAppDone(client, name)
	} else {
		r := bufio.NewReader(os.Stdin)
		b, _, e := r.ReadLine()
		if e != nil {
			log.Fatalln(e)
		}
		str := strings.ToLower(strings.TrimSpace(string(b)))
		if str == "" ||
			str == "yes" || str == "y" ||
			str == "true" || str == "1" {
			removeAppDone(client, name)
		} else {
			fmt.Println("cancel install")
		}
	}
}
func removeAppDone(client grpc_service.ServiceClient, name string) {
	stream, e := client.Remove(context.Background())
	if e != nil {
		log.Fatalln(e)
	}
	defer stream.CloseSend()

	e = stream.Send(&grpc_service.RemoveRequest{
		Name: name,
	})
	if e != nil {
		log.Fatalln(e)
	}
	var reply *grpc_service.RemoveReply
	for {
		reply, e = stream.Recv()
		if e != nil {
			log.Fatalln(e)
		}

		switch reply.Status {
		case grpc_service.RemoveReply_Lock:
			fmt.Println("lock :", name, reply.Data)
		case grpc_service.RemoveReply_Stoping:
			fmt.Println("stoping")
		case grpc_service.RemoveReply_Stopped:
			fmt.Println("stopped")
		case grpc_service.RemoveReply_Remove:
			fmt.Println("remove")
		case grpc_service.RemoveReply_Success:
			fmt.Println("remove success")
			return
		case grpc_service.RemoveReply_NotFound:
			fmt.Println("not found")
			return
		default:
			log.Println("unknow status", reply.Status, reply.Data)
		}
	}
}
