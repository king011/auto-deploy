package cmd

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	grpc_forward "gitlab.com/king011/auto-deploy/protocol/forward"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"log"
	"time"
)

func init() {
	var filename string
	cmd := &cobra.Command{
		Use:   "ping",
		Short: "ping server",
		Run: func(cmd *cobra.Command, args []string) {
			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
				return
			}
			defer conn.Close()
			client := grpc_forward.NewForwardClient(conn)

			last := time.Now()
			_, e = client.Ping(context.Background(), &grpc_app.PingRequest{})
			if e != nil {
				log.Fatalln(e)
			}
			fmt.Println(time.Now().Sub(last))

			last = time.Now()
			_, e = client.Ping(context.Background(), &grpc_app.PingRequest{})
			if e != nil {
				log.Fatalln(e)
			}
			fmt.Println(time.Now().Sub(last))
		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	rootCmd.AddCommand(cmd)
}
