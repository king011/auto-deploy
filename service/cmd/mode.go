package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"gitlab.com/king011/auto-deploy/service/db/data"
	"golang.org/x/net/context"
	"log"
	"strings"
)

func init() {
	var filename, run, update, pkg string
	cmd := &cobra.Command{
		Use:   "mode",
		Short: "change application run mode or update mode",
		Long: `change application run mode or update mode
		
   # change run mode to not auto run
   service mode -p gcc -r manual

   # change update mode to auto update include dev version
   service mode -p gcc -u dev
`,
		Run: func(cmd *cobra.Command, args []string) {
			pkg = strings.TrimSpace(pkg)
			run = strings.TrimSpace(run)
			update = strings.TrimSpace(update)

			if pkg == "" {
				cmd.Help()
			} else if run != "" {
				var val int32
				switch run {
				case "auto":
					val = data.RunAuto
				case "keep":
					val = data.RunKeep
				case "manual":
					val = data.RunManual
				default:
					log.Fatalln("unknow run mode", run)
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_service.NewServiceClient(conn)
				_, e = client.Mode(context.Background(),
					&grpc_service.ModeRequest{
						Name: pkg,
						Mode: grpc_service.ModeRequest_Run,
						Val:  val,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Println("change run mode success")
			} else if update != "" {
				var val int32
				switch update {
				case "auto":
					val = data.UpdateAuto
				case "dev":
					val = data.UpdateDev
				case "manual":
					val = data.UpdateManual
				default:
					log.Fatalln("unknow run mode", run)
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_service.NewServiceClient(conn)
				_, e = client.Mode(context.Background(),
					&grpc_service.ModeRequest{
						Name: pkg,
						Mode: grpc_service.ModeRequest_Update,
						Val:  val,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Println("change update mode success")
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.StringVarP(&run,
		"run", "r",
		"",
		"change run mode [ auto keep manual ]",
	)
	flags.StringVarP(&update,
		"update", "u",
		"",
		"change update mode [ auto dev manual ]",
	)
	flags.StringVarP(&pkg,
		"package", "p",
		"",
		"package name",
	)

	rootCmd.AddCommand(cmd)
}
