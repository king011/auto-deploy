package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"golang.org/x/net/context"
	"log"
)

func init() {
	var filename string
	cmd := &cobra.Command{
		Use:   "exit",
		Short: "notification service exit",
		Run: func(cmd *cobra.Command, args []string) {
			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
			}
			defer conn.Close()
			client := grpc_service.NewServiceClient(conn)
			_, e = client.Exit(context.Background(),
				&grpc_service.ExitRequest{},
			)
			if e != nil {
				log.Fatalln(e)
			}
			fmt.Println("send exit success")
		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)

	rootCmd.AddCommand(cmd)
}
