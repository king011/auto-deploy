package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/king011/auto-deploy/service/utils"
	"log"
	"path/filepath"
	"strings"
)

func init() {
	var install bool
	var src, dst, root string
	cmd := &cobra.Command{
		Use:   "test",
		Short: "test scripts",
		Long: `test scripts

	# test merge.js v0.0.1 -> v0.0.2
	service test -r apps -s v0.0.1 -d v0.0.2

	# test install.js v0.0.2
	service test -r apps -d v0.0.2 -i
`,
		Run: func(cmd *cobra.Command, args []string) {
			src = strings.TrimSpace(src)
			dst = strings.TrimSpace(dst)
			root = strings.TrimSpace(root)
			if install {
				if dst == "" || root == "" {
					cmd.Help()
					return
				}
				var e error
				root, e = filepath.Abs(root)
				if e != nil {
					log.Fatalln(e)
				}
				e = utils.Install(root, dst, "install.js", true)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Printf("install.js success %v [%v]\n", root, dst)
			} else {
				if src == "" || dst == "" || root == "" {
					cmd.Help()
					return
				}
				var e error
				root, e = filepath.Abs(root)
				if e != nil {
					log.Fatalln(e)
				}
				e = utils.Merge(root, src, dst, "merge.js", true)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Printf("merge.js success %v [%v] -> [%v]\n", root, src, dst)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&src,
		"src", "s",
		"",
		"src version",
	)
	flags.StringVarP(&dst,
		"dst", "d",
		"",
		"dst version",
	)
	flags.StringVarP(&root,
		"root", "r",
		"",
		"root directory",
	)
	flags.BoolVarP(&install,
		"install", "i",
		false,
		"if true test install.js, false test merge.js")

	rootCmd.AddCommand(cmd)
}
