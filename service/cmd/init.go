package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/king011/auto-deploy/version"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
)

const (
	// App 程式名
	App = "service"
)

var _Version bool
var rootCmd = &cobra.Command{
	Use:   App,
	Short: "auto-deploy service",
	Run: func(cmd *cobra.Command, args []string) {
		if _Version {
			fmt.Println(runtime.GOOS, runtime.GOARCH)
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
		} else {
			fmt.Println(runtime.GOOS, runtime.GOARCH)
			fmt.Println(App)
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
			fmt.Printf(`Use "%v --help" for more information about this program.
`, App)
		}
	},
}
var _ConfigureFile string

// ConfigureFile .
func ConfigureFile() string {
	if _ConfigureFile == "" {
		str, e := exec.LookPath(os.Args[0])
		if e != nil {
			log.Fatalln(e)
		}
		str, e = filepath.Abs(str)
		if e != nil {
			log.Fatalln(e)
		}
		_ConfigureFile = filepath.Dir(str) + "/etc/service.jsonnet"
	}
	return _ConfigureFile
}
func init() {
	flags := rootCmd.Flags()
	flags.BoolVarP(&_Version,
		"version", "v",
		false,
		"show version",
	)
}

// Execute 執行命令
func Execute() error {
	return rootCmd.Execute()
}
func abort() {
	os.Exit(1)
}
