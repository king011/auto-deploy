package cmd

import (
	"context"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"log"
	"os"
	"os/exec"
	"strings"
)

func init() {
	var filename, name string
	var pipe bool
	cmd := &cobra.Command{
		Use:   "run",
		Short: "run a process for package",
		Run: func(cmd *cobra.Command, args []string) {
			name = strings.TrimSpace(name)
			if name == "" {
				cmd.Help()
				return
			}

			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
			}
			defer conn.Close()
			client := grpc_service.NewServiceClient(conn)
			var reply *grpc_service.ProcessReply
			reply, e = client.Process(context.Background(),
				&grpc_service.ProcessRequest{
					Name: name,
				},
			)
			if e != nil {
				log.Fatalln(e)
			}
			c := exec.Command(reply.Name, reply.Args...)
			c.Dir = reply.Dir
			if pipe {
				c.Stderr = os.Stderr
				c.Stdout = os.Stdout
				c.Stdin = os.Stdin
				e = c.Run()
				if e != nil {
					log.Fatalln(e)
				}
			} else {
				e = c.Start()
				if e != nil {
					log.Fatalln(e)
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.StringVarP(&name,
		"name", "n",
		"",
		"package name",
	)
	flags.BoolVarP(&pipe,
		"pipe", "p",
		false,
		"pipe std io",
	)
	rootCmd.AddCommand(cmd)
}
