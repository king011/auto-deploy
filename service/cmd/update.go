package cmd

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"gitlab.com/king011/auto-deploy/utils"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"io"
	"log"
	"os"
	"strings"
)

func init() {
	var filename, tarfile string
	var dev, yes bool
	cmd := &cobra.Command{
		Use:   "update",
		Short: "update package",
		Long: `update package

   # update all package, if it's not manual mode.
   service update

   # update gcc cmake
   service update gcc cmake

   # update gcc by local tar file
   service update -t gcc.tar.gz"
`,
		Run: func(cmd *cobra.Command, args []string) {
			tarfile = strings.TrimSpace(tarfile)
			if tarfile != "" {
				updateByTar(filename, tarfile)
				return
			}
			if len(args) == 0 {
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				fmt.Println("searching...")
				client := grpc_service.NewServiceClient(conn)
				var reply *grpc_service.UpdateListReply
				reply, e = client.UpdateList(context.Background(),
					&grpc_service.UpdateListRequest{},
				)
				if e != nil {
					log.Fatalln(e)
				}
				if len(reply.Data) == 0 {
					fmt.Println("not found any update package")
				} else {
					for i := 0; i < len(reply.Data); i++ {
						updateApp(client, reply.Data[i].Name, yes, reply.Data[i].Dev)
					}
				}
			} else {
				keys := make(map[string]bool)
				for i := 0; i < len(args); i++ {
					str := strings.TrimSpace(args[i])
					if str != "" {
						keys[str] = true
					}
				}
				var apps []string
				if len(keys) == 0 {
					cmd.Help()
					return
				}
				i := 0
				apps = make([]string, len(keys))
				for app := range keys {
					apps[i] = app
					i++
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_service.NewServiceClient(conn)
				for _, app := range apps {
					updateApp(client, app, yes, dev)
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.StringVarP(&tarfile,
		"tar", "t",
		"",
		"install by local tar file",
	)
	flags.BoolVar(&dev,
		"dev",
		false,
		"include development package",
	)
	flags.BoolVarP(&yes,
		"yes", "y",
		false,
		"auto yes install",
	)
	rootCmd.AddCommand(cmd)
}
func updateByTar(filename, tarfile string) {
	b, e := utils.UnpackTarGzipFile(tarfile, "auto-deploy.json")
	if e != nil {
		log.Fatalln(e)
	}
	version := &data.Version{}
	e = json.Unmarshal(b, version)
	if e != nil {
		log.Fatalln(e)
	}
	e = version.Format()
	if e != nil {
		log.Fatalln(e)
	}
	var conn *grpc.ClientConn
	conn, e = client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	client := grpc_service.NewServiceClient(conn)
	var stream grpc_service.Service_UpdateTarClient
	stream, e = client.UpdateTar(context.Background())
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println("send package info")
	e = stream.Send(&grpc_service.UpdateTarRequest{
		Package: version.Package,
		Version: version.Name,
		Dev:     version.Dev,
	})
	if e != nil {
		log.Fatalln(e)
	}
	var reply *grpc_service.UpdateTarReply
	reply, e = stream.Recv()
	if e != nil {
		log.Fatalln(e)
	}
	switch reply.Status {
	case grpc_service.UpdateTarReply_Locked:
		fmt.Println("locked", version.Package)
	default:
		log.Fatalln("bad status", reply.Status)
	}
	// 上傳 數據
	var f *os.File
	f, e = os.Open(tarfile)
	if e != nil {
		log.Fatalln(e)
	}
	var info os.FileInfo
	info, e = f.Stat()
	if e != nil {
		log.Fatalln(e)
	}
	strSize := utils.SizeString(info.Size())
	b = make([]byte, 1024*16)
	var n int
	var sum int64
	last := 0
	for {
		n, e = f.Read(b)
		if e != nil {
			if e == io.EOF {
				break
			}
			log.Fatalln(e)
		}
		if n == 0 {
			continue
		}

		e = stream.Send(&grpc_service.UpdateTarRequest{
			Data: b[:n],
		})
		if e != nil {
			log.Fatalln(e)
		}
		sum += int64(n)
		last = utils.Rprintf(last, "upload %v / %v ", strSize, utils.SizeString(sum))
	}
	if last != 0 {
		last = 0
		fmt.Println()
	}
	f.Close()
	// 通知 上傳 完成
	e = stream.Send(&grpc_service.UpdateTarRequest{
		Complete: true,
	})
	if e != nil {
		log.Fatalln(e)
	}

	// 等待安裝 完成
	for {
		reply, e = stream.Recv()
		if e != nil {
			log.Fatalln(e)
		}
		switch reply.Status {
		case grpc_service.UpdateTarReply_Success:
			fmt.Println("install success")
			return
		case grpc_service.UpdateTarReply_Updating:
			fmt.Println("updating : ", version.Package, version.Name)
		case grpc_service.UpdateTarReply_Restart:
			fmt.Println("restart : ", version.Package, version.Name)
		default:
			log.Fatalln("bad status", reply.Status, reply.Data)
		}
	}
}
func updateApp(client grpc_service.ServiceClient, app string, yes, dev bool) {
	if dev {
		fmt.Printf("update %s dev <yes/no> [yes] ", app)
	} else {
		fmt.Printf("update %s <yes/no> [yes] ", app)
	}

	if yes {
		fmt.Println()
		updateAppDone(client, app, dev)
	} else {
		r := bufio.NewReader(os.Stdin)
		b, _, e := r.ReadLine()
		if e != nil {
			log.Fatalln(e)
		}
		str := strings.ToLower(strings.TrimSpace(string(b)))
		if str == "" ||
			str == "yes" || str == "y" ||
			str == "true" || str == "1" {
			updateAppDone(client, app, dev)
		} else {
			fmt.Println("cancel install")
		}
	}
}
func updateAppDone(client grpc_service.ServiceClient, pkg string, dev bool) {
	stream, e := client.Update(context.Background())
	if e != nil {
		log.Fatalln(e)
	}
	defer stream.CloseSend()

	e = stream.Send(&grpc_service.UpdateRequest{
		Name: pkg,
		Dev:  dev,
	})
	if e != nil {
		log.Fatalln(e)
	}
	var reply *grpc_service.UpdateReply
	first := true
	last := 0
	for {
		reply, e = stream.Recv()
		if e != nil {
			log.Fatalln(e)
		}
		if reply.Status != grpc_service.UpdateReply_Downloading {
			if last != 0 {
				last = 0
				fmt.Println()
			}
		}
		switch reply.Status {
		case grpc_service.UpdateReply_Lock:
			fmt.Println("lock :", pkg)
		case grpc_service.UpdateReply_Searching:
			if first {
				fmt.Println("searching", reply.Data)
				first = false
			} else {
				fmt.Println("\nsearching", reply.Data)
			}
		case grpc_service.UpdateReply_None:
			fmt.Println("not need update", pkg)
			return
		case grpc_service.UpdateReply_Downloading:
			last = utils.Rprint(last, "down : ", reply.Data, " ")
		case grpc_service.UpdateReply_Updating:
			fmt.Println("updating : ", reply.Data)
		case grpc_service.UpdateReply_Restart:
			fmt.Println("restart : ", pkg)
		case grpc_service.UpdateReply_Success:
			fmt.Println("update success")
			return
		default:
			log.Println("unknow status", reply.Status, reply.Data)
		}
	}
}
