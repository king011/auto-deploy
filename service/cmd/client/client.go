package client

import (
	"crypto/tls"
	"gitlab.com/king011/auto-deploy/service/configure"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// Get 返回 grpc client 客戶端
func Get(cnf *configure.Service) (client *grpc.ClientConn, e error) {
	opts := make([]grpc.DialOption, 0, 10)
	safe := cnf.TLS()
	opts = append(opts,
		grpc.WithPerRPCCredentials(NewToken(safe, cnf.Password)),
	)
	if safe {
		creds := credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: cnf.SkipVerify,
		})
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}
	client, e = grpc.Dial(
		cnf.LAddr,
		opts...,
	)
	return
}

// InitAndGet 初始化配置 並返回 grpc client 客戶端
func InitAndGet(filename string) (client *grpc.ClientConn, e error) {
	// 加載 配置
	var cnf *configure.Configure
	cnf, e = configure.Init(filename)
	if e != nil {
		return
	}
	client, e = Get(&cnf.Service)
	return
}
