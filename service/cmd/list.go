package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"golang.org/x/net/context"
	"log"
)

func init() {
	var filename string
	var onlyname bool
	cmd := &cobra.Command{
		Use:   "list",
		Short: "list all installed package",
		Run: func(cmd *cobra.Command, args []string) {
			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
			}
			defer conn.Close()
			client := grpc_service.NewServiceClient(conn)
			var reply *grpc_service.ListReply
			reply, e = client.List(context.Background(),
				&grpc_service.ListRequest{},
			)
			if e != nil {
				log.Fatalln(e)
			}

			for i := 0; i < len(reply.Data); i++ {
				node := reply.Data[i]
				if onlyname {
					fmt.Println(node.Package)
				} else {
					fmt.Println(node.Package, "   ", node.Version)
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.BoolVar(&onlyname, "onlyname",
		false, "only show name")
	rootCmd.AddCommand(cmd)
}
