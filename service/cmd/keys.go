package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"gitlab.com/king011/auto-deploy/service/utils"
	"golang.org/x/net/context"
	"log"
)

func init() {
	var filename string
	var remote bool
	cmd := &cobra.Command{
		Use:   "keys",
		Short: "return keys",
		Run: func(cmd *cobra.Command, args []string) {
			if remote {
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_service.NewServiceClient(conn)
				var reply *grpc_service.KeysReply
				reply, e = client.Keys(context.Background(),
					&grpc_service.KeysRequest{},
				)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Println("remote keys :")
				keys := reply.Data
				for i := 0; i < len(keys); i++ {
					fmt.Println("  -  ", keys[i])
				}
			} else {
				keys, e := utils.GetKeys()
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Println("local keys :")
				for i := 0; i < len(keys); i++ {
					fmt.Println("  *  ", keys[i])
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.BoolVarP(&remote,
		"remote", "r",
		false,
		"if true return remove keys default flase return local keys",
	)

	rootCmd.AddCommand(cmd)
}
