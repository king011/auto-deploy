package daemon

import (
	"gitlab.com/king011/auto-deploy/service/cmd/daemon/rpc"
	"gitlab.com/king011/auto-deploy/service/configure"
	"gitlab.com/king011/auto-deploy/service/db/manipulator"
	"gitlab.com/king011/auto-deploy/service/logger"
	"log"
)

// Run 運行 指令
func Run(filename string, disable bool) {
	// 加載 配置
	cnf, e := configure.Init(filename)
	if e != nil {
		log.Fatalln(e)
	}
	// 初始化 日誌
	logger.Init()

	// 初始化 grpc 客戶端
	rpc.Init(&cnf.Server)

	// 初始化 數據庫
	manipulator.Init()
	// 運行 grpc 服務器
	runGRPC(&cnf.Service, disable)
}
