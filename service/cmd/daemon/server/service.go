package server

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"sync/atomic"

	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	grpc_data "gitlab.com/king011/auto-deploy/protocol/data"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/daemon/download"
	"gitlab.com/king011/auto-deploy/service/cmd/daemon/rpc"
	"gitlab.com/king011/auto-deploy/service/db/data"
	"gitlab.com/king011/auto-deploy/service/db/manipulator"
	"gitlab.com/king011/auto-deploy/service/logger"
	"gitlab.com/king011/auto-deploy/service/process"
	service_utils "gitlab.com/king011/auto-deploy/service/utils"
	"gitlab.com/king011/auto-deploy/utils"
	"gitlab.com/king011/king-go/os/fileperm"
	"go.uber.org/zap"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var errCancel = context.Canceled

// Service 提供 客戶端 套件管理
type Service struct {
}

var serviceExitReply grpc_service.ExitReply

// Exit 退出服務
func (Service) Exit(ctx context.Context, request *grpc_service.ExitRequest) (reply *grpc_service.ExitReply, e error) {
	defer func() {
		recover()
	}()

	reply = &serviceExitReply

	close(SignalExit)
	return
}

// Install 安裝 套件
func (s Service) Install(stream grpc_service.Service_InstallServer) (e error) {

	var request *grpc_service.InstallRequest
	request, e = stream.Recv()
	if e != nil {
		logger.Error("grpc error",
			zap.Error(e),
		)
		return
	}
	var cancel int32
	go func() {
		// 監聽 網路異常/用戶取消安裝
		_, e := stream.Recv()
		if e != io.EOF && grpc.Code(e) != codes.Canceled {
			logger.Error("grpc error",
				zap.Error(e),
			)
		}
		atomic.StoreInt32(&cancel, 1)
	}()
	pkg := strings.TrimSpace(request.Package)
	version := strings.TrimSpace(request.Version)

	// 鎖定套件
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	if e = stream.Send(&grpc_service.InstallReply{
		Status: grpc_service.InstallReply_Lock,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	var mApp manipulator.App
	var app *data.App
	app, e = mApp.LockInstall(pkg)
	if e != nil {
		logger.Warn("install app error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	} else if app.Install != data.AppInstalling {
		// 已經 安裝 直接 返回
		e = stream.Send(&grpc_service.InstallReply{
			Status: grpc_service.InstallReply_Installed,
			Data:   app.Version,
		})
		if e != nil {
			logger.Error("grpc error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
		return
	}
	defer func() {
		if e == nil {
			// 解鎖 並 設置套件 安裝信息
			app.Version = version
			mApp.InstallSuccess(app)
		} else {
			// 刪除 套件信息
			mApp.InstallError(pkg)
		}
	}()

	// 向服務器 查找 套件
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	if e = stream.Send(&grpc_service.InstallReply{
		Status: grpc_service.InstallReply_Searching,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	client := grpc_app.NewAppClient(rpc.Single())
	var reply *grpc_app.InfonReply
	reply, e = client.Info(context.Background(),
		&grpc_app.InfoRequest{
			Package: pkg,
			Version: version,
		},
	)
	if e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	app.Description = reply.Description
	app.Dev = reply.Dev
	strSize := utils.SizeString(reply.Size)
	if e = stream.Send(&grpc_service.InstallReply{
		Status: grpc_service.InstallReply_Downloading,
		Data:   "0 / " + strSize,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	// 驗證 是否 需要 下載套件
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	filename := manipulator.DownloadFilename(pkg, version)
	var hash string
	hash, e = utils.HashFile(filename)
	if hash == reply.Hash {
		// 通知 下載 完成
		if e = stream.Send(&grpc_service.InstallReply{
			Status: grpc_service.InstallReply_Downloading,
			Data: fmt.Sprintf("<%s/%s>",
				strSize, strSize,
			),
		}); e != nil {
			logger.Warn("grpc error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
	} else {
		// 開始下載
		e = download.Download(filename, pkg, version,
			&cancel,
			client,
			reply.Size,
			func(str string) error {
				return stream.SendMsg(&grpc_service.InstallReply{
					Status: grpc_service.InstallReply_Downloading,
					Data:   str,
				})
			},
		)
		if e != nil {
			return
		}
	}

	// 開始 安裝
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	if e = stream.Send(&grpc_service.InstallReply{
		Status: grpc_service.InstallReply_Installing,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	dir := manipulator.AppDir(pkg, version)
	e = os.RemoveAll(dir)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	e = os.MkdirAll(dir, fileperm.Directory)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	// 解壓 檔案夾
	e = utils.UnpackTarGzip(filename, dir)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	// 運行 安裝 腳本
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	e = service_utils.Install(manipulator.AppRoot(pkg), version, "install.js", false)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	// 通知 安裝 完成
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	// 記錄 成功
	logger.Info("install application",
		zap.String("package", pkg),
		zap.String("version", version),
	)
	if es := stream.Send(&grpc_service.InstallReply{
		Status: grpc_service.InstallReply_Success,
	}); es != nil {
		logger.Warn("grpc error",
			zap.Error(es),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	return
}

// InstallTar 從 tar 包 安裝 套件
func (Service) InstallTar(stream grpc_service.Service_InstallTarServer) (e error) {

	var request *grpc_service.InstallTarRequest
	request, e = stream.Recv()
	if e != nil {
		logger.Error("grpc error",
			zap.Error(e),
		)
		return
	}
	pkg := strings.TrimSpace(request.Package)
	if pkg == "" {
		e = utils.ErrPackageEmpty
		logger.Warn("install tar error",
			zap.Error(e),
		)
		return
	}
	version := strings.TrimSpace(request.Version)
	e = utils.CheckVersion(version)
	if e != nil {
		logger.Warn("install tar error",
			zap.Error(e),
		)
		return
	}
	description := strings.TrimSpace(request.Description)

	var cancel int32
	ch := make(chan []byte)
	go func(ch chan []byte) {
		// 監聽 網路異常/用戶取消安裝
		for {
			reply, e := stream.Recv()
			if e == nil {
				if ch == nil {
					e = errors.New("data ch already closed")
					logger.Warn("grpc error",
						zap.Error(e),
					)
				} else {
					if reply.Complete {
						close(ch)
						ch = nil
					} else {
						if len(reply.Data) != 0 {
							ch <- reply.Data
						}
					}
				}
			} else {
				if e != io.EOF && grpc.Code(e) != codes.Canceled {
					logger.Error("grpc error",
						zap.Error(e),
					)
				}
				atomic.StoreInt32(&cancel, 1)
				break
			}

		}
		if ch != nil {
			close(ch)
		}
	}(ch)

	// 鎖定套件
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install tar cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	var mApp manipulator.App
	var app *data.App
	app, e = mApp.LockInstall(pkg)
	if e != nil {
		logger.Warn("install tar app error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	} else if app.Install != data.AppInstalling {
		// 已經 安裝 直接 返回
		e = stream.Send(&grpc_service.InstallTarReply{
			Status: grpc_service.InstallTarReply_Installed,
			Data:   app.Version,
		})
		if e != nil {
			logger.Error("grpc error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
		return
	}

	defer func() {
		if e == nil {
			// 解鎖 並 設置套件 安裝信息
			app.Version = version
			mApp.InstallSuccess(app)
		} else {
			// 刪除 套件信息
			mApp.InstallError(pkg)
		}
	}()
	app.Description = description
	app.Dev = request.Dev

	// 通知 已經 鎖定 套件
	e = stream.Send(&grpc_service.InstallTarReply{
		Status: grpc_service.InstallTarReply_Locked,
		Data:   app.Version,
	})
	if e != nil {
		logger.Error("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	// 開始 接收 tar 數據
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install tar cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	filename := manipulator.UploadFilename(pkg, version)
	var f *os.File
	f, e = os.Create(filename)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	for {
		// 驗證 取消
		if atomic.LoadInt32(&cancel) != 0 {
			f.Close()
			e = errCancel
			logger.Warn("install tar cancel",
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
		// 接受 數據
		b := <-ch
		if b == nil {
			break
		}
		// 寫入 磁盤
		_, e = f.Write(b)
		if e != nil {
			f.Close()
			logger.Warn("system error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
	}
	f.Close()

	// 開始 安裝
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install tar cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	if e = stream.Send(&grpc_service.InstallTarReply{
		Status: grpc_service.InstallTarReply_Installing,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	dir := manipulator.AppDir(pkg, version)
	e = os.RemoveAll(dir)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	e = os.MkdirAll(dir, fileperm.Directory)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	// 解壓 檔案夾
	e = utils.UnpackTarGzip(filename, dir)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	// 運行 安裝 腳本
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install tar cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	e = service_utils.Install(manipulator.AppRoot(pkg), version, "install.js", false)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	// 通知 安裝 完成
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("install tar cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	// 記錄 成功
	logger.Info("install tar application",
		zap.String("package", pkg),
		zap.String("version", version),
	)
	if es := stream.Send(&grpc_service.InstallTarReply{
		Status: grpc_service.InstallTarReply_Success,
	}); es != nil {
		logger.Warn("grpc error",
			zap.Error(es),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	return
}

// Update 更新 套件
func (s Service) Update(stream grpc_service.Service_UpdateServer) (e error) {

	var request *grpc_service.UpdateRequest
	request, e = stream.Recv()
	if e != nil {
		logger.Error("grpc error",
			zap.Error(e),
		)
		return
	}
	var cancel int32
	go func() {
		// 監聽 網路異常/用戶取消 更新
		_, e := stream.Recv()
		if e != io.EOF && grpc.Code(e) != codes.Canceled {
			logger.Error("grpc error",
				zap.Error(e),
			)
		}
		atomic.StoreInt32(&cancel, 1)
	}()
	pkg := request.Name
	dev := request.Dev
	// 鎖定套件
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("update cancel",
			zap.String("package", pkg),
		)
		return
	}
	if e = stream.Send(&grpc_service.UpdateReply{
		Status: grpc_service.UpdateReply_Lock,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	var mApp manipulator.App
	var app *data.App
	app, e = mApp.LockUpdate(pkg)
	if e != nil {
		logger.Warn("update app error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	// 原始 版本
	src := app.Version
	// 當前 版本
	current := app.Version
	currentDev := app.Dev
	versions := make([]string, 1, 10)
	versions[0] = current
	defer func() {
		var complete bool
		if e == nil && current != src {
			complete = true
		}
		var err error
		if current != src {
			// 通知 正在重設 套件
			if complete {
				if err = stream.Send(&grpc_service.UpdateReply{
					Status: grpc_service.UpdateReply_Restart,
					Data:   current,
				}); e != nil {
					logger.Warn("grpc error",
						zap.Error(err),
						zap.String("package", pkg),
					)
				}
			}
			// 重啓 套件
			err = mApp.SetRestart(pkg)
			if err == nil {
				// 關閉 套件
				process.StopAndWait(pkg)
			} else {
				logger.Error("update restart error",
					zap.Error(err),
					zap.String("package", pkg),
				)
			}
		}

		// 解鎖
		err = mApp.UnlockUpdate(pkg, current, currentDev)
		if err != nil {
			logger.Error("unlock update error",
				zap.Error(err),
				zap.String("package", pkg),
			)
			return
		}

		// 刪除 無效的 版本
		for _, v := range versions {
			if v == current {
				continue
			}
			err = os.RemoveAll(manipulator.AppDir(pkg, v))
			if err != nil {
				logger.Warn("remove update files error",
					zap.Error(err),
					zap.String("package", pkg),
					zap.String("version", v),
				)
			}
		}

		if current != src &&
			(app.Run == data.Run || app.Run == data.Running) {
			// 運行 套件
			err = process.Run(pkg)
			if err != nil {
				logger.Warn("run app error",
					zap.Error(err),
					zap.String("package", pkg),
				)
			}
		}
		// 通知 正在重設 套件
		if complete {
			if err = stream.Send(&grpc_service.UpdateReply{
				Status: grpc_service.UpdateReply_Success,
				Data:   current,
			}); err != nil {
				logger.Warn("grpc error",
					zap.Error(err),
					zap.String("package", pkg),
				)
			}

			logger.Info("update package",
				zap.String("package", pkg),
				zap.String("src", src),
				zap.String("dst", current),
			)
		}
	}()
	client := grpc_app.NewAppClient(rpc.Single())
	for {
		// 通知 開始 查詢 升級版本
		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			logger.Warn("update cancel",
				zap.String("package", pkg),
			)
			return
		}
		if e = stream.Send(&grpc_service.UpdateReply{
			Status: grpc_service.UpdateReply_Searching,
			Data:   current,
		}); e != nil {
			logger.Warn("grpc error",
				zap.Error(e),
				zap.String("package", pkg),
			)
			return
		}
		var reply *grpc_app.UpgradeReply
		reply, e = client.Upgrade(context.Background(),
			&grpc_app.UpgradeRequest{
				Package: pkg,
				Version: current,
				Dev:     dev,
			},
		)
		if e != nil {
			logger.Warn("grpc error",
				zap.Error(e),
				zap.String("package", pkg),
			)
			return
		}

		// 通知 沒有新版本 無需 更新
		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			logger.Warn("update cancel",
				zap.String("package", pkg),
			)
			return
		}
		if reply.Version == "" {
			if src == current {
				if e = stream.Send(&grpc_service.UpdateReply{
					Status: grpc_service.UpdateReply_None,
				}); e != nil {
					logger.Warn("grpc error",
						zap.Error(e),
						zap.String("package", pkg),
					)
					return
				}
			} else {
				break
			}
			return
		}
		strSize := utils.SizeString(reply.Size)
		// 驗證 是否 需要 下載套件
		filename := manipulator.DownloadFilename(pkg, reply.Version)
		var hash string
		hash, e = utils.HashFile(filename)
		if hash == reply.Hash {
			// 通知 下載 完成
			if e = stream.Send(&grpc_service.UpdateReply{
				Status: grpc_service.UpdateReply_Downloading,
				Data: fmt.Sprintf("%s <%s/%s>",
					reply.Version,
					strSize, strSize,
				),
			}); e != nil {
				logger.Warn("grpc error",
					zap.Error(e),
					zap.String("package", pkg),
					zap.String("version", reply.Version),
				)
				return
			}
		} else {
			// 開始 下載
			e = download.Download(filename, pkg, reply.Version,
				&cancel,
				client,
				reply.Size,
				func(str string) error {
					return stream.Send(&grpc_service.UpdateReply{
						Status: grpc_service.UpdateReply_Downloading,
						Data:   str,
					})
				})
			if e != nil {
				return
			}
		}

		// 開始 更新
		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			logger.Warn("update cancel",
				zap.String("package", pkg),
				zap.String("version", reply.Version),
			)
			return
		}
		if e = stream.Send(&grpc_service.UpdateReply{
			Status: grpc_service.UpdateReply_Updating,
			Data:   current + " -> " + reply.Version,
		}); e != nil {
			logger.Warn("grpc error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", reply.Version),
			)
			return
		}
		dir := manipulator.AppDir(pkg, reply.Version)
		e = os.RemoveAll(dir)
		if e != nil {
			logger.Warn("system error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", reply.Version),
			)
			return
		}
		e = os.MkdirAll(dir, fileperm.Directory)
		if e != nil {
			logger.Warn("system error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", reply.Version),
			)
			return
		}
		// 解壓 檔案夾
		e = utils.UnpackTarGzip(filename, dir)
		if e != nil {
			logger.Warn("system error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", reply.Version),
			)
			return
		}

		// 合併 數據
		e = service_utils.Merge(manipulator.AppRoot(pkg), current, reply.Version, "merge.js", false)
		if e != nil {
			logger.Warn("merge error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("src", current),
				zap.String("dst", reply.Version),
			)
			return
		}

		// 設置 新版本 信息
		current = reply.Version
		currentDev = reply.Dev
		versions = append(versions, current)

	}

	return
}

// UpdateTar 從頭 tar 包 更新 套件
func (Service) UpdateTar(stream grpc_service.Service_UpdateTarServer) (e error) {

	var request *grpc_service.UpdateTarRequest
	request, e = stream.Recv()
	if e != nil {
		logger.Error("grpc error",
			zap.Error(e),
		)
		return
	}
	pkg := strings.TrimSpace(request.Package)
	if pkg == "" {
		e = utils.ErrPackageEmpty
		logger.Warn("update tar error",
			zap.Error(e),
		)
		return
	}
	version := strings.TrimSpace(request.Version)
	e = utils.CheckVersion(version)
	if e != nil {
		logger.Warn("update tar error",
			zap.Error(e),
		)
		return
	}
	var cancel int32
	ch := make(chan []byte)
	go func(ch chan []byte) {
		// 監聽 網路異常/用戶取消安裝
		for {
			reply, e := stream.Recv()
			if e == nil {
				if ch == nil {
					e = errors.New("data ch already closed")
					logger.Warn("grpc error",
						zap.Error(e),
					)
				} else {
					if reply.Complete {
						close(ch)
						ch = nil
					} else {
						if len(reply.Data) != 0 {
							ch <- reply.Data
						}
					}
				}
			} else {
				if e != io.EOF && grpc.Code(e) != codes.Canceled {
					logger.Error("grpc error",
						zap.Error(e),
					)
				}
				atomic.StoreInt32(&cancel, 1)
				break
			}

		}
		if ch != nil {
			close(ch)
		}
	}(ch)

	// 鎖定套件
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("update tar cancel",
			zap.String("package", pkg),
		)
		return
	}
	var mApp manipulator.App
	var app *data.App
	app, e = mApp.LockUpdate(pkg)
	if e != nil {
		logger.Warn("update tar app error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	// 比較 版本
	if version <= app.Version {
		e = fmt.Errorf("target version [%v] <= current version [%v]", version, app.Version)
		logger.Warn("update tar app error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		mApp.UnlockUpdate(pkg, app.Version, app.Dev)
		return
	}

	// 原始 版本
	src := app.Version
	// 當前 版本
	current := app.Version
	versions := make([]string, 1, 10)
	versions[0] = current
	currentDev := app.Dev
	defer func() {
		var complete bool
		if e == nil && current != src {
			complete = true
		}
		var err error
		if current != src {
			// 通知 正在重設 套件
			if complete {
				if err = stream.Send(&grpc_service.UpdateTarReply{
					Status: grpc_service.UpdateTarReply_Restart,
					Data:   current,
				}); e != nil {
					logger.Warn("grpc error",
						zap.Error(err),
						zap.String("package", pkg),
					)
				}
			}
			// 重啓 套件
			err = mApp.SetRestart(pkg)
			if err == nil {
				// 關閉 套件
				process.StopAndWait(pkg)
			} else {
				logger.Error("update restart error",
					zap.Error(err),
					zap.String("package", pkg),
				)
			}
		}

		// 解鎖
		err = mApp.UnlockUpdate(pkg, current, currentDev)
		if err != nil {
			logger.Error("unlock update error",
				zap.Error(err),
				zap.String("package", pkg),
			)
			return
		}

		// 刪除 無效的 版本
		for _, v := range versions {
			if v == current {
				continue
			}
			err = os.RemoveAll(manipulator.AppDir(pkg, v))
			if err != nil {
				logger.Warn("remove update files error",
					zap.Error(err),
					zap.String("package", pkg),
					zap.String("version", v),
				)
			}
		}

		if current != src &&
			(app.Run == data.Run || app.Run == data.Running) {
			// 運行 套件
			err = process.Run(pkg)
			if err != nil {
				logger.Warn("run app error",
					zap.Error(err),
					zap.String("package", pkg),
				)
			}
		}
		// 通知 正在重設 套件
		if complete {
			if err = stream.Send(&grpc_service.UpdateTarReply{
				Status: grpc_service.UpdateTarReply_Success,
				Data:   current,
			}); err != nil {
				logger.Warn("grpc error",
					zap.Error(err),
					zap.String("package", pkg),
				)
			}

			logger.Info("update package",
				zap.String("package", pkg),
				zap.String("src", src),
				zap.String("dst", current),
			)
		}
	}()

	// 通知 已經 鎖定 套件
	e = stream.Send(&grpc_service.UpdateTarReply{
		Status: grpc_service.UpdateTarReply_Locked,
		Data:   app.Version,
	})
	if e != nil {
		logger.Error("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	// 開始 接收 tar 數據
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("update tar cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	filename := manipulator.UploadFilename(pkg, version)
	var f *os.File
	f, e = os.Create(filename)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	for {
		// 驗證 取消
		if atomic.LoadInt32(&cancel) != 0 {
			f.Close()
			e = errCancel
			logger.Warn("update tar cancel",
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
		// 接受 數據
		b := <-ch
		if b == nil {
			break
		}
		// 寫入 磁盤
		_, e = f.Write(b)
		if e != nil {
			f.Close()
			logger.Warn("system error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
	}
	f.Close()

	// 開始 更新
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("update tar cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	if e = stream.Send(&grpc_service.UpdateTarReply{
		Status: grpc_service.UpdateTarReply_Updating,
		Data:   current + " -> " + version,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	dir := manipulator.AppDir(pkg, version)
	e = os.RemoveAll(dir)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	e = os.MkdirAll(dir, fileperm.Directory)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	// 解壓 檔案夾
	e = utils.UnpackTarGzip(filename, dir)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	// 合併 數據
	e = service_utils.Merge(manipulator.AppRoot(pkg), current, version, "merge.js", false)
	if e != nil {
		logger.Warn("merge error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("src", current),
			zap.String("dst", version),
		)
		return
	}

	// 設置 新版本 信息
	current = version
	currentDev = request.Dev
	versions = append(versions, current)

	return
}

// List 列出 安裝的 套件
func (Service) List(ctx context.Context, request *grpc_service.ListRequest) (reply *grpc_service.ListReply, e error) {
	var mApp manipulator.App
	var apps []data.App
	apps, e = mApp.List()
	if e != nil {
		logger.Warn("list error",
			zap.Error(e),
		)
		return
	}
	reply = &grpc_service.ListReply{}
	if len(apps) != 0 {
		reply.Data = make([]*grpc_data.App, len(apps))
		for i := 0; i < len(apps); i++ {
			reply.Data[i] = apps[i].ConvertToPB()
		}
	}
	return
}

// Show 列出 安裝的 套件
func (Service) Show(ctx context.Context, request *grpc_service.ShowRequest) (reply *grpc_service.ShowReply, e error) {
	var mApp manipulator.App
	var apps []data.App
	apps, e = mApp.Show(request.Name)
	if e != nil {
		logger.Warn("show error",
			zap.Error(e),
			zap.Strings("apps", request.Name),
		)
		return
	}
	reply = &grpc_service.ShowReply{}
	if len(apps) != 0 {
		reply.Data = make([]*grpc_data.App, len(apps))
		for i := 0; i < len(apps); i++ {
			reply.Data[i] = apps[i].ConvertToPB()
		}
	}
	return
}

// Remove 刪除 套件 版本
func (Service) Remove(stream grpc_service.Service_RemoveServer) (e error) {

	var request *grpc_service.RemoveRequest
	request, e = stream.Recv()
	if e != nil {
		logger.Error("grpc error",
			zap.Error(e),
		)
		return
	}
	var cancel int32
	go func() {
		// 監聽 網路異常/用戶取消安裝
		_, e := stream.Recv()
		if e != io.EOF && grpc.Code(e) != codes.Canceled {
			logger.Error("grpc error",
				zap.Error(e),
			)
		}
		atomic.StoreInt32(&cancel, 1)
	}()
	pkg := strings.TrimSpace(request.Name)

	// 鎖定套件
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("remove cancel",
			zap.String("package", pkg),
		)
		return
	}
	if e = stream.Send(&grpc_service.RemoveReply{
		Status: grpc_service.RemoveReply_Lock,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	var mApp manipulator.App
	var app *data.App
	app, e = mApp.LockRemove(pkg)
	if e != nil {
		logger.Warn("remove app error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	} else if app == nil {
		logger.Info("remove app not found",
			zap.String("package", pkg),
		)
		// 通知 未找到 套件
		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			logger.Warn("remove cancel",
				zap.String("package", pkg),
			)
			return
		}
		e = stream.Send(&grpc_service.RemoveReply{
			Status: grpc_service.RemoveReply_NotFound,
		})
		if e != nil {
			logger.Error("grpc error",
				zap.Error(e),
				zap.String("package", pkg),
			)
			return
		}
		return
	}
	version := app.Version

	defer func() {
		if e == nil {
			// 刪除 套件 信息
			mApp.RemoveSuccess(pkg)
		} else {
			// 刪除
			if app.Install == data.AppRemoved {
				// 數據 已損毀
				mApp.RemoveDamaged(app)
			} else {
				// 恢復到 安裝 狀態
				mApp.RemoveRestore(app)
			}
		}
	}()
	// 停止 套件
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("remove cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	if e = stream.Send(&grpc_service.RemoveReply{
		Status: grpc_service.RemoveReply_Stoping,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	process.StopAndWait(pkg)

	// 套件 已停止
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("remove cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	if e = stream.Send(&grpc_service.RemoveReply{
		Status: grpc_service.RemoveReply_Stopped,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	// 更新 移除 狀態
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("remove cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	app.Install = data.AppRemoved
	e = mApp.Update(app)
	if e != nil {
		logger.Warn("remove app error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		app.Install = data.AppRemoving
		return
	}

	// 移除數據
	if atomic.LoadInt32(&cancel) != 0 {
		app.Install = data.AppRemoving
		e = errCancel
		logger.Warn("remove cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	dir := manipulator.AppDir(pkg, version)
	e = os.RemoveAll(dir)
	if e != nil {
		logger.Error("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	logger.Info("remove app",
		zap.String("package", pkg),
		zap.String("version", version),
	)
	// 通知 移除 完成
	if atomic.LoadInt32(&cancel) != 0 {
		return
	}
	if e = stream.Send(&grpc_service.RemoveReply{
		Status: grpc_service.RemoveReply_Success,
	}); e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	return
}

var _ServiceModeReply grpc_service.ModeReply

// Mode 修改 運行/更新 模式
func (Service) Mode(ctx context.Context, request *grpc_service.ModeRequest) (reply *grpc_service.ModeReply, e error) {

	switch request.Mode {
	case grpc_service.ModeRequest_Run:
		var mApp manipulator.App
		mode := data.RunMode(request.Val)
		e = mApp.ModeRun(request.Name, mode)
		if e != nil {
			return
		}
		logger.Info("change mode",
			zap.String("mode", grpc_service.ModeRequest_Run.String()),
			zap.String("val", mode.String()),
		)
	case grpc_service.ModeRequest_Update:
		var mApp manipulator.App
		mode := data.UpdateMode(request.Val)
		e = mApp.ModeUpdate(request.Name, mode)
		if e != nil {
			return
		}
		logger.Info("change mode",
			zap.String("mode", grpc_service.ModeRequest_Update.String()),
			zap.String("val", mode.String()),
		)
	default:
		e = fmt.Errorf("unknow mode %v", request.Mode)
		return
	}
	reply = &_ServiceModeReply
	return
}

var _ServiceAppReply grpc_service.AppReply

// App 運行/停止 套件
func (Service) App(ctx context.Context, request *grpc_service.AppRequest) (reply *grpc_service.AppReply, e error) {
	pkg := strings.TrimSpace(request.Name)
	if request.Run {
		e = process.Run(pkg)
		if e != nil {
			logger.Warn("run app error",
				zap.Error(e),
				zap.String("package", pkg),
			)
			return
		}
		logger.Info("run app",
			zap.String("package", pkg),
		)
	} else {
		var mApp manipulator.App
		e = mApp.LockStop(pkg)
		if e != nil {
			logger.Warn("stop app error",
				zap.Error(e),
				zap.String("package", pkg),
			)
			return
		}
		e = process.Stop(pkg)
		if e != nil {
			logger.Warn("stop app error",
				zap.Error(e),
				zap.String("package", pkg),
			)
			return
		}
		logger.Info("stop app",
			zap.String("package", pkg),
		)
	}
	reply = &_ServiceAppReply
	return
}

// Find 查找 套件
func (Service) Find(ctx context.Context, request *grpc_service.FindRequest) (reply *grpc_service.FindReply, e error) {
	client := grpc_app.NewAppClient(rpc.Single())
	var r *grpc_app.FindReply
	names := request.Name
	r, e = client.Find(context.Background(),
		&grpc_app.FindRequest{
			Name: request.Name,
		},
	)
	if e != nil {
		logger.Warn("find package error",
			zap.Error(e),
			zap.Strings("name", names),
		)
		return
	}
	var mApp manipulator.App
	var m map[string]bool
	m, e = mApp.Installed()
	if e != nil {
		logger.Warn("find package error",
			zap.Error(e),
		)
		return
	}

	reply = &grpc_service.FindReply{}
	if len(r.Data) != 0 {
		reply.Data = make([]*grpc_service.PackageInfo, len(r.Data))
		for i := 0; i < len(reply.Data); i++ {
			if m != nil && m[r.Data[i].Name] {
				reply.Data[i] = &grpc_service.PackageInfo{
					Name:        r.Data[i].Name,
					Description: r.Data[i].Description,
					Installed:   true,
				}
			} else {
				reply.Data[i] = &grpc_service.PackageInfo{
					Name:        r.Data[i].Name,
					Description: r.Data[i].Description,
					Installed:   false,
				}
			}
		}
	}
	return
}

// FindEx  查找 服務器 存在的 套件 並且 包含 全部 信息
func (Service) FindEx(ctx context.Context, request *grpc_service.FindExRequest) (reply *grpc_service.FindExReply, e error) {
	client := grpc_app.NewAppClient(rpc.Single())
	var r *grpc_app.FindExReply
	r, e = client.FindEx(context.Background(),
		&grpc_app.FindExRequest{
			Name: request.Name,
		},
	)
	if e != nil {
		logger.Warn("find package error",
			zap.Error(e),
		)
		return
	}
	var mApp manipulator.App
	var m map[string]string
	m, e = mApp.InstalledVersion()
	if e != nil {
		logger.Warn("find package error",
			zap.Error(e),
		)
		return
	}

	reply = &grpc_service.FindExReply{}
	if len(r.Data) != 0 {
		reply.Data = make([]*grpc_service.PackageInfoEx, len(r.Data))
		for i := 0; i < len(r.Data); i++ {
			var node *grpc_service.PackageInfoEx
			var installed bool
			var version string
			if m != nil {
				var ok bool
				if version, ok = m[r.Data[i].Name]; ok {
					installed = true
				}
			}
			node = &grpc_service.PackageInfoEx{
				Name:        r.Data[i].Name,
				Description: r.Data[i].Description,
				Installed:   installed,
			}
			reply.Data[i] = node
			vs := r.Data[i].Data
			if len(vs) != 0 {
				node.Data = make([]*grpc_service.VersionInfo, len(vs))
				for j := 0; j < len(vs); j++ {
					if version == vs[j].Name {
						installed = true
					} else {
						installed = false
					}
					node.Data[j] = &grpc_service.VersionInfo{
						Name:        vs[j].Name,
						Description: vs[j].Description,
						Dev:         vs[j].Dev,
						Installed:   installed,
					}
				}
			}
		}
	}
	return
}

// Keys 返回 網卡 指紋
func (Service) Keys(ctx context.Context, request *grpc_service.KeysRequest) (reply *grpc_service.KeysReply, e error) {
	var keys []string
	keys, e = service_utils.GetKeys()
	if e != nil {
		return
	}

	reply = &grpc_service.KeysReply{
		Data: keys,
	}
	return
}

// UpdateList 返回所有 設置了自動更新的 套件列表
func (Service) UpdateList(ctx context.Context, request *grpc_service.UpdateListRequest) (reply *grpc_service.UpdateListReply, e error) {
	var mApp manipulator.App
	var apps []*data.App
	apps, e = mApp.UpdateList()
	if e != nil {
		logger.Warn("UpdateList error",
			zap.Error(e),
		)
		return
	}

	reply = &grpc_service.UpdateListReply{}
	if len(apps) != 0 {
		reply.Data = make([]*grpc_service.UpdateListData, len(apps))
		for i, app := range apps {
			reply.Data[i] = &grpc_service.UpdateListData{
				Name: app.Name,
				Dev:  app.UpdateMode == data.UpdateDev,
			}
		}
	}
	return
}

// Process 返回 套件的 進程 啓動 信息
func (Service) Process(ctx context.Context, request *grpc_service.ProcessRequest) (reply *grpc_service.ProcessReply, e error) {
	var name, dir string
	var args []string
	pkg := request.Name
	name, args, dir, e = process.GetCommand(pkg)
	if e != nil {
		logger.Warn("Process",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	reply = &grpc_service.ProcessReply{
		Name: name,
		Args: args,
		Dir:  dir,
	}
	return
}
