package server

import (
	"io"
	"sync/atomic"

	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	grpc_forward "gitlab.com/king011/auto-deploy/protocol/forward"
	"gitlab.com/king011/auto-deploy/service/cmd/daemon/rpc"
	"gitlab.com/king011/auto-deploy/service/logger"
	"go.uber.org/zap"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// Forward 將 service 客戶端 向 service 服務器的 請求 轉發 到 server
type Forward struct {
}

// Search 查找 可安裝的 套件
func (Forward) Search(ctx context.Context, request *grpc_app.SearchRequest) (reply *grpc_app.SearchReply, e error) {
	client := grpc_app.NewAppClient(rpc.Single())
	reply, e = client.Search(ctx, request)
	if e != nil {
		logger.Warn("forward search",
			zap.Error(e),
			zap.Strings("names", request.Name),
			zap.Bool("dev", request.Dev),
		)
	}
	return
}

// Download 下載 app
func (Forward) Download(stream grpc_forward.Forward_DownloadServer) (e error) {

	// 接收 下載 參數
	var request *grpc_app.DownloadRequest
	request, e = stream.Recv()
	if e != nil {
		logger.Warn("forward download",
			zap.Error(e),
		)
		return
	}
	pkg := request.Package
	version := request.Version

	// 監聽 用戶 取消
	var cancel int32
	go func() {
		// 監聽 網路異常/用戶取消安裝
		_, e := stream.Recv()
		if e != io.EOF && grpc.Code(e) != codes.Canceled {
			logger.Error("forward download",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", version),
			)
		}
		atomic.StoreInt32(&cancel, 1)
	}()
	// 轉發 請求
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("forward download cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	client := grpc_app.NewAppClient(rpc.Single())
	var downloadStream grpc_app.App_DownloadClient
	downloadStream, e = client.Download(context.Background())
	if e != nil {
		logger.Warn("grpc",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		logger.Warn("forward download cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	e = downloadStream.Send(request)
	if e != nil {
		logger.Warn("grpc",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	var reply *grpc_app.DownloadReply
	for {
		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			logger.Warn("forward download cancel",
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
		reply, e = downloadStream.Recv()
		if e != nil {
			logger.Warn("grpc",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", version),
			)
			break
		}

		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			logger.Warn("forward download cancel",
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
		e = stream.Send(reply)
		if e != nil {
			logger.Warn("grpc",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", version),
			)
			break
		}
	}
	return
}

// Ping 測試 服務器 響應 速度
func (Forward) Ping(ctx context.Context, request *grpc_app.PingRequest) (reply *grpc_app.PingReply, e error) {
	client := grpc_app.NewAppClient(rpc.Single())
	reply, e = client.Ping(ctx, request)
	if e != nil {
		logger.Warn("forward ping",
			zap.Error(e),
		)
	}
	return
}
