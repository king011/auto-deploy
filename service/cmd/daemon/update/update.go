package update

import (
	"context"
	"os"
	"sync"

	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	"gitlab.com/king011/auto-deploy/service/cmd/daemon/download"
	"gitlab.com/king011/auto-deploy/service/cmd/daemon/rpc"
	"gitlab.com/king011/auto-deploy/service/db/data"
	"gitlab.com/king011/auto-deploy/service/db/manipulator"
	"gitlab.com/king011/auto-deploy/service/logger"
	"gitlab.com/king011/auto-deploy/service/process"
	service_utils "gitlab.com/king011/auto-deploy/service/utils"
	"gitlab.com/king011/auto-deploy/utils"
	"gitlab.com/king011/king-go/os/fileperm"
	"go.uber.org/zap"
)

type _Context struct {
	sync.Mutex
	locked bool
}

var _Single _Context

// Done .
func Done(v string) {
	_Single.Done(v)
}
func (c *_Context) Done(v string) {
	c.Lock()
	if c.locked {
		c.Unlock()
		logger.Info("auto update can't lock",
			zap.String("val", v),
		)
		return
	}
	c.locked = true
	c.Unlock()
	logger.Info("auto update",
		zap.String("val", v),
	)

	c.done()

	c.Lock()
	c.locked = false
	c.Unlock()
}
func (c *_Context) done() {
	var mApp manipulator.App
	apps, e := mApp.UpdateList()
	if e != nil {
		logger.Error("get update list error",
			zap.Error(e),
		)
		return
	}
	if len(apps) == 0 {
		logger.Info("not found update package")
		return
	}

	for _, app := range apps {
		c.update(app.Name, app.UpdateMode == data.UpdateDev)
	}
}
func (c *_Context) update(pkg string, dev bool) {
	// 鎖定套件
	var mApp manipulator.App
	app, e := mApp.LockUpdate(pkg)
	if e != nil {
		logger.Warn("auto update app error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}

	// 原始 版本
	src := app.Version
	// 當前 版本
	current := app.Version
	currentDev := app.Dev
	versions := make([]string, 1, 10)
	versions[0] = current
	defer func() {
		var complete bool
		if e == nil && current != src {
			complete = true
		}
		var err error
		if current != src {
			// 通知 正在重設 套件
			// 重啓 套件
			err = mApp.SetRestart(pkg)
			if err == nil {
				// 關閉 套件
				process.StopAndWait(pkg)
			} else {
				logger.Error("update restart error",
					zap.Error(err),
					zap.String("package", pkg),
				)
			}
		}

		// 解鎖
		err = mApp.UnlockUpdate(pkg, current, currentDev)
		if err != nil {
			logger.Error("unlock update error",
				zap.Error(err),
				zap.String("package", pkg),
			)
			return
		}

		// 刪除 無效的 版本
		for _, v := range versions {
			if v == current {
				continue
			}
			err = os.RemoveAll(manipulator.AppDir(pkg, v))
			if err != nil {
				logger.Warn("remove update files error",
					zap.Error(err),
					zap.String("package", pkg),
					zap.String("version", v),
				)
			}
		}

		if current != src &&
			(app.Run == data.Run || app.Run == data.Running) {
			// 運行 套件
			err = process.Run(pkg)
			if err != nil {
				logger.Warn("run app error",
					zap.Error(err),
					zap.String("package", pkg),
				)
			}
		}
		// 通知 正在重設 套件
		if complete {
			logger.Info("update package",
				zap.String("package", pkg),
				zap.String("src", src),
				zap.String("dst", current),
			)
		}
	}()

	client := grpc_app.NewAppClient(rpc.Single())
	for {
		// 通知 開始 查詢 升級版本
		var reply *grpc_app.UpgradeReply
		reply, e = client.Upgrade(context.Background(),
			&grpc_app.UpgradeRequest{
				Package: pkg,
				Version: current,
				Dev:     dev,
			},
		)
		if e != nil {
			logger.Warn("grpc error",
				zap.Error(e),
				zap.String("package", pkg),
			)
			return
		}

		// 通知 沒有新版本 無需 更新
		if reply.Version == "" {
			if src == current {
			} else {
				break
			}
			return
		}
		// 驗證 是否 需要 下載套件
		filename := manipulator.DownloadFilename(pkg, reply.Version)
		var hash string
		hash, e = utils.HashFile(filename)
		if hash == reply.Hash {
		} else {
			// 開始 下載
			e = download.Download(
				filename,
				pkg, reply.Version,
				nil,
				client,
				0, nil,
			)
			if e != nil {
				return
			}
		}

		// 開始 更新
		dir := manipulator.AppDir(pkg, reply.Version)
		e = os.RemoveAll(dir)
		if e != nil {
			logger.Warn("system error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", reply.Version),
			)
			return
		}
		e = os.MkdirAll(dir, fileperm.Directory)
		if e != nil {
			logger.Warn("system error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", reply.Version),
			)
			return
		}
		// 解壓 檔案夾
		e = utils.UnpackTarGzip(filename, dir)
		if e != nil {
			logger.Warn("system error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", reply.Version),
			)
			return
		}

		// 合併 數據
		e = service_utils.Merge(manipulator.AppRoot(pkg), current, reply.Version, "merge.js", false)
		if e != nil {
			logger.Warn("merge error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("src", current),
				zap.String("dst", reply.Version),
			)
			return
		}

		// 設置 新版本 信息
		current = reply.Version
		currentDev = reply.Dev
		versions = append(versions, current)

	}

}
