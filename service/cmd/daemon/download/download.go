package download

import (
	"context"
	"fmt"
	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	"gitlab.com/king011/auto-deploy/service/logger"
	"gitlab.com/king011/auto-deploy/speed"
	"gitlab.com/king011/auto-deploy/utils"
	"gitlab.com/king011/king-go/os/fileperm"
	"go.uber.org/zap"
	"os"
	"sync/atomic"
	"time"
)

const speedDuration = time.Second * 5

var errCancel = context.Canceled

// Download 下載檔案
func Download(filename, pkg, version string,
	cancel *int32,
	client grpc_app.AppClient,
	size int64,
	outputf func(str string) error,
) (e error) {
	// 開始 下載
	last := time.Now()

	var offset int64
	var hash []byte
	offset, hash, e = getHash(filename)
	if e != nil {
		logger.Warn("system",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	// 向 服務器 請求檔案
	var download grpc_app.App_DownloadClient
	download, e = client.Download(context.Background())
	if e != nil {
		logger.Warn("grpc",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	e = download.Send(&grpc_app.DownloadRequest{
		Package: pkg,
		Version: version,

		Offset: offset,
		Hash:   hash,
	})
	if e != nil {
		logger.Warn("grpc",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	if cancel != nil && atomic.LoadInt32(cancel) != 0 {
		e = errCancel
		logger.Warn("cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	// 獲取 響應
	var reply *grpc_app.DownloadReply
	reply, e = download.Recv()
	if e != nil {
		logger.Warn("grpc error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	if cancel != nil && atomic.LoadInt32(cancel) != 0 {
		e = errCancel
		logger.Warn("cancel",
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}

	// 依據 響應 分別處理 v0.1.9 之前 和 之後的 下載
	if reply.Code == grpc_app.DownloadReply_UNIVERSAL {
		// v0.1.9 和 之前 版本 不支持 斷點續傳
		downloadV0_1_9(filename, pkg, version,
			cancel,
			client,
			size,
			outputf,
			last,
			download, reply,
		)
	} else {
		downloadV0_1_9after(filename, pkg, version,
			cancel,
			client,
			size,
			outputf,
			last, offset,
			download, reply,
		)
	}
	return
}
func downloadV0_1_9after(filename, pkg, version string,
	cancel *int32,
	client grpc_app.AppClient,
	total int64,
	outputf func(str string) error,
	last time.Time, offset int64,
	download grpc_app.App_DownloadClient,
	reply *grpc_app.DownloadReply,
) (e error) {
	var f *os.File
	defer func() {
		if f != nil {
			f.Close()
		}
	}()

	var size int64
	var statistics *speed.Statistics
	var totalStr, currentStr string
	if outputf != nil {
		statistics = speed.NewStatistics(speedDuration)
		totalStr = utils.SizeString(total)
		currentStr = utils.SizeString(offset)
	}
	first := true
	exit := false
	for {
		// 驗證 取消
		if cancel != nil && atomic.LoadInt32(cancel) != 0 {
			e = errCancel
			logger.Warn("cancel",
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
		// 接受 數據
		if first {
			// 初始 數據 已經 被 函數 傳入 不需要 recv
			first = false
		} else {
			reply, e = download.Recv()
			if e != nil {
				logger.Warn("grpc error",
					zap.Error(e),
					zap.String("package", pkg),
					zap.String("version", version),
				)
				return
			}
		}

		// 寫入磁盤
		switch reply.Code {
		case grpc_app.DownloadReply_Info:
			if outputf != nil {
				size = total
				total = reply.Size
				totalStr = utils.SizeString(total)

				if e = outputf(fmt.Sprintf("<%s/%s> %s %s",
					currentStr, totalStr,
					getSpeed(statistics),
					getETA(statistics, total),
				)); e != nil {
					logger.Warn("download",
						zap.Error(e),
						zap.String("package", pkg),
						zap.String("version", version),
					)
					return
				}
			}
		case grpc_app.DownloadReply_Changed:
			// 重新 創建 檔案
			f, e = os.Create(filename)
			if e != nil {
				logger.Warn("download",
					zap.Error(e),
					zap.String("package", pkg),
					zap.String("version", version),
				)
				return
			}
			if outputf != nil {
				offset = 0
				currentStr = "0"
				if e = outputf(fmt.Sprintf("<%s/%s> %s %s",
					currentStr, totalStr,
					getSpeed(statistics),
					getETA(statistics, total),
				)); e != nil {
					logger.Warn("download",
						zap.Error(e),
						zap.String("package", pkg),
						zap.String("version", version),
					)
					return
				}
			}
		case grpc_app.DownloadReply_Bytes:
			if len(reply.Data) != 0 {
				// 打開 檔案
				if f == nil {
					f, e = os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, fileperm.File)
					if e != nil {
						logger.Warn("download",
							zap.Error(e),
							zap.String("package", pkg),
							zap.String("version", version),
						)
						return
					}
					total -= offset
				}
				// 寫入 數據
				_, e = f.Write(reply.Data)
				if e != nil {
					if e != nil {
						logger.Warn("download",
							zap.Error(e),
							zap.String("package", pkg),
							zap.String("version", version),
						)
						return
					}
				}
				if outputf != nil {
					n := int64(len(reply.Data))
					total -= n
					statistics.Push(n)

					offset += n
					currentStr = utils.SizeString(offset)

					if e = outputf(fmt.Sprintf("<%s/%s> %s %s",
						currentStr, totalStr,
						getSpeed(statistics),
						getETA(statistics, total),
					)); e != nil {
						logger.Warn("download",
							zap.Error(e),
							zap.String("package", pkg),
							zap.String("version", version),
						)
						return
					}
				}
			}
		case grpc_app.DownloadReply_Completed:
			if f == nil && size == 0 {
				// 創建 空 檔案
				f, e = os.Create(filename)
				if e != nil {
					logger.Warn("download",
						zap.Error(e),
						zap.String("package", pkg),
						zap.String("version", version),
					)
					return
				}
			}
			exit = true
			if outputf != nil {
				if e = outputf(fmt.Sprintf("<%s/%s> %s [%v]",
					currentStr, totalStr,
					getSpeed(statistics),
					time.Now().Sub(last),
				)); e != nil {
					logger.Warn("download",
						zap.Error(e),
						zap.String("package", pkg),
						zap.String("version", version),
					)
					return
				}
			}
		default:
			e = fmt.Errorf("unknow code %v", reply.Code)
			return
		}
		// 下載 完成
		if exit {
			break
		}
	}
	return
}
func downloadV0_1_9(filename, pkg, version string,
	cancel *int32,
	client grpc_app.AppClient,
	size int64,
	outputf func(str string) error,
	last time.Time,
	download grpc_app.App_DownloadClient,
	reply *grpc_app.DownloadReply,
) (e error) {
	var f *os.File
	f, e = os.Create(filename)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
			zap.String("version", version),
		)
		return
	}
	defer f.Close()
	var sum int64
	var statistics *speed.Statistics
	var strSize string
	if outputf != nil {
		statistics = speed.NewStatistics(speedDuration)
		strSize = utils.SizeString(size)
	}
	first := true
	for {
		// 驗證 取消
		if cancel != nil && atomic.LoadInt32(cancel) != 0 {
			e = errCancel
			logger.Warn("cancel",
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}
		// 接受 數據
		if first {
			// 初始 數據 已經 被 函數 傳入 不需要 recv
			first = false
		} else {
			reply, e = download.Recv()
			if e != nil {
				logger.Warn("grpc error",
					zap.Error(e),
					zap.String("package", pkg),
					zap.String("version", version),
				)
				return
			}
		}
		if reply.Complete {
			// 下載完成
			if outputf != nil {
				if e = outputf(fmt.Sprintf("<%s/%s> [%s/s] [%v]",
					utils.SizeString(sum), strSize,
					utils.SizeString(statistics.Speed()),
					time.Now().Sub(last),
				)); e != nil {
					logger.Warn("grpc error",
						zap.Error(e),
						zap.String("package", pkg),
						zap.String("version", version),
					)
					return
				}
			}
			break
		} else if len(reply.Data) == 0 {
			continue
		}

		// 寫入 磁盤
		_, e = f.Write(reply.Data)
		if e != nil {
			logger.Warn("system error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.String("version", version),
			)
			return
		}

		// 更新下載 進度
		n := int64(len(reply.Data))
		sum += n
		if outputf != nil {
			statistics.Push(n)
			if e = outputf(fmt.Sprintf("<%s/%s> %s %s",
				utils.SizeString(sum), strSize,
				getSpeed(statistics),
				getETA(statistics, size-sum),
			)); e != nil {
				logger.Warn("grpc error",
					zap.Error(e),
					zap.String("package", pkg),
					zap.String("version", version),
				)
				return
			}
		}
	}
	return
}
func getSpeed(statistics *speed.Statistics) (str string) {
	s := statistics.Speed()
	if s < 1 {
		return
	}
	str = fmt.Sprintf("[%v/s]", utils.SizeString(s))
	return
}
func getETA(statistics *speed.Statistics, total int64) (str string) {
	if total < 1024 {
		return
	}
	s := statistics.Speed()
	if s < 1024 {
		return
	}

	str = fmt.Sprint(time.Duration(total*int64(time.Second)/s), " ETA")
	return
}
