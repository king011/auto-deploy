package download

import (
	"crypto/md5"
	"io"
	"os"
)

func getHash(filename string) (offset int64, hash []byte, e error) {
	// 添加 讀寫 權限
	info, e := os.Stat(filename)
	if e != nil {
		if os.IsNotExist(e) {
			e = nil
			return
		}
	}

	// 查詢 檔案 大小
	offset = info.Size()
	if offset == 0 {
		return
	}

	// 計算 hash
	var f *os.File
	f, e = os.Open(filename)
	if e != nil {
		return
	}
	h := md5.New()
	_, e = io.Copy(h, f)
	f.Close()
	if e != nil {
		return
	}
	hash = h.Sum(nil)
	return
}
