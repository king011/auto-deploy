package rpc

import (
	"context"
	"crypto/tls"
	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"gitlab.com/king011/auto-deploy/service/configure"
	"gitlab.com/king011/auto-deploy/service/logger"
	"gitlab.com/king011/auto-deploy/service/utils"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"
	"os"
	"strings"
	"time"
)

var _Client *grpc.ClientConn

// Single .
func Single() *grpc.ClientConn {
	return _Client
}

// Init 初始化 rpc
func Init(cnf *configure.Server) {
	keys, e := utils.GetKeys()
	if e != nil {
		logger.Fatal("init rpc error",
			zap.Error(e),
		)
		os.Exit(1)
		return
	}
	appkey := ""
	switch len(keys) {
	case 0:
		// = ""
	case 1:
		appkey = keys[0]
	case 2:
		appkey = keys[0] + "-" + keys[1]
	case 3:
		appkey = keys[0] + "-" + keys[1] + "-" + keys[2]
	default:
		appkey = strings.Join(keys, "-")
	}

	opts := make([]grpc.DialOption, 0, 10)
	opts = append(opts,
		grpc.WithPerRPCCredentials(client.NewToken(cnf.SSL, appkey)),
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time: time.Second * 20,
		}),
	)
	if cnf.SSL {
		creds := credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: cnf.SkipVerify,
		})
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}
	_Client, e = grpc.Dial(
		cnf.Addr,
		opts...,
	)
	if e != nil {
		logger.Fatal("init rpc error",
			zap.Error(e),
		)
		os.Exit(1)
		return
	}
	if cnf.Ping != 0 {
		go runPing(cnf.Ping)
	}
}
func runPing(interval time.Duration) {
	for {
		time.Sleep(interval)
		client := grpc_app.NewAppClient(_Client)
		client.Ping(context.Background(), &grpc_app.PingRequest{})
	}
}
