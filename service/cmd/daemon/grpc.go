package daemon

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"runtime"
	"strings"
	"time"

	grpc_forward "gitlab.com/king011/auto-deploy/protocol/forward"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/daemon/server"
	"gitlab.com/king011/auto-deploy/service/cmd/daemon/update"
	"gitlab.com/king011/auto-deploy/service/configure"
	"gitlab.com/king011/auto-deploy/service/db/manipulator"
	"gitlab.com/king011/auto-deploy/service/logger"
	"gitlab.com/king011/auto-deploy/service/process"
	"gitlab.com/king011/auto-deploy/service/status"
	"gitlab.com/king011/auto-deploy/service/utils"
	king_timer "gitlab.com/king011/king-go/timer"
	"go.uber.org/zap"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

func grpcInterceptor(ctx context.Context,
	request interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler) (reply interface{}, e error) {

	e = utils.CheckAdmin(ctx)
	if e != nil {
		return
	}

	reply, e = handler(ctx, request)
	return
}
func grpcInterceptorStream(srv interface{},
	ss grpc.ServerStream,
	info *grpc.StreamServerInfo,
	handler grpc.StreamHandler) (e error) {

	e = utils.CheckAdmin(ss.Context())
	if e != nil {
		return
	}

	e = handler(srv, ss)
	return
}
func runGRPC(cnf *configure.Service, disable bool) {
	// 創建 grpc 服務器
	var gs *grpc.Server
	safe := cnf.TLS()
	if safe {
		creds, e := credentials.NewServerTLSFromFile(cnf.Cert, cnf.Key)
		if e != nil {
			logger.Fatal(e.Error())
		}
		gs = grpc.NewServer(
			// 設置 tls
			grpc.Creds(creds),
			//為服務 設置 攔截器
			grpc.UnaryInterceptor(grpcInterceptor),
			grpc.StreamInterceptor(grpcInterceptorStream),
		)
	} else {
		gs = grpc.NewServer(
			//為服務 設置 攔截器
			grpc.UnaryInterceptor(grpcInterceptor),
			grpc.StreamInterceptor(grpcInterceptorStream),
		)
	}

	//創建 監聽 Listener
	l, e := net.Listen("tcp", cnf.LAddr)
	if e != nil {
		logger.Fatal(e.Error())
	}
	defer l.Close()

	// 初始化 檔案系統
	var pkgs []string
	pkgs, e = manipulator.InitFilesystem()
	if e != nil {
		logger.Fatal(e.Error())
	}
	// 運行 進程
	for i := 0; i < len(pkgs); i++ {
		if e := process.Run(pkgs[i]); e == nil {
			log.Println("run package " + pkgs[i])
			logger.Info("run package " + pkgs[i])
		}
	}
	// 啓動 自動 更新
	apps := configure.Single().Apps
	if apps.DisableUpdate {
		logger.Warn("disabled auto update")
	} else {
		updates := apps.Update
		for i := 0; i < len(updates); i++ {
			if updates[i] == "" {
				continue
			}
			var crontab king_timer.Crontab
			str := updates[i]
			et := crontab.FromString(str)
			if et == nil {
				if logger.OutputFile {
					fmt.Println("auto update on : " + str)
				}
				logger.Info("auto update on : " + str)
				crontab.Run(func(c *king_timer.Crontab, t time.Time) (_e error) {
					if status.IsExit() {
						c.Close()
						return
					}
					update.Done(str)
					return
				})
			} else {
				logger.Warn("init update timer error",
					zap.Error(et),
					zap.String("val", str),
				)
			}
		}
	}
	// go func() {
	// 	time.Sleep(time.Second)
	// 	update.Done("test update")
	// }()
	if safe {
		if logger.OutputFile {
			log.Println("grpc h2 work at " + cnf.LAddr)
		}
		logger.Info("grpc h2 work at " + cnf.LAddr)
	} else {
		if logger.OutputFile {
			log.Println("grpc h2c work at " + cnf.LAddr)
		}
		logger.Info("grpc h2c work at " + cnf.LAddr)
	}
	// 註冊 grpc 服務
	grpc_service.RegisterServiceServer(gs, server.Service{})
	grpc_forward.RegisterForwardServer(gs, server.Forward{})

	// 註冊 反射 到 服務 路由
	reflection.Register(gs)

	ch := make(chan interface{})
	server.SignalExit = ch
	go func() {
		<-ch
		// 設置 退出 狀態
		status.SetExit()
		// 結束 進程
		process.Exit()
		time.Sleep(time.Second)
		gs.Stop()
	}()
	if !disable {
		go debugConsole(gs, ch)
	}
	// 讓 rpc 在 Listener 上 工作
	if e := gs.Serve(l); e != nil {
		logger.Fatal(e.Error())
	}
	logger.Info("service exit")
}
func debugHelp() {
	fmt.Print(`h		show help
e/exit/q/quit	exit service
runtime		show runtime info
`)
}
func debugConsole(gs *grpc.Server, ch chan interface{}) {
	r := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("\n$ > ")
		b, _, _ := r.ReadLine()
		str := strings.TrimSpace(string(b))
		if str == "e" ||
			str == "exit" ||
			str == "q" ||
			str == "quit" {
			close(ch)
			break
		} else if str == "h" {
			debugHelp()
		} else if str == "runtime" {
			fmt.Println(runtime.GOOS, runtime.GOARCH, runtime.Version())
			fmt.Println("cpu       :", runtime.NumCPU())
			fmt.Println("threads   :", runtime.GOMAXPROCS(0))
			fmt.Println("cgo       :", runtime.NumCgoCall())
			fmt.Println("goroutine :", runtime.NumGoroutine())
			fmt.Println("Store :", manipulator.Store())

		}
	}
}
