package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"gitlab.com/king011/auto-deploy/service/db/data"
	"golang.org/x/net/context"
	"log"
)

func init() {
	var filename string
	cmd := &cobra.Command{
		Use:   "show",
		Short: "display detailed information about a package",
		Long: `display detailed information about a package
		
   # display package gcc cmake
   service show gcc cmake
`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				return
			}

			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
			}
			defer conn.Close()
			client := grpc_service.NewServiceClient(conn)
			var reply *grpc_service.ShowReply
			reply, e = client.Show(context.Background(),
				&grpc_service.ShowRequest{
					Name: args,
				},
			)
			if e != nil {
				log.Fatalln(e)
			}
			if len(reply.Data) == 0 {
				fmt.Println("not found any application")
			} else {
				for i := 0; i < len(reply.Data); i++ {
					var app data.App
					app.FromPB(reply.Data[i])

					fmt.Println("Package    :", app.Name)
					fmt.Println("Version    :", app.Version)
					fmt.Println("Dev        :", app.Dev)
					fmt.Println("RunMode    :", app.RunMode)
					fmt.Println("UpdateMode :", app.UpdateMode)
					fmt.Println("Run        :", app.Run)
					fmt.Println("Install    :", app.Install)
					fmt.Println(app.Description)
					fmt.Println()
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	rootCmd.AddCommand(cmd)
}
