package cmd

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	grpc_forward "gitlab.com/king011/auto-deploy/protocol/forward"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"gitlab.com/king011/auto-deploy/utils"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"io"
	"log"
	"os"
	"strings"
)

func init() {
	var filename string
	var dev, yes bool
	var pkg, version, tarfile, description string
	cmd := &cobra.Command{
		Use:   "install",
		Short: "install packages",
		Long: `install packages

   # instal gcc cmake
   service install gcc cmake -y

   # install gcc version 5.4.0
   service install -p gcc -v v5.4.0

   # install gcc by local tar file
   service install -t gcc.tar.gz -d "c/c++ dev tools"
`,
		Run: func(cmd *cobra.Command, args []string) {
			pkg = strings.TrimSpace(pkg)
			version = strings.TrimSpace(version)
			tarfile = strings.TrimSpace(tarfile)
			description = strings.TrimSpace(description)
			if tarfile != "" {
				installByTar(filename, tarfile, description)
			} else if pkg != "" && version != "" {
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()

				client := grpc_service.NewServiceClient(conn)
				installApp(client, pkg, version, false, yes)
			} else {
				if len(args) == 0 {
					cmd.Help()
					return
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()

				fmt.Println("searching packages")
				arrs := installSearch(conn, args, dev).Data
				if len(arrs) == 0 {
					log.Fatalln("not found any application")
					return
				}
				for _, node := range arrs {
					if node.Dev {
						fmt.Printf("%s [%s] dev\n", node.Package, node.Version)
					} else {
						fmt.Printf("%s [%s]\n", node.Package, node.Version)
					}
					fmt.Println(node.Hash)
					fmt.Println(node.Description)
				}
				client := grpc_service.NewServiceClient(conn)
				for _, node := range arrs {
					installApp(client, node.Package, node.Version, node.Dev, yes)
				}
			}
		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.BoolVar(&dev,
		"dev",
		false,
		"include development package",
	)
	flags.BoolVarP(&yes,
		"yes", "y",
		false,
		"auto yes install",
	)
	flags.StringVarP(&pkg,
		"package", "p",
		"",
		"specified package,must be used with version",
	)
	flags.StringVarP(&tarfile,
		"tar", "t",
		"",
		"install by local tar file",
	)
	flags.StringVarP(&description,
		"description", "d",
		"",
		"description for tar",
	)

	flags.StringVarP(&version,
		"version", "v",
		"",
		"specified version,must be used with package",
	)
	rootCmd.AddCommand(cmd)
}

func installSearch(conn *grpc.ClientConn, name []string, dev bool) (reply *grpc_app.SearchReply) {
	client := grpc_forward.NewForwardClient(conn)
	var e error
	reply, e = client.Search(context.Background(),
		&grpc_app.SearchRequest{
			Name: name,
			Dev:  dev,
		})
	if e != nil {
		log.Fatalln(e)
	}

	return
}
func installByTar(filename, tarfile, description string) {
	b, e := utils.UnpackTarGzipFile(tarfile, "auto-deploy.json")
	if e != nil {
		log.Fatalln(e)
	}
	version := &data.Version{}
	e = json.Unmarshal(b, version)
	if e != nil {
		log.Fatalln(e)
	}
	e = version.Format()
	if e != nil {
		log.Fatalln(e)
	}
	var conn *grpc.ClientConn
	conn, e = client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	client := grpc_service.NewServiceClient(conn)
	var stream grpc_service.Service_InstallTarClient
	stream, e = client.InstallTar(context.Background())
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println("send package info")
	e = stream.Send(&grpc_service.InstallTarRequest{
		Package:     version.Package,
		Version:     version.Name,
		Description: description,
		Dev:         version.Dev,
	})
	if e != nil {
		log.Fatalln(e)
	}
	var reply *grpc_service.InstallTarReply
	reply, e = stream.Recv()
	if e != nil {
		log.Fatalln(e)
	}
	switch reply.Status {
	case grpc_service.InstallTarReply_Locked:
		fmt.Println("locked", version.Package)
	case grpc_service.InstallTarReply_Installed:
		fmt.Printf("%s [%s] already installed\n", version.Package, reply.Data)
		return
	default:
		log.Fatalln("bad status", reply.Status)
	}
	// 上傳 數據
	var f *os.File
	f, e = os.Open(tarfile)
	if e != nil {
		log.Fatalln(e)
	}
	var info os.FileInfo
	info, e = f.Stat()
	if e != nil {
		log.Fatalln(e)
	}
	strSize := utils.SizeString(info.Size())
	b = make([]byte, 1024*16)
	var n int
	var sum int64
	last := 0
	for {
		n, e = f.Read(b)
		if e != nil {
			if e == io.EOF {
				break
			}
			if last != 0 {
				last = 0
				fmt.Println()
			}
			log.Fatalln(e)
		}
		if n == 0 {
			continue
		}

		e = stream.Send(&grpc_service.InstallTarRequest{
			Data: b[:n],
		})
		if e != nil {
			if last != 0 {
				last = 0
				fmt.Println()
			}
			log.Fatalln(e)
		}
		sum += int64(n)
		last = utils.Rprintf(last, "upload %v / %v ", strSize, utils.SizeString(sum))
	}
	if last != 0 {
		last = 0
		fmt.Println()
	}
	f.Close()
	// 通知 上傳 完成
	e = stream.Send(&grpc_service.InstallTarRequest{
		Complete: true,
	})
	if e != nil {
		log.Fatalln(e)
	}
	// 等待安裝 完成
	for {
		reply, e = stream.Recv()
		if e != nil {
			log.Fatalln(e)
		}
		switch reply.Status {
		case grpc_service.InstallTarReply_Success:
			fmt.Println("install success")
			return
		case grpc_service.InstallTarReply_Installing:
			fmt.Println("installing : ", version.Package, version.Name)
		default:
			log.Fatalln("bad status", reply.Status, reply.Data)
		}
	}
}
func installApp(client grpc_service.ServiceClient, pkg, version string, dev, yes bool) {
	if dev {
		fmt.Printf("install %s [%s] dev <yes/no> [yes] ", pkg, version)
	} else {
		fmt.Printf("install %s [%s] <yes/no> [yes] ", pkg, version)
	}
	if yes {
		fmt.Println()
		installAppDone(client, pkg, version)
	} else {
		r := bufio.NewReader(os.Stdin)
		b, _, e := r.ReadLine()
		if e != nil {
			log.Fatalln(e)
		}
		str := strings.ToLower(strings.TrimSpace(string(b)))
		if str == "" ||
			str == "yes" || str == "y" ||
			str == "true" || str == "1" {
			installAppDone(client, pkg, version)
		} else {
			fmt.Println("cancel install")
		}
	}
}
func installAppDone(client grpc_service.ServiceClient, pkg, version string) {
	stream, e := client.Install(context.Background())
	if e != nil {
		log.Fatalln(e)
	}
	defer stream.CloseSend()

	e = stream.Send(&grpc_service.InstallRequest{
		Package: pkg,
		Version: version,
	})
	if e != nil {
		log.Fatalln(e)
	}
	var reply *grpc_service.InstallReply
	last := 0

	for {
		reply, e = stream.Recv()
		if e != nil {
			log.Fatalln(e)
		}
		if reply.Status != grpc_service.InstallReply_Downloading {
			if last != 0 {
				last = 0
				fmt.Println()
			}
		}
		switch reply.Status {
		case grpc_service.InstallReply_Lock:
			fmt.Println("lock :", pkg)
		case grpc_service.InstallReply_Installed:
			fmt.Printf("%s [%s] already installed\n", pkg, reply.Data)
			return
		case grpc_service.InstallReply_Searching:
			fmt.Println("find :", pkg, version)
		case grpc_service.InstallReply_Downloading:
			last = utils.Rprint(last, "down : ", reply.Data, " ")
		case grpc_service.InstallReply_Installing:
			fmt.Println("installing : ", pkg, version)
		case grpc_service.InstallReply_Success:
			fmt.Println("install success")
			return
		default:
			log.Println("unknow status", reply.Status, reply.Data)
		}
	}
}
