package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"golang.org/x/net/context"
	"log"
)

func init() {
	var filename string
	cmd := &cobra.Command{
		Use:   "start",
		Short: "start package",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				return
			}
			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
			}
			defer conn.Close()
			client := grpc_service.NewServiceClient(conn)
			for _, name := range args {
				_, e := client.App(context.Background(),
					&grpc_service.AppRequest{
						Name: name,
						Run:  true,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Println("started", name)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	rootCmd.AddCommand(cmd)
}
