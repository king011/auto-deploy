package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/king011/auto-deploy/service/cmd/daemon"
)

func init() {
	var filename string
	var disable bool
	cmd := &cobra.Command{
		Use:   "daemon",
		Short: "run service as a daemon",
		Run: func(cmd *cobra.Command, args []string) {
			daemon.Run(filename, disable)
		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.BoolVarP(&disable,
		"disable", "d",
		false,
		"disable debug console",
	)

	rootCmd.AddCommand(cmd)
}
