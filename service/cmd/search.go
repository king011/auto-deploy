package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	grpc_service "gitlab.com/king011/auto-deploy/protocol/service"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"golang.org/x/net/context"
	"log"
)

func init() {
	var filename string
	var list bool
	cmd := &cobra.Command{
		Use:   "search",
		Short: "search package",
		Long: `search package
		
   # search package gcc cmake
   service search gcc cmake

   # search package gcc cmake and display detailed information
   service search gcc cmake -l
`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				return
			}
			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
			}
			defer conn.Close()
			client := grpc_service.NewServiceClient(conn)
			if list {
				var reply *grpc_service.FindExReply
				reply, e = client.FindEx(context.Background(),
					&grpc_service.FindExRequest{
						Name: args,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}
				if len(reply.Data) == 0 {
					fmt.Println("not found any package")
				} else {
					for _, node := range reply.Data {
						var str string
						if node.Installed {
							str = "i"
						} else {
							str = "p"
						}
						fmt.Printf("%-3v %-20v %v\n", str, node.Name, node.Description)
						for i := 0; i < len(node.Data); i++ {
							v := node.Data[i]
							var dev, installed string
							if v.Dev {
								dev = "dev"
							}
							if v.Installed {
								installed = "**"
							} else {
								installed = "--"
							}
							fmt.Printf("    %v  %-10v %-5v %v\n", installed, v.Name, dev, v.Description)
						}
					}
				}
			} else {
				var reply *grpc_service.FindReply
				reply, e = client.Find(context.Background(),
					&grpc_service.FindRequest{
						Name: args,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}
				if len(reply.Data) == 0 {
					fmt.Println("not found any package")
				} else {
					for _, node := range reply.Data {
						var str string
						if node.Installed {
							str = "i"
						} else {
							str = "p"
						}
						fmt.Printf("%-3v %-20v %v\n", str, node.Name, node.Description)
					}
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.BoolVarP(&list,
		"list", "l",
		false,
		"display detailed information",
	)
	rootCmd.AddCommand(cmd)
}
