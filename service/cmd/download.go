package cmd

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	grpc_forward "gitlab.com/king011/auto-deploy/protocol/forward"
	"gitlab.com/king011/auto-deploy/service/cmd/client"
	"gitlab.com/king011/auto-deploy/utils"
	"google.golang.org/grpc"
	"log"
	"os"
	"strings"
)

func init() {
	var filename string
	var pkg, version, dst string
	cmd := &cobra.Command{
		Use:   "download",
		Short: "download packages",
		Run: func(cmd *cobra.Command, args []string) {
			pkg = strings.TrimSpace(pkg)
			version = strings.TrimSpace(version)
			dst = strings.TrimSpace(dst)

			if pkg == "" || version == "" {
				cmd.Help()
				return
			}

			if dst == "" {
				dst = fmt.Sprintf("%s-%s.tar.gz", pkg, version)
			}
			e := downloadApp(filename, pkg, version, dst)
			if e != nil {
				log.Fatalln(e)
			}

		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.StringVarP(&pkg,
		"package", "p",
		"",
		"package name",
	)
	flags.StringVarP(&version,
		"version", "v",
		"",
		"package version",
	)
	flags.StringVarP(&dst,
		"dst", "d",
		"",
		"local dst filename",
	)
	rootCmd.AddCommand(cmd)
}
func downloadApp(filename, pkg, version, dst string) (e error) {
	fmt.Println("create dst", dst)
	var f *os.File
	f, e = os.Create(dst)
	if e != nil {
		return
	}
	var conn *grpc.ClientConn
	defer func() {
		if conn != nil {
			conn.Close()
		}
		f.Close()
		if e == nil {
			fmt.Println("download complete")
		} else {
			os.Remove(dst)
		}
	}()
	conn, e = client.InitAndGet(filename)
	if e != nil {
		return
	}
	client := grpc_forward.NewForwardClient(conn)
	var stream grpc_forward.Forward_DownloadClient
	stream, e = client.Download(context.Background())
	if e != nil {
		return
	}
	e = stream.Send(&grpc_app.DownloadRequest{
		Package: pkg,
		Version: version,
	})
	if e != nil {
		return
	}
	var reply *grpc_app.DownloadReply
	fmt.Println("start download")
	last := 0
	var size, n int64
	var sizeStr string
	for {
		reply, e = stream.Recv()
		if e != nil {
			return
		}
		if reply.Size != 0 {
			sizeStr = utils.SizeString(reply.Size)
		}

		n = int64(len(reply.Data))
		if n != 0 {
			size += n
			if sizeStr == "" {
				last = utils.Rprint(last, "recv data : ", utils.SizeString(size), " ")
			} else {
				last = utils.Rprintf(last, "recv data : %s / %s ", utils.SizeString(size), sizeStr)
			}
			_, e = f.Write(reply.Data)
			if e != nil {
				return
			}
		}

		if reply.Complete {
			break
		}
	}
	if last != 0 {
		fmt.Println()
	}
	return
}
