package process

import (
	"sync"
)

var _Monitor = &Monitor{
	process: make(map[int]exitfs),
}

type exitf func()
type exitfs []exitf

// Monitor 監控 進程 退出 狀態
type Monitor struct {
	sync.Mutex
	process map[int]exitfs
}

// Init 初始化
func (m *Monitor) Init() {
	m.process = make(map[int]exitfs)
}

// Started 進程已啓動
func (m *Monitor) Started(pid int) {
	m.Lock()
	_, ok := m.process[pid]
	if !ok {
		m.process[pid] = nil
	}
	m.Unlock()
}

// Stopped 進程已退出
func (m *Monitor) Stopped(pid int) {
	m.Lock()
	fs, ok := m.process[pid]
	if ok && len(fs) != 0 {
		for i := 0; i < len(fs); i++ {
			fs[i]()
		}
	}
	delete(m.process, pid)
	m.Unlock()
}

// OnStopped 註冊 回調函數 當進程退出/或已經退出 回調
func (m *Monitor) OnStopped(pid int, f func()) {
	m.Lock()
	fs, ok := m.process[pid]
	if ok {
		if fs == nil {
			// 不存在 數組 創建回調 數組
			fs = make([]exitf, 1, 5)
			fs[0] = f
			m.process[pid] = fs
		} else {
			// 添加 回調
			fs = append(fs, f)
		}
	} else {
		// 進程 不存在 直接 回調
		f()
	}
	m.Unlock()
}
