package process

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	duk "gitlab.com/king011/auto-deploy/duktape"
	"gitlab.com/king011/auto-deploy/service/db/data"
	"gitlab.com/king011/auto-deploy/service/db/manipulator"
	"gitlab.com/king011/auto-deploy/service/logger"
	"gitlab.com/king011/auto-deploy/service/status"
	"go.uber.org/zap"
)

type _System struct {
	sync.Mutex
	m map[string]*Application
}

var _Single = &_System{
	m: make(map[string]*Application),
}

// Run 運行 套件
func Run(pkg string) (e error) {
	if status.IsExit() {
		e = fmt.Errorf("Service Exiting, can't Run %v", pkg)
		return
	}
	e = _Single.Run(pkg)
	return
}

// GetCommand 返回 套件 啓動 命令
func GetCommand(pkg string) (name string, args []string, dir string, e error) {
	if status.IsExit() {
		e = fmt.Errorf("Service Exiting, can't GetCommand %v", pkg)
		return
	}
	name, args, dir, e = _Single.GetCommand(pkg)
	return
}

// Stop 停止 一個套件
func Stop(pkg string) (e error) {
	e = _Single.Stop(pkg)
	return
}

// Exit 退出 所有 進程 並等待 其結束
//
// Exit 必須在 調用 status.SetExit() 之後 才可調用
func Exit() {
	_Single.Exit()
}

// StopAndWait 停止 並等待 套件 結束
func StopAndWait(pkg string) {
	if pkg == "" {
		return
	}
	var mApp manipulator.App
	mApp.SetStop(pkg)

	_Single.Stop(pkg)

	var yes bool
	var e error
	//last := time.Now().Add(configure.Single().Apps.Kill)
	//for time.Now().After(last) {
	//fmt.Println("begin stop", pkg)
	for {
		yes, e = mApp.IsStopped(pkg)
		if e != nil {
			time.Sleep(time.Millisecond * 100)
			continue
		}
		if yes {
			break
		}
		time.Sleep(time.Millisecond * 100)
	}
	//fmt.Println("end stop", pkg)
	time.Sleep(time.Millisecond * 100)
}
func (s *_System) Exit() {
	// 獲取 所有 運行的 進程
	var pkgs []string
	s.Lock()
	if len(s.m) != 0 {
		pkgs = make([]string, 0, len(s.m))
		for pkg := range s.m {
			pkgs = append(pkgs, pkg)
		}
	}
	s.Unlock()

	// 退出 進程
	for _, pkg := range pkgs {
		StopAndWait(pkg)
	}
}
func (s *_System) GetCommand(pkg string) (name string, args []string, dir string, e error) {
	s.Lock()
	// 鎖定 運行
	var mApp manipulator.App
	var status data.RunStatus
	var mode data.RunMode
	var version string
	status, mode, version, e = mApp.LockRun(pkg)
	// 創建 新 app 實例
	app := &Application{
		Name:   pkg,
		Mode:   mode,
		Status: appStart,
	}
	if e != nil {
		s.Unlock()
		logger.Warn("command app error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	s.Unlock()
	defer mApp.RunError(pkg, status)

	dir = manipulator.AppDir(pkg, version)
	buf := bytes.NewBufferString(`(function(){
	"use strict";
	const __dirname="` + dir + `";
	const __service=false;
	var exports = {};
	(function(){
`)

	filename := dir + "/process.js"
	var f *os.File
	f, e = os.Open(filename)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	_, e = io.Copy(buf, f)
	f.Close()
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	_, e = buf.WriteString(`
	})();
	return exports;
})();`)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	// 執行 配置 js
	ctx := duk.New()
	if ctx == nil {
		logger.Warn("can't init duk",
			zap.String("package", pkg),
		)
	}
	defer ctx.DestroyHeap()
	if e = ctx.PevalString(buf.String()); e != nil {
		ctx.Pop()
		logger.Warn("can't init duk",
			zap.String("package", pkg),
		)
		return
	}
	if ctx.IsObject(-1) {
		ctx.GetPropString(-1, "Service")
		n := ctx.GetLength(-1)
		if n > 0 {
			app.Process = make([]*Process, 0, n)
			if ctx.IsArray(-1) {
				for i := 0; i < n; i++ {
					ctx.GetPropIndex(-1, uint(i))
					if ctx.IsObject(-1) {
						p := s.getProcess(ctx)
						if p != nil {
							app.Process = append(app.Process, p)
						}
					}
					ctx.Pop()
				}
			}
		}
		ctx.Pop()
	}
	ctx.Pop()
	if len(app.Process) == 0 {
		e = fmt.Errorf("package [%s] not define process", pkg)
		return
	}

	// 運行 套件
	if len(app.Process) == 0 {
		e = fmt.Errorf("package [%s] not found process define", pkg)
		return
	}
	p := app.Process[0]
	name = p.Start[0]
	args = p.Start[1:]
	dir = p.Dir

	return
}
func (s *_System) Run(pkg string) (e error) {
	s.Lock()
	// 驗證 是否 已經 創建 運行實例
	app, ok := s.m[pkg]
	if ok {
		app.Lock()
		status := app.Status
		app.Unlock()
		if status != appNone {
			s.Unlock()
			e = fmt.Errorf("%v already running [%v]", pkg, status)
			logger.Warn("run app error",
				zap.Error(e),
				zap.String("package", pkg),
				zap.Uint("status", status),
			)
			return
		}
	}
	// 鎖定 運行
	var mApp manipulator.App
	var status data.RunStatus
	var mode data.RunMode
	var version string
	status, mode, version, e = mApp.LockRun(pkg)
	// 創建 新 app 實例
	app = &Application{
		Name:   pkg,
		Mode:   mode,
		Status: appStart,
	}
	if e != nil {
		s.Unlock()
		logger.Warn("run app error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	s.m[pkg] = app
	s.Unlock()
	defer func() {
		if e == nil {
			e = mApp.RunSuccess(pkg)
		} else {
			mApp.RunError(pkg, status)
			s.Lock()
			delete(s.m, pkg)
			s.Unlock()
		}
	}()
	dir := manipulator.AppDir(pkg, version)
	buf := bytes.NewBufferString(`(function(){
	"use strict";
	const __dirname="` + dir + `";
	const __service=true;
	var exports = {};
	(function(){
`)

	filename := dir + "/process.js"
	var f *os.File
	f, e = os.Open(filename)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	_, e = io.Copy(buf, f)
	f.Close()
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	_, e = buf.WriteString(`
	})();
	return exports;
})();`)
	if e != nil {
		logger.Warn("system error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	// 執行 配置 js
	ctx := duk.New()
	if ctx == nil {
		logger.Warn("can't init duk",
			zap.String("package", pkg),
		)
	}
	defer ctx.DestroyHeap()
	if e = ctx.PevalString(buf.String()); e != nil {
		ctx.Pop()
		logger.Warn("can't init duk",
			zap.String("package", pkg),
		)
		return
	}
	if ctx.IsObject(-1) {
		ctx.GetPropString(-1, "Service")
		n := ctx.GetLength(-1)
		if n > 0 {
			app.Process = make([]*Process, 0, n)
			if ctx.IsArray(-1) {
				for i := 0; i < n; i++ {
					ctx.GetPropIndex(-1, uint(i))
					if ctx.IsObject(-1) {
						p := s.getProcess(ctx)
						if p != nil {
							app.Process = append(app.Process, p)
						}
					}
					ctx.Pop()
				}
			}
		}
		ctx.Pop()
	}
	ctx.Pop()
	if len(app.Process) == 0 {
		e = fmt.Errorf("package [%s] not define process", pkg)
		return
	}

	// 運行 套件
	e = app.Run()
	if e != nil {
		logger.Warn("run app error",
			zap.Error(e),
			zap.String("package", pkg),
		)
		return
	}
	// 監聽 停止
	go app.Monitor()
	return
}
func (s *_System) getProcess(ctx *duk.Context) (p *Process) {
	// start
	tmp := &Process{}
	ctx.GetPropString(-1, "Start")
	if !ctx.IsArray(-1) {
		ctx.Pop()
		return
	}
	n := ctx.GetLength(-1)
	if n < 1 {
		ctx.Pop()
		return
	}
	tmp.Start = make([]string, 0, n)
	for i := 0; i < n; i++ {
		ctx.GetPropIndex(-1, uint(i))
		str := ctx.SafeToString(-1)
		tmp.Start = append(tmp.Start, str)
		ctx.Pop()
	}
	ctx.Pop()
	if !filepath.IsAbs(tmp.Start[0]) {
		if str, e := filepath.Abs(tmp.Start[0]); e == nil {
			tmp.Start[0] = str
		} else {
			logger.Error("system error",
				zap.Error(e),
			)
		}
	}
	p = tmp

	// dir
	ctx.GetPropString(-1, "Dir")
	if ctx.IsString(-1) {
		tmp.Dir = strings.TrimSpace(ctx.ToString(-1))
		if !filepath.IsAbs(tmp.Dir) {
			if str, e := filepath.Abs(tmp.Dir); e == nil {
				tmp.Dir = str
			} else {
				logger.Error("system error",
					zap.Error(e),
				)
			}
		}
	}
	ctx.Pop()

	// stop
	ctx.GetPropString(-1, "Stop")
	if !ctx.IsArray(-1) {
		ctx.Pop()
		return
	}
	n = ctx.GetLength(-1)
	if n < 1 {
		ctx.Pop()
		return
	}
	tmp.Stop = make([]string, 0, n)
	for i := 0; i < n; i++ {
		ctx.GetPropIndex(-1, uint(i))
		str := ctx.SafeToString(-1)
		tmp.Stop = append(tmp.Stop, str)
		ctx.Pop()
	}
	ctx.Pop()
	return
}

func (s *_System) Stop(pkg string) (e error) {
	s.Lock()
	app, ok := s.m[pkg]
	s.Unlock()
	if !ok {
		if !status.IsExit() {
			logger.Warn("stop application not found",
				zap.String("package", pkg),
			)
		}
		return
	}

	app.Stop()
	return
}
