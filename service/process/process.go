package process

import (
	"context"
	"fmt"
	"gitlab.com/king011/auto-deploy/service/configure"
	"gitlab.com/king011/auto-deploy/service/db/data"
	"gitlab.com/king011/auto-deploy/service/db/manipulator"
	"gitlab.com/king011/auto-deploy/service/logger"
	"go.uber.org/zap"
	//"os"
	"os/exec"
	"sync"
	"time"
)

// Process 一個 運行的 程式
type Process struct {
	sync.Mutex
	Cmd *exec.Cmd
	// 啓動 指令
	Start []string
	// 退出 指令 如果爲空 強制結束
	Stop []string
	// 工作目錄
	Dir string

	Error error

	done chan interface{}
	exit bool
}

// Fault 啓動錯誤 退出程式
func (p *Process) Fault() {
	p.Lock()
	if p.exit {
		p.Unlock()
		return
	}
	p.done = nil
	p.exit = true

	// kill
	cmd := p.Cmd
	p.Unlock()
	if cmd == nil {
		return
	}
	p.kill(cmd)
}

// Exit 退出程式
func (p *Process) Exit() {
	p.Lock()
	if p.exit {
		p.Unlock()
		return
	}
	p.exit = true

	// kill
	cmd := p.Cmd
	p.Unlock()
	if cmd == nil {
		return
	}
	p.kill(cmd)
}
func (p *Process) kill(cmd *exec.Cmd) {
	if len(p.Stop) == 0 {
		cmd.Process.Kill()
	} else {
		d := time.Now().Add(configure.Single().Apps.Kill)
		e := p.execDeadline(d, p.Stop[0], p.Stop[1:]...)
		if e == nil {
			var timeout time.Duration
			now := time.Now()
			if d.After(now) {
				timeout = d.Sub(now)
			} else {
				timeout = time.Second
			}
			ch := make(chan interface{})
			t := time.NewTimer(timeout)
			_Monitor.OnStopped(cmd.Process.Pid, func() {
				close(ch)
			})
			select {
			case <-ch:
				// 進程 已退出 停止 超時 定時器
				if !t.Stop() {
					<-t.C
				}
			case <-t.C:
				// 超時 強制 結束 進程
				cmd.Process.Kill()
			}
		} else {
			cmd.Process.Kill()
		}
	}
}
func (p *Process) execDeadline(d time.Time, name string, args ...string) (e error) {
	ctx, cancel := context.WithDeadline(context.Background(), d)
	defer cancel()
	cmd := exec.CommandContext(ctx, name, args...)
	e = cmd.Run()
	if e != nil {
		return
	}
	return
}

// Run 運行 子進程
func (p *Process) Run(mode data.RunMode) (e error) {
	var sleep time.Duration
	for {
		p.Lock()
		exit := p.exit
		p.Unlock()
		if exit {
			e = fmt.Errorf("cancel")
			return
		}
		if e = p.run(mode); e == nil {
			// 運行 成功 next
			break
		}
		logger.Warn("new process error",
			zap.Error(e),
			zap.Strings("process", p.Start),
		)
		if mode != data.RunKeep {
			// 運行 失敗
			return
		}
		// 重新運行
		if sleep == 0 {
			sleep = time.Second
		} else if sleep < time.Second*16 {
			sleep *= 2
		}
		logger.Debug("later try process",
			zap.Strings("process", p.Start),
			zap.Duration("later", sleep),
		)
		time.Sleep(sleep) // 等待 以免 cpu被 佔光
	}
	return
}
func (p *Process) run(mode data.RunMode) (e error) {
	// 啓動 子進程
	cmd := exec.Command(p.Start[0], p.Start[1:]...)
	cmd.Dir = p.Dir
	// fmt.Println(cmd.Dir)
	// fmt.Println(cmd.Args)
	// cmd.Stdout = os.Stdout
	// cmd.Stderr = os.Stderr
	// fmt.Println("Start", p.Start)
	// fmt.Println("Stop", p.Stop)
	// fmt.Println("Dir", p.Dir)
	e = cmd.Start()
	if e != nil {
		return
	}
	p.Lock()

	// 通知 創建 子進程
	_Monitor.Started(cmd.Process.Pid)
	p.Cmd = cmd
	p.Unlock()
	go func() {
		// 等待 進程 結束
		p.Error = cmd.Wait()
		// 通知 子進程 結束
		_Monitor.Stopped(cmd.Process.Pid)

		p.Lock()
		p.Cmd = nil
		p.Unlock()

		// 驗證 模式
		if mode == data.RunKeep {
			p.Lock()
			exit := p.exit
			done := p.done
			p.Unlock()
			if exit {
				if done != nil {
					done <- nil
				}
			} else {
				// 重新運行
				p.Run(mode)
			}
		} else {
			p.Lock()
			done := p.done
			p.Unlock()
			if done != nil {
				done <- nil
			}
		}
	}()
	return
}

const (
	// 未 運行
	appNone = iota
	// 啓動中
	appStart
	// 運行中
	appRunning
)

// Application 一組 運行的 套件 程式
type Application struct {
	sync.Mutex
	Status uint
	Name   string
	// 運行 模式
	Mode data.RunMode

	// 子進程
	Process []*Process

	isKill   bool
	waitDone chan interface{}
}

// Monitor 監控 運行 狀態
func (a *Application) Monitor() {
	// 等待所有 子進程 結束
	sum := 0
	for sum != len(a.Process) {
		<-a.waitDone
		sum++
	}

	// 向 數據庫 寫入 停止
	var mApp manipulator.App
	if a.isKill {
		mApp.RunExit(a.Name, data.Kill)
	} else {
		status := data.RunStatus(data.Exit)
		for i := 0; i < len(a.Process); i++ {
			p := a.Process[i]
			p.Lock()
			e := p.Error
			p.Unlock()
			if e != nil {
				status = data.Abort
				break
			}
		}
		mApp.RunExit(a.Name, status)
	}

	a.Lock()
	a.Status = appNone
	a.Unlock()
}

// Run 運行 套件
func (a *Application) Run() (e error) {
	defer func() {
		if e == nil {
			a.Lock()
			a.Status = appRunning
			a.Unlock()
		} else {
			// 錯誤 退出 已運行 程式
			for i := 0; i < len(a.Process); i++ {
				a.Process[i].Fault()
			}
		}
	}()
	a.waitDone = make(chan interface{})
	for i := 0; i < len(a.Process); i++ {
		a.Process[i].done = a.waitDone
		e = a.Process[i].run(a.Mode)
		if e != nil {
			break
		}
	}
	return
}

// Stop 停止 運行的 進程
func (a *Application) Stop() {
	a.Lock()
	if a.Status == appStart {
		a.Status = appNone
		a.Unlock()
		return
	} else if a.Status == appRunning {
		a.Status = appNone
		a.isKill = true
	} else {
		a.Unlock()
		return
	}
	a.Unlock()
	for i := 0; i < len(a.Process); i++ {
		a.Process[i].Exit()
	}
}
