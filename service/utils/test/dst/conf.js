function LoadConf(filename) {
    var s = utils.LoadFile(filename)
    alert("LoadConf :")
    alert(s)
    s = utils.ParseConf(s)
    alert("-> JSON :")
    alert(JSON.stringify(s, undefined, "   "), "\n")
    return s;
}
function toConfSection(arrs, k, section) {
    var comment = section.Comment
    if (comment) {
        if (comment.endsWith("\n")) {
            arrs.push(comment);
        } else {
            arrs.push(comment + "\n");
        }
    }
    if (k != "") {
        arrs.push("[" + k + "]\n")
    }

    var keys = [];
    for (var k in section.Item) {
        keys.push(k)
    }
    if (keys.length == 0) {
        return
    }
    keys.sort()
    for (var i = 0; i < keys.length; i++) {
        var k = keys[i]
        var item = section.Item[k]
        if (item != undefined && item != null) {
            comment = item.Comment
            if (comment) {
                if (comment.endsWith("\n")) {
                    arrs.push(comment);
                } else {
                    arrs.push(comment + "\n");
                }
            }
            var v = item.Val
            if (v != null && v != undefined) {
                arrs.push(k + "=" + v + "\n");
            }
        }
    }
}
function toConf(obj) {
    var arrs = []
    if (obj.main) {
        toConfSection(arrs, "", obj.main)
    }
    if (obj.section) {
        for (var k in obj.section) {
            toConfSection(arrs, k, obj.section[k])
        }
    }
    return arrs.join("");
}

function SaveConf(filename, obj) {
    var s = toConf(obj)
    alert("SaveConf :")
    alert(s, "\n")
    utils.SaveFile(filename, s)
}
function WriteNew(filename) {
    alert("write default configure file :", filename)
}
function MergeFile(src, dst) {
    var obj = LoadConf(src)
    obj.main.Item["http.addr"].Val = "127.0.0.1"
    obj.section.dev.Item["log.trace.output"] = {
        Val: "off",
        Comment: "# 測試\n",
    }
    SaveConf(dst, obj)
}
exports.Merge = function (root, src, dst) {
    var name = "test.conf"
    var oldfile = root + "/" + src + "/" + name
    var newfile = root + "/" + dst + "/" + name
    if (!utils.IsFileExist(oldfile)) {
        WriteNew(newfile)
        return
    }
    MergeFile(oldfile, newfile)
}