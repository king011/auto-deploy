function LoadXML(filename) {
    var s = utils.LoadFile(filename)
    alert("LoadXML :")
    alert(s)
    s = utils.ParseXML(s)
    alert("-> JSON :")
    alert(JSON.stringify(s, undefined, "   "), "\n")
    return s;
}
function toXMLNode(arrs, k, v, pre) {
    var next = pre + "   ";
    if (Array.isArray(v)) {
        arrs.push(pre + "<" + k + ">\n")
        for (var i = 0; i < v.length; i++) {
            toXMLNode(arrs, "item", v[i], next);
        }
        arrs.push(pre + "</" + k + ">\n")
        return
    }
    var t = typeof v
    if (t == "object") {
        arrs.push(pre + "<" + k + ">\n")
        for (var kk in v) {
            toXMLNode(arrs, kk, v[kk], next);
        }
        arrs.push(pre + "</" + k + ">\n")
    } else if (typeof v == "string" || typeof v == "number") {
        arrs.push(pre + "<" + k + ">" + v + "</" + k + ">\n")
    }
}
function toXML(obj) {
    var arrs = []
    for (var k in obj) {
        toXMLNode(arrs, k, obj[k], "")
    }
    return arrs.join("");
}
function SaveXML(filename, obj) {
    var s = toXML(obj)
    alert("SaveXML :")
    alert(s, "\n")
    utils.SaveFile(filename, s)
}
function WriteNew(filename) {
    alert("write default configure file :", filename)
}
function MergeFile(src, dst) {
    var obj = LoadXML(src)
    obj.ID = 1
    SaveXML(dst, obj)
}
exports.Merge = function (root, src, dst) {
    var name = "test.xml"
    var oldfile = root + "/" + src + "/" + name
    var newfile = root + "/" + dst + "/" + name
    if (!utils.IsFileExist(oldfile)) {
        WriteNew(newfile)
        return
    }
    MergeFile(oldfile, newfile)
}