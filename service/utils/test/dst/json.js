function LoadJSON(filename) {
    var s = utils.LoadFile(filename)
    alert("LoadJSON :")
    alert(s, "\n")
    return JSON.parse(s);
}
function SaveJSON(filename, obj) {
    var s = JSON.stringify(obj, undefined, "\t")
    alert("SaveJSON :")
    alert(s, "\n")
    utils.SaveFile(filename, s)
}
function WriteNew(filename) {
    alert("write default configure file :", filename)
}
function MergeFile(src, dst) {
    var obj = LoadJSON(src)
    obj.ID = 1
    SaveJSON(dst, obj)
}
exports.Merge = function (root, src, dst) {
    var name = "test.json"
    var oldfile = root + "/" + src + "/" + name
    var newfile = root + "/" + dst + "/" + name
    if (!utils.IsFileExist(oldfile)) {
        WriteNew(newfile)
        return
    }
    MergeFile(oldfile, newfile)
}