package utils_test

import (
	"gitlab.com/king011/auto-deploy/service/utils"
	"testing"
)

func TestMergeJSON(t *testing.T) {
	root := "test"
	e := utils.Merge(root, "src", "dst", "json.js", true)
	if e != nil {
		t.Fatal(e)
	}
}
func TestMergeExec(t *testing.T) {
	root := "test"
	e := utils.Merge(root, "src", "dst", "exec.js", true)
	if e != nil {
		t.Fatal(e)
	}
}
func TestMergeXML(t *testing.T) {
	root := "test"
	e := utils.Merge(root, "src", "dst", "xml.js", true)
	if e != nil {
		t.Fatal(e)
	}
}

func TestMergeConf(t *testing.T) {
	root := "test"
	e := utils.Merge(root, "src", "dst", "conf.js", true)
	if e != nil {
		t.Fatal(e)
	}
}
func TestInstall(t *testing.T) {
	root := "test"
	e := utils.Install(root, "dst", "install.js", true)
	if e != nil {
		t.Fatal(e)
	}
}
