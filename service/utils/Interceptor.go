package utils

import (
	"errors"
	"gitlab.com/king011/auto-deploy/service/configure"
	"gitlab.com/king011/auto-deploy/service/logger"
	"gitlab.com/king011/auto-deploy/service/status"
	"go.uber.org/zap"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

var errServiceExit = errors.New("service exiting")

// CheckAdmin 驗證是否是 管理員 操作
func CheckAdmin(ctx context.Context) (e error) {
	if status.IsExit() {
		e = errServiceExit
		return
	}
	// 解析metada中的信息并验证
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		e = grpc.Errorf(codes.Unauthenticated, "unknow token")
		logger.Warn("interceptor error",
			zap.Error(e),
		)
		return
	}
	if pwd, ok := md["appkey"]; !ok {
		e = grpc.Errorf(codes.Unauthenticated, "unknow token")
		logger.Warn("interceptor error",
			zap.Error(e),
		)
		return
	} else if len(pwd) == 0 || pwd[0] != configure.Single().Service.Password {
		e = grpc.Errorf(codes.PermissionDenied, "password not match")
		logger.Warn("interceptor error",
			zap.Error(e),
		)
		return
	}
	return
}
