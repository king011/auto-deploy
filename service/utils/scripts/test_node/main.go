package main

import (
	"log"
	"os"
	"os/exec"
	"path/filepath"

	"gitlab.com/king011/auto-deploy/service/utils"
)

func main() {
	path, _ := exec.LookPath(os.Args[0])
	path, _ = filepath.Abs(path)
	path = filepath.Dir(path)

	root := filepath.Dir(path)
	dst := filepath.Base(path)
	e := utils.Install(root, dst, "install.js", true)
	if e != nil {
		log.Fatalln(e)
	}

}
