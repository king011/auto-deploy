import { Cat } from './wrapper/Cat'
import { Dog } from './wrapper/Dog'
import { Utils } from 'auto-deploy-utils'
export function Install(root: string, dst: string) {
    const cat = new Cat("cat")
    cat.Speak()
    const dog = new Dog("dog")
    dog.Speak()

    const utils = Utils as any
    for (let k in utils) {
        const v = utils[k];
        if (typeof v == "function") {
            console.log("utils import :", k, typeof v)
        } else {
            console.log("utils import :", k, typeof v, v)
        }
    }

    let filename = "/home/king/123.txt"
    console.log(Utils.Dir(filename), Utils.Base(filename), Utils.Ext(filename))
    console.log(filename, Utils.IsFileExist(filename))
    let dir = "/home"
    console.log(dir, Utils.IsDirExist(dir))

    console.log(Utils.Clean("/home/king/.."))
    console.log(Utils.Clean("/home"))
    console.log(Utils.IsAbs("."), Utils.IsAbs("/home"))
    console.log(Utils.Abs("."))

    Utils.Copy("wrapper2", "wrapper", true)
    Utils.Remove("wrapper2", true)

    var str = "cerberus is an idea"
    Utils.SaveFile("tmp.file", str)
    if (Utils.LoadFile("tmp.file") != str) {
        throw "err SaveFile LoadFile"
    }
    Utils.Remove("tmp.file")

    console.log(Utils.CompileJSONNET("123"))
    console.log(Utils.CompileJSONNET("local id=123;{ID:id+321}"))

    Utils.Exec("touch", "touch.file")
    if (!Utils.IsFileExist("touch.file")) {
        throw "err Exec touch"
    }
    Utils.Remove("touch.file")
    str = Utils.LoadFile("../../test/dst/test.xml")
    alert(str)
    alert(Utils.CompileXML(str))
    alert(JSON.stringify(Utils.ParseXML(str), null, "\t"))

    str = Utils.LoadFile("../../test/dst/test.conf")
    alert(str)
    alert(Utils.CompileConf(str))
    alert(JSON.stringify(Utils.ParseConf(str), null, "\t"))
}