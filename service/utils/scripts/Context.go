package scripts

import (
	"errors"
	"fmt"

	duk "gitlab.com/king011/auto-deploy/duktape"
)

const (
	// DukErrNone no error (e.g. from duk_get_error_code())
	DukErrNone = 0
	// DukErrError Error
	DukErrError = 1
	// DukErrEvalError EvalError
	DukErrEvalError = 2
	// DukErrRangeError RangeError
	DukErrRangeError = 3
	// DukErrReferenceError ReferenceError
	DukErrReferenceError = 4
	// DukErrSyntaxError SyntaxError
	DukErrSyntaxError = 5
	// DukErrTypeError TypeError
	DukErrTypeError = 6
	// DukErrURIError URIError
	DukErrURIError = 7
)

// Context js 執行環境
type Context struct {
	ctx *duk.Context
}

// New 創建 js 執行環境
func New(root []string, dev bool) *Context {
	c := &Context{
		ctx: duk.New(),
	}
	c.ctx.ModuleNodeInit(root, dev)
	return c
}

// Destory 銷毀執行環境
func (c *Context) Destory() {
	if c.ctx != nil {
		c.ctx.DestroyHeap()
		c.ctx = nil
	}
}

// Merge 執行 合併 腳本
func (c *Context) Merge(filename, root, src, dst, str string) (e error) {
	ctx := c.ctx
	popN := 0
	defer func() {
		if popN > 0 {
			switch popN {
			case 1:
				ctx.Pop()
			case 2:
				ctx.Pop2()
			case 3:
				ctx.Pop3()
			default:
				ctx.PopN(popN)
			}
		}
	}()
	// 執行 腳本
	popN++
	ctx.PushString(str)
	ctx.PushString(filename)
	if e = ctx.Pcompile(0); e != nil {
		//if e = ctx.PevalString(str); e != nil {
		return
	}
	if ctx.Pcall(0) != 0 {
		e = errors.New(ctx.SafeToString(-1))
		return
	}
	if !ctx.IsObject(-1) {
		e = fmt.Errorf("not found exports object")
		return
	}
	popN++
	ctx.GetPropString(-1, "Merge")
	if !ctx.IsFunction(-1) {
		e = fmt.Errorf("not export function Merge(root:string, src:string, dst:string) throw")
		return
	}
	ctx.PushString(root)
	ctx.PushString(src)
	ctx.PushString(dst)
	if ctx.Pcall(3) != 0 {
		e = errors.New(ctx.SafeToString(-1))
		return
	}
	return
}

// Install 執行 安裝 腳本
func (c *Context) Install(filename, root, dst, str string) (e error) {
	ctx := c.ctx
	popN := 0
	defer func() {
		if popN > 0 {
			switch popN {
			case 1:
				ctx.Pop()
			case 2:
				ctx.Pop2()
			case 3:
				ctx.Pop3()
			default:
				ctx.PopN(popN)
			}
		}
	}()
	// 執行 腳本
	popN++
	ctx.PushString(str)
	ctx.PushString(filename)
	if e = ctx.Pcompile(0); e != nil {
		//if e = ctx.PevalString(str); e != nil {
		return
	}
	if ctx.Pcall(0) != 0 {
		e = errors.New(ctx.SafeToString(-1))
		return
	}
	if !ctx.IsObject(-1) {
		e = fmt.Errorf("not found exports object")
		return
	}
	popN++
	ctx.GetPropString(-1, "Install")
	if !ctx.IsFunction(-1) {
		e = fmt.Errorf("not export function Install(root:string, dst:string) throw")
		return
	}
	ctx.PushString(root)
	ctx.PushString(dst)
	if ctx.Pcall(2) != 0 {
		e = errors.New(ctx.SafeToString(-1))
		return
	}
	return
}
