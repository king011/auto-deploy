package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"

	"encoding/json"
	"gitlab.com/king011/auto-deploy/service/utils/scripts"
	"os/exec"
)

func newScript(dir string, dev bool) (ctx *scripts.Context, e error) {
	var base string
	base, e = exec.LookPath(os.Args[0])
	if e != nil {
		return
	}
	base, e = filepath.Abs(base)
	if e != nil {
		return
	}
	base = filepath.Dir(base)

	dir, e = filepath.Abs(dir)
	if e != nil {
		return
	}

	var root []string
	if base == dir {
		root = []string{base}
	} else {
		root = []string{base, dir}
	}

	ctx = scripts.New(root, dev)
	return
}

// Install 安裝 檔案
func Install(root, dst, script string, dev bool) (e error) {
	ctx, e := newScript(root+"/"+dst, dev)
	if e != nil {
		return
	}
	defer ctx.Destory()
	// 讀取 合併 腳本
	filename := root + "/" + dst + "/" + script
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		if os.IsNotExist(e) {
			e = nil
		}
		return
	}
	str := strings.TrimSpace(string(b))
	if str == "" {
		return
	}
	dir := path.Dir(filename)
	str, e = wrapperStr(filename, dir, str)
	if e != nil {
		return
	}
	e = ctx.Install(filename, root, dst, str)
	return e
}
func wrapperStr(filename, dir, str string) (data string, e error) {
	var bf, bd []byte
	bf, e = json.Marshal(filename)
	if e != nil {
		return
	}
	bd, e = json.Marshal(filepath.Dir(filename))
	if e != nil {
		return
	}
	data = fmt.Sprintf(`(function(){"use strict";var utils = require("auto-deploy-utils").Utils;var __dirname=%s;var __filename=%s;	var exports = {};(function(){%s})();
	return exports;
})();`,
		bd, bf,
		str,
	)
	return
}

// Merge 合併 檔案
func Merge(root, src, dst, script string, dev bool) (e error) {
	ctx, e := newScript(root+"/"+dst, dev)
	if e != nil {
		return
	}
	// 讀取 合併 腳本
	filename := root + "/" + dst + "/" + script
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		if os.IsNotExist(e) {
			e = nil
		}
		return
	}
	str := strings.TrimSpace(string(b))
	if str == "" {
		return
	}
	dir := path.Dir(filename)
	str, e = wrapperStr(filename, dir, str)
	if e != nil {
		return
	}
	e = ctx.Merge(filename, root, src, dst, str)
	return e
}
