package utils

import (
	"crypto/md5"
	"encoding/hex"
	"net"
	"strings"
)

// GetKeys 返回 網卡 物理地址
func GetKeys() ([]string, error) {
	is, e := net.Interfaces()
	if e != nil {
		return nil, e
	}
	strs := make([]string, 0, 10)
	for i := 0; i < len(is); i++ {
		str := strings.TrimSpace(is[i].HardwareAddr.String())
		if str != "" {
			strs = append(strs, str)
		}
	}

	for i := 0; i < len(strs); i++ {
		b := md5.Sum([]byte(strs[i]))
		strs[i] = hex.EncodeToString(b[:])
	}
	return strs, nil
}
