package status

import (
	"sync/atomic"
)

var _Exit int32

// IsExit 返回是否 正在 結束 服務
func IsExit() bool {
	return atomic.LoadInt32(&_Exit) != 0
}

// SetExit 設置 服務 退出 狀態
func SetExit() {
	atomic.StoreInt32(&_Exit, 1)
}
