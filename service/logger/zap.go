package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"runtime"
)

// 日誌 單件
var _Logger *zap.Logger

// CallerEncoder 爲 caller 增加 函數名稱 默認只有檔案名行號
//
// filename:lineNum:funcName
func CallerEncoder(caller zapcore.EntryCaller, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(caller.TrimmedPath() + ":" + runtime.FuncForPC(caller.PC).Name())
}

// Debug .
func Debug(name string, fields ...zapcore.Field) {
	_Logger.Debug(name, fields...)
}

// Info .
func Info(name string, fields ...zapcore.Field) {
	_Logger.Info(name, fields...)
}

// Warn .
func Warn(name string, fields ...zapcore.Field) {
	_Logger.Warn(name, fields...)
}

// Error .
func Error(name string, fields ...zapcore.Field) {
	_Logger.Error(name, fields...)
}

// DPanic .
func DPanic(name string, fields ...zapcore.Field) {
	_Logger.DPanic(name, fields...)
}

// Panic .
func Panic(name string, fields ...zapcore.Field) {
	_Logger.Panic(name, fields...)
}

// Fatal .
func Fatal(name string, fields ...zapcore.Field) {
	_Logger.Fatal(name, fields...)
}
