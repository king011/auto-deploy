package configure

import (
	"crypto/sha512"
	"encoding/hex"
	"gitlab.com/king011/auto-deploy/utils"
	"strings"
)

// Service 服務 配置
type Service struct {
	// 服務器 工作地址
	LAddr string
	// 不驗證 ssl 證書
	SkipVerify bool
	// ssl證書 位置
	Cert string
	// ssl證書 key 位置
	Key string
	// 服務 管理密碼
	Password string
}

// Format 標準化 配置
func (s *Service) Format() {
	b := sha512.Sum512([]byte(s.Password))
	s.Password = hex.EncodeToString(b[:])

	s.Cert = strings.TrimSpace(s.Cert)
	s.Key = strings.TrimSpace(s.Key)
	if s.Cert == "" || s.Key == "" {
		s.Cert = ""
		s.Key = ""
	} else {
		s.Cert = utils.ABS(s.Cert)
		s.Key = utils.ABS(s.Key)
	}
}

// TLS 返回是否使用 TLS
func (s *Service) TLS() bool {
	return s.Cert != "" && s.Key != ""
}
