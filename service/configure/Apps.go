package configure

import (
	"gitlab.com/king011/auto-deploy/utils"
	"strings"
	"time"
)

// Apps 套件 定義
type Apps struct {
	// 倉庫 位置
	Store string
	// 停止進程 超時時間
	Kill time.Duration

	// 套件 自動更新 時間 如果爲空則 不自動更新
	// #分鐘		小時		號數		月份		周星期
	// #[0,59]		[0,23]		[1,31]		[1~12]		[0,7] 0==7==星期天
	Update []string

	// 禁用 自動 更新
	DisableUpdate bool
}

// Format 標準化 配置
func (a *Apps) Format() (e error) {
	a.Store = strings.TrimSpace(a.Store)
	if a.Store == "" {
		a.Store = "var/app"
	}
	a.Store = utils.ABS(a.Store)

	if len(a.Update) > 0 {
		arrs := make([]string, 0, len(a.Update))
		keys := make(map[string]bool)
		for i := 0; i < len(a.Update); i++ {
			str := strings.TrimSpace(a.Update[i])
			if str == "" || keys[str] {
				continue
			}
			keys[str] = true
			arrs = append(arrs, str)
		}
		a.Update = arrs
	}
	return
}
