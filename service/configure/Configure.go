package configure

import (
	"encoding/json"
	"github.com/google/go-jsonnet"
	"io/ioutil"
)

// Configure 配置 檔案
type Configure struct {
	Service Service
	Server  Server
	Logger  Logger
	Apps    Apps
}

// Format 標準化 配置
func (c *Configure) Format() (e error) {
	c.Service.Format()
	c.Logger.Format()
	e = c.Server.Format()
	if e != nil {
		return
	}
	e = c.Apps.Format()
	if e != nil {
		return
	}
	return
}

var _Configure Configure

// Single 返回 配置 單件
func Single() *Configure {
	return &_Configure
}

// Init 初始化 配置
func Init(filename string) (cnf *Configure, e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}

	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	e = json.Unmarshal([]byte(jsonStr), &_Configure)
	if e != nil {
		return
	}

	e = _Configure.Format()
	if e != nil {
		return
	}

	cnf = &_Configure
	return
}
