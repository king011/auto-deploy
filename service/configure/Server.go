package configure

import (
	"time"
)

// Server tcp 服務器 配置
type Server struct {
	// 服務器 地址
	Addr string
	// 是否使用 ssl 加密
	SSL bool

	// 不驗證 ssl 證書
	SkipVerify bool

	// 如果 不爲 0 定時 Ping 服務器 以免 tcp 斷線 (單位:秒)
	Ping time.Duration
}

// Format .
func (s *Server) Format() (e error) {
	if s.Ping < 0 {
		s.Ping = 0
	} else {
		s.Ping *= time.Second
	}
	return
}
