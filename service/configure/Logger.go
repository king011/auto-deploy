package configure

import (
	"gitlab.com/king011/auto-deploy/utils"
	"strings"
)

// Logger 日誌 配置
type Logger struct {
	// 日誌 http 如果爲空 則不啓動 http
	HTTP string
	// 日誌 檔案名
	Filename string
	// 單個日誌檔案 大小上限 MB
	MaxSize int
	// 保存 多少個 日誌 檔案
	MaxBackups int
	// 保存 多少天內的 日誌
	MaxAge int
	// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
	Level string
}

// Format 標準化 配置
func (l *Logger) Format() {
	l.HTTP = strings.TrimSpace(l.HTTP)
	l.Filename = strings.TrimSpace(l.Filename)
	if l.Filename != "" {
		l.Filename = utils.ABS(l.Filename)
	}
	if l.MaxSize < 10 {
		l.MaxSize = 10
	}
	l.Level = strings.ToLower(strings.TrimSpace(l.Level))
	switch l.Level {
	case "debug":
	case "info":
	case "warn":
	case "error":
	case "dpanic":
	case "panic":
	case "fatal":
	default:
		l.Level = "info"
	}
}
