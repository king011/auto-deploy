// 定義一些時間常量
local Millisecond = 1000000;
local Second = 1000 * Millisecond;
local Minute = 60 * Second;
local Hour = 60 * Minute;
local Day = 24 * Hour;
// 定義 根目錄位置
local Root = "";
{
	// 服務配置
	Service:{
		// 服務器 工作地址
		LAddr:"127.0.0.1:61000",
		// ssl證書 位置 (同時設置了 Cert Key 才啓用 tls)
		//Cert:Root + "etc/service.crt",
		// ssl證書 key 位置
		//Key:Root + "etc/service.key",
		// 服務 管理密碼
		Password:"12345678",
	},
	// 套件 服務器 配置
	Server:{
		// 服務器 工作地址
		Addr:"xsd.king011.com:60000",
		// 是否使用 ssl 加密
		SSL:false,
		// 如果爲 true 忽略 ssl 證書驗證
		SkipVerify:false,
		// 如果 不爲 0 定時 Ping 服務器 以免 tcp 斷線 (單位:秒)
		Ping:60*10
	},
	// 套件 設置
	Apps:{
		// 倉庫 位置
		//Store:Root + "var/app",
		// 停止進程 超時時間
		Kill:Minute,
		// 套件 自動更新 時間 如果爲空則 不自動更新
		// #分鐘		小時		號數		月份		周星期
		// #[0,59]		[0,23]		[1,31]		[1~12]		[0,7] 0==7==星期天
		Update:[
			"* 2 * * *",    // 每天 晚上 2點 更新
			"* 2 * * 0",    // 每週 星期天 晚上 2點 更新
			"* 2 1 * *",    // 每月 1 號 晚上 2點 更新

			// 無效的定義會被直接 忽略
			"",
		]
	},
	// Logger 日誌 配置
	Logger:{
		// 日誌 http 如果爲空 則不啓動 http
		//HTTP:"localhost:20001",
		// 日誌 檔案名 如果爲空 則輸出到 控制檯
		//Filename:Root + "var/log/service.log",
		// 單個日誌檔案 大小上限 MB
		MaxSize:    100, 
		// 保存 多少個 日誌 檔案
		MaxBackups: 3,
		// 保存 多少天內的 日誌
		MaxAge:     28,
		// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
		Level :"debug",
		// 是否要 輸出 代碼位置
        Caller:true,
	},
}
