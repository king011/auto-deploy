
// 定義 根目錄位置
local Root = "";
//local ShowSQL = true;
local ShowSQL = false;
{
	// 服務器 配置
	Server:{
		// 服務器 工作地址
		LAddr:"127.0.0.1:60000",
		// ssl證書 位置 (同時設置了 Cert Key 才啓用 tls)
		//Cert:Root + "etc/service.crt",
		// ssl證書 key 位置
		//Key:Root + "etc/service.key",
		// 服務器 管理密碼
		Password:"12345678",

		// 啓用 用戶驗證模式 只有管理員和自己可以修改 自己的包
		// 如果 不啓用 則 任何人都可以修改 任意包
		User:true,
		// cookie 過期時間 單位 秒
		MaxAge:60*60*24,
		// cookie 密鑰檔案 
		//Secure:"etc/securecookie.json"
	},
	// 套件 倉庫 位置
	Store:Root + "var/store",
	// 數據庫設置
	DB:{
		// 連接字符串
		Str:"AutoDeploy:12345678@/AutoDeploy?charset=utf8",

		// 是否要顯示sql指令
		Show:ShowSQL,

		// ping 數據庫 間隔 單位 分鐘
		Ping:60,

		// 連接池 最大連接 < 1 無限制
		MaxOpen:1000,
		// 連接池 最大 空閒 連接 < 1 不允許空閒連接
		MaxIdle:10,

		// 如果為0 使用 默認值 如果小於0 將不啟用緩存
		Cache:1000,
	},
	// Logger 日誌 配置
	Logger:{
		// 日誌 http 如果爲空 則不啓動 http
		//HTTP:"localhost:20000",
		// 日誌 檔案名
		//Filename:Root + "var/log/server.log",
		// 單個日誌檔案 大小上限 MB
		MaxSize:    100, 
		// 保存 多少個 日誌 檔案
		MaxBackups: 3,
		// 保存 多少天內的 日誌
		MaxAge:     28,
		// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
		Level :"debug",
		// 是否要 輸出 代碼位置
        Caller:true,
	},
}
