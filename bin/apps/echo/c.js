#!/usr/bin/env node

var net = require("net")
//定義 服務器 地址
const Port = 6000
const Host = "127.0.0.1"

// 連接 服務器
var c = net.createConnection({
    host: Host,
    port: Port
}, () => {
    console.log('connected success!');

    c.write('this is a test')
}).on('data', (data) => {
    console.log(data.toString())

    // 斷開 連接
    c.end()
}).on('close', () => {
    // 物理 end 還是 error 後 close 都會被調用
    console.log("close")

    // 銷毀 連接
    c.destroy(true)
}).on('end', () => {
    console.log('end')
}).on('error', (e) => {
    console.log('error', e)
})