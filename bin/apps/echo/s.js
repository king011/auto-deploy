#!/usr/bin/env node

var net = require("net")
//定義 服務器 地址
const Port = 6000
const Host = ""
//創建 服務器
var server = net.createServer(function (c) {
    // 新連接
    console.log('client connected :', c.remoteAddress, c.remotePort);
    // 監聽 斷開連接
    c.on('close', () => {
        // 物理 end 還是 error 後 close 都會被調用
        console.log('client close :', c.remoteAddress, c.remotePort);

        // 銷毀 連接
        c.destroy(true)
    }).on('end', () => {
        console.log('end')
    }).on('error', (e) => {
        console.log('error')
    })

    // 接受到數據
    c.on('data', function (buffer) {
        console.log(buffer.toString())

        // 發送數據
        if (!c.write(buffer)) {
            console.log("bad write");
        }
    });
}).on('error', (e) => {// 創建失敗 錯誤處理
    if (e.code === 'EADDRINUSE') {
        console.log('地址端口已經被佔用,等待重試...');
        setTimeout(() => {
            server.close();
            server.listen(Port, Host);
        }, 1000);
    }
}).listen(Port, Host, undefined, () => {//監聽 地址 端口
    console.log("work at ", Host, ":", Port)
});