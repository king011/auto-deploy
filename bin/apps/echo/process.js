// 此腳本 定義了 套件 進程
var dir = __dirname;
exports.Service = [
    {
        // 啓動 命令
        Start: [__dirname + "/s.js"],
        // 結束命令 如果 未定義 則 強制 kill 進程
        Stop: ["node", "-v"],
        //Stop: undefined,
        // 工作目錄 
        Dir: __dirname,
    },
]
