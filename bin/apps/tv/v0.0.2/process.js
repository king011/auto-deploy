
// this scripts define package's process

// variable __dirname is package installed path
// named __dirname for cosplay nodejs, but environment not nodejs, it's duktape
// https://duktape.org/api.html
var dir = __dirname;

// exports cosplay nodejs
// export Service array, it's process group.
exports.Service = [
	// every element is a process
	{
		// how to start process
		Start: [__dirname + "/main.js"],
		//Start: [__dirname + "/main.js" , "daemon"],
		// how to stop process, if invalid will kill process
		Stop: undefined,
		//Stop: [__dirname + "/main.js" , "kill"],
		// work directory, if invalid will use parent's work directory.
		Dir: __dirname,
	},
]
