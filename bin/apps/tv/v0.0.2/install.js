exports.Install = function (root, dst) {
    var filename = root + "/" + dst + "/log"
    var arrs = []
    arrs.push("#/bin/bash")
    arrs.push("root = " + root)
    arrs.push("dst = " + dst)
    arrs.push(new Date())
    utils.SaveFile(filename, arrs.join("\n"), 0777)
}