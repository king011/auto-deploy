#!/bin/bash
#Program:
#       go share for bash completion
#History:
#       2018-12-04 king011 first release
#Email:
#       zuiwuchang@gmail.com

function __king011_auto_deploy_server_daemon(){
    # 參數定義
    local opts='-c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_server_package(){
    # 參數定義
    local opts='--user --set-user --remove-user --add-user --add --delete -g --get -i --init -n --name -r --remove -s --search -u --update -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        --user|--set-user|--remove-user|--add-user|--add|--delete|-d|--description|-n|--name)
           COMPREPLY=()
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_server_version(){
    # 參數定義
    local opts='-i --init -l --list --package -p --push -r --remove -u --update -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -i|--init|-l|--list|--package|-p|--push|-r|--remove|-u|--update)
           COMPREPLY=()
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_server_login(){
    # 參數定義
    local opts='-r --refresh -u --user -p --password -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -u|--user|-p|--password)
           COMPREPLY=()
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_server_passwd(){
    # 參數定義
    local opts='-u --user -p --password -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -u|--user|-p|--password)
           COMPREPLY=()
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}

function __king011_auto_deploy_server(){
    # 獲取 正在輸入的 參數
    local cur=${COMP_WORDS[COMP_CWORD]}
    
    #  輸入 第1個 參數
    if [ 1 == $COMP_CWORD ];then
        local opts="daemon package login passwd version --help -h -v --version"
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    else
        # switch 子命令
        case ${COMP_WORDS[1]} in
            daemon)
                __king011_auto_deploy_server_daemon
            ;;

            package)
               __king011_auto_deploy_server_package
            ;;
            
            version)
               __king011_auto_deploy_server_version
            ;;

            login)
               __king011_auto_deploy_server_login
            ;;

            passwd)
                __king011_auto_deploy_server_passwd
            ;;
        esac
    fi
}

complete -F __king011_auto_deploy_server auto-deploy-server
complete -F __king011_auto_deploy_server ./auto-deploy-server
complete -F __king011_auto_deploy_server auto-deploy-server.exe
complete -F __king011_auto_deploy_server ./auto-deploy-server.exe

function __king011_auto_deploy_service_daemon(){
    # 參數定義
    local opts='-d --disable -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}

function __king011_auto_deploy_service_download(){
    # 參數定義
    local opts='-d --dst -p --package -v --version -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_service_exit(){
    # 參數定義
    local opts='-c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_service_install(){
    # 參數定義
    local opts='-d --description --dev -p --package -t --tar -v --version -y --yes -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;
        
        -d|--description|-p|--package|-t|--tar|-v|--version)
            COMPREPLY=()
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_service_keys(){
    # 參數定義
    local opts='-r --remote -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_service_list(){
    # 參數定義
    local opts='--onlyname -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -h|--help)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;

        # default
        *)
            local opts_items=`auto-deploy-service list --onlyname 2> /dev/null`
            if [ $? == 0 ] ;then
                COMPREPLY=( $(compgen -W "${opts} ${opts_items}" -- ${cur}) )
            else
                COMPREPLY=( $(compgen -W "${opts_items}" -- ${cur}) )
            fi
        ;;
    esac
}
function __king011_auto_deploy_service_test(){
    # 參數定義
    local opts='-i --install -d --dst -r --root -s --src -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure|-r|--root)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -d|--dst|-s|--src)
            COMPREPLY=()
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}

function __king011_auto_deploy_service_mode(){
    # 參數定義
    local opts='-p --package -r --run -u --update -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -p|--package)
            local opts_items=`auto-deploy-service list --onlyname 2> /dev/null`
            if [ $? == 0 ] ;then
                COMPREPLY=( $(compgen -W "${opts_items}" -- ${cur}) )
            else
                COMPREPLY=()
            fi
        ;;

        -r|--run)
            COMPREPLY=( $(compgen -W "auto keep manual" -- ${cur}) )
        ;;

        -u|--update)
            COMPREPLY=( $(compgen -W "auto dev manual" -- ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_service_ping(){
    # 參數定義
    local opts='-c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_service_remove(){
    # 參數定義
    local opts='-y --yes -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;


        # default
        *)
            local opts_items=`auto-deploy-service list --onlyname 2> /dev/null`
            if [ $? == 0 ] ;then
                COMPREPLY=( $(compgen -W "${opts} ${opts_items}" -- ${cur}) )
            else
                COMPREPLY=( $(compgen -W "${opts_items}" -- ${cur}) )
            fi
        ;;
    esac
}
function __king011_auto_deploy_service_run(){
    # 參數定義
    local opts='-n --name -p --pipe -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -n|--name)
            local opts_items=`auto-deploy-service list --onlyname 2> /dev/null`
            if [ $? == 0 ] ;then
                COMPREPLY=( $(compgen -W "${opts_items}" -- ${cur}) )
            else
                COMPREPLY=()
            fi
        ;;

        # default
        *)
           COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_service_search(){
    # 參數定義
    local opts='-l --list -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_auto_deploy_service_show(){
    # 參數定義
    local opts='-c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;


        # default
        *)
            local opts_items=`auto-deploy-service list --onlyname 2> /dev/null`
            if [ $? == 0 ] ;then
                COMPREPLY=( $(compgen -W "${opts} ${opts_items}" -- ${cur}) )
            else
                COMPREPLY=( $(compgen -W "${opts_items}" -- ${cur}) )
            fi
        ;;
    esac
}
function __king011_auto_deploy_service_start(){
    # 參數定義
    local opts='-c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;


        # default
        *)
            local opts_items=`auto-deploy-service list --onlyname 2> /dev/null`
            if [ $? == 0 ] ;then
                COMPREPLY=( $(compgen -W "${opts} ${opts_items}" -- ${cur}) )
            else
                COMPREPLY=( $(compgen -W "${opts_items}" -- ${cur}) )
            fi
        ;;
    esac
}
function __king011_auto_deploy_service_stop(){
    # 參數定義
    local opts='-c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;


        # default
        *)
            local opts_items=`auto-deploy-service list --onlyname 2> /dev/null`
            if [ $? == 0 ] ;then
                COMPREPLY=( $(compgen -W "${opts} ${opts_items}" -- ${cur}) )
            else
                COMPREPLY=( $(compgen -W "${opts_items}" -- ${cur}) )
            fi
        ;;
    esac
}
function __king011_auto_deploy_service_update(){
    # 參數定義
    local opts='--dev -t --tar -y --yes -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -t|--tar)
            COMPREPLY=()
        ;;

        # default
        *)
            local opts_items=`auto-deploy-service list --onlyname 2> /dev/null`
            if [ $? == 0 ] ;then
                COMPREPLY=( $(compgen -W "${opts} ${opts_items}" -- ${cur}) )
            else
                COMPREPLY=( $(compgen -W "${opts_items}" -- ${cur}) )
            fi
        ;;
    esac
}
function __king011_auto_deploy_service(){
    # 獲取 正在輸入的 參數
    local cur=${COMP_WORDS[COMP_CWORD]}
    
    #  輸入 第1個 參數
    if [ 1 == $COMP_CWORD ];then
        local opts="daemon download exit install keys list test mode ping remove run search show start stop update --help -h -v --version"
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    else
        # switch 子命令
        case ${COMP_WORDS[1]} in
            daemon)
                __king011_auto_deploy_service_daemon
            ;;

            download)
               __king011_auto_deploy_service_download
            ;;
            
            exit)
               __king011_auto_deploy_service_exit
            ;;

            install)
               __king011_auto_deploy_service_install
            ;;

            keys)
               __king011_auto_deploy_service_keys
            ;;

            list)
               __king011_auto_deploy_service_list
            ;;

            test)
               __king011_auto_deploy_service_test
            ;;

            mode)
               __king011_auto_deploy_service_mode
            ;;

            ping)
                __king011_auto_deploy_service_ping
            ;;

            remove)
               __king011_auto_deploy_service_remove
            ;;

            run)
               __king011_auto_deploy_service_run
            ;;

            search)
               __king011_auto_deploy_service_search
            ;;

            show)
               __king011_auto_deploy_service_show
            ;;

            start)
               __king011_auto_deploy_service_start
            ;;

            stop)
               __king011_auto_deploy_service_stop
            ;;

            update)
               __king011_auto_deploy_service_update
            ;;
        esac
    fi   
}
complete -F __king011_auto_deploy_service auto-deploy-service
complete -F __king011_auto_deploy_service ./auto-deploy-service
complete -F __king011_auto_deploy_service auto-deploy-service.exe
complete -F __king011_auto_deploy_service ./auto-deploy-service.exe