#!/bin/bash
#Program:
#       go share for bash completion
#History:
#       2018-12-04 king011 first release
#Email:
#       zuiwuchang@gmail.com

export PATH=$PATH:$(cd $(dirname $BASH_SOURCE) && pwd)
. $(cd $(dirname $BASH_SOURCE) && pwd)/completion-auto-deploy.sh