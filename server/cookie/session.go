package cookie

import (
	"context"
	"encoding/gob"
	"time"

	"google.golang.org/grpc/metadata"
)

const (
	// CookieName cookie 名稱
	CookieName = "king011_auto_deploy_grpc_session"
)

func init() {
	gob.Register(&Session{})
}

// Session 客戶連接 信息
type Session struct {
	// 用戶登入名稱
	Name string

	// 是否是管理員
	Root bool

	// session 創建時間
	Time time.Time
}

// Cookie 爲 session 創建 cookie 值
func (s *Session) Cookie() (string, error) {
	return Encode("session", s)
}

// FromContext 由Context值 恢復session
func FromContext(ctx context.Context) (session *Session, e error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		session, e = FromMD(md)
	}
	return
}

// FromMD 由cookie值 恢復session
func FromMD(md metadata.MD) (session *Session, e error) {
	strs := md.Get(CookieName)
	if len(strs) > 0 {
		session, e = FromCookie(strs[0])
		return
	}
	return
}

// FromCookie 由cookie值 恢復session
func FromCookie(val string) (session *Session, e error) {
	var s Session
	e = Decode("session", val, &s)
	if e != nil {
		return
	}
	session = &s
	return
}
