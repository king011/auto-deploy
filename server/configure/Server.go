package configure

import (
	"crypto/sha512"
	"encoding/hex"
	"path/filepath"
	"strings"

	"gitlab.com/king011/auto-deploy/utils"
)

// Server tcp 服務器 配置
type Server struct {
	// 服務器 工作地址
	LAddr string
	// 不驗證 ssl 證書
	SkipVerify bool
	// ssl證書 位置 (同時設置了 Cert Key 才啓用 tls)
	Cert string
	// ssl證書 key 位置 (如果不使用 ssl 忽略此設置)
	Key string

	// 服務器 管理密碼
	Password string

	// 啓用 用戶驗證模式 只有管理員和自己可以修改 自己的包
	// 如果 不啓用 則 任何人都可以修改 任意包
	User bool

	// cookie 過期時間 單位 秒
	MaxAge int64
	// cookie 密鑰檔案 如果爲空 使用 隨機的密鑰
	Secure string
}

// Format 標準化 配置
func (s *Server) Format() {
	b := sha512.Sum512([]byte(s.Password))
	s.Password = hex.EncodeToString(b[:])
	s.Cert = strings.TrimSpace(s.Cert)
	s.Key = strings.TrimSpace(s.Key)
	if s.Cert == "" || s.Key == "" {
		s.Cert = ""
		s.Key = ""
	} else {
		s.Cert = utils.ABS(s.Cert)
		s.Key = utils.ABS(s.Key)
	}

	if s.MaxAge < 1 {
		s.MaxAge = 60 * 60 * 24
	}
	if s.Secure == "" {
		s.Secure = utils.BasePath() + "/etc/securecookie.json"
	} else {
		if filepath.IsAbs(s.Secure) {
			s.Secure = filepath.Clean(s.Secure)
		} else {
			s.Secure = filepath.Clean(utils.BasePath() + "/" + s.Secure)
		}
	}
}

// TLS 返回是否使用 TLS
func (s *Server) TLS() bool {
	return s.Cert != "" && s.Key != ""
}
