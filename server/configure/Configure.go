package configure

import (
	"encoding/json"
	"io/ioutil"
	"strings"

	"github.com/google/go-jsonnet"
	"gitlab.com/king011/auto-deploy/utils"
	"gitlab.com/king011/king-go/log/logger.zap"
)

// Configure 配置 檔案
type Configure struct {
	Server Server
	// 日誌 配置
	Logger logger.Options
	DB     DB `json:"DB"`
	// 套件 倉庫 位置
	Store string
}

// Format 標準化 配置
func (c *Configure) Format() (e error) {
	c.Server.Format()
	c.DB.Format()
	_Configure.Store = strings.TrimSpace(_Configure.Store)
	if _Configure.Store == "" {
		_Configure.Store = "var/store"
	}
	_Configure.Store = utils.ABS(_Configure.Store)
	return
}

var _Configure Configure

// Single 返回 配置 單件
func Single() *Configure {
	return &_Configure
}

// Init 初始化 配置
func Init(filename string) (cnf *Configure, e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}

	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	e = json.Unmarshal([]byte(jsonStr), &_Configure)
	if e != nil {
		return
	}

	e = _Configure.Format()
	if e != nil {
		return
	}

	cnf = &_Configure
	return
}
