package configure

import (
	"strings"
	"time"
)

// DB 數據庫設置
type DB struct {
	// 連接字符串
	Str string

	// 是否要顯示sql指令
	Show bool

	// ping 數據庫 間隔 單位 分鐘
	Ping time.Duration

	// 連接池 最大連接 < 1 無限制
	MaxOpen int
	// 連接池 最大 空閒 連接 < 1 不允許空閒連接
	MaxIdle int

	// 如果為0 使用 默認值 如果小於0 將不啟用緩存
	Cache int
}

// Format 標準化 配置
func (d *DB) Format() {
	d.Str = strings.TrimSpace(d.Str)

	if d.Ping < 1 {
		d.Ping = time.Hour * 60
	} else {
		d.Ping *= time.Minute
	}
	if d.MaxOpen < 0 {
		d.MaxOpen = 0
	}
	if d.MaxIdle < 0 {
		d.MaxIdle = 0
	}
	if d.Cache == 0 {
		d.Cache = 1000
	}
}
