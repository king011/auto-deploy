package manipulator

import (
	"fmt"
	"os"
	"time"

	"github.com/go-xorm/xorm"
	"github.com/google/uuid"
	"gitlab.com/king011/auto-deploy/server/configure"
	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/auto-deploy/server/logger"
	"gitlab.com/king011/king-go/os/fileperm"
	"go.uber.org/zap"
)

var _Engine *xorm.Engine
var _Store string

// Init 初始化 數據庫
func Init() (e error) {
	cnf := configure.Single()
	_Store = cnf.Store

	// create engine
	if _Engine, e = xorm.NewEngine("mysql", cnf.DB.Str); e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "connect database error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
		return
	} else if e = _Engine.Ping(); e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "ping database error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
		return
	}
	// 設置 連接池
	if cnf.DB.MaxOpen > 1 {
		_Engine.SetMaxOpenConns(cnf.DB.MaxOpen)
	}
	if cnf.DB.MaxIdle > 1 {
		_Engine.SetMaxIdleConns(cnf.DB.MaxIdle)
	}

	// show sql
	if cnf.DB.Show {
		_Engine.ShowSQL(true)
	}

	// keep live
	go func() {
		var e error
		for {
			time.Sleep(cnf.DB.Ping)
			e = _Engine.Ping()
			if e != nil {
				if ce := logger.Logger.Check(zap.WarnLevel, "keep live ping database error"); ce != nil {
					ce.Write(
						zap.Error(e),
					)
				}
			}
		}
	}()

	// cache
	if cnf.DB.Cache > 0 {
		_Engine.SetDefaultCacher(
			xorm.NewLRUCacher(
				xorm.NewMemoryStore(),
				cnf.DB.Cache,
			),
		)
	}

	// init tables
	if e = initTables(); e != nil {
		return e
	}

	return
}

func initTables() (e error) {
	session, e := NewTransaction()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "NewTransaction error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	defer CloseTransaction(session, &e)

	e = initTable(session, &data.Package{})
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "table package error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	e = initTable(session, &data.Version{})
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "init table version error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	e = initTable(session, &data.User{})
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "init table user error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	return
}
func initTable(session *xorm.Session, bean interface{}) (e error) {
	var ok bool
	if ok, e = session.IsTableExist(bean); e != nil {
		return
	} else if ok {
		e = session.Sync2(bean)
	} else {
		if e = session.CreateTable(bean); e != nil {
			return
		}
		if e = session.CreateIndexes(bean); e != nil {
			return
		}
		if e = session.CreateUniques(bean); e != nil {
			return
		}
	}
	return
}

// InitFilesystem 初始化 檔案系統
func InitFilesystem() (e error) {
	root := _Store
	// 創建 根目錄
	var info os.FileInfo
	info, e = os.Stat(root)
	if e == nil {
		if !info.IsDir() {
			e = fmt.Errorf("[%v] is not a directory", root)
			if ce := logger.Logger.Check(zap.ErrorLevel, "init filesystem error"); ce != nil {
				ce.Write(zap.Error(e))
			}
			return
		}
	} else {
		if os.IsNotExist(e) {
			e = os.MkdirAll(root, fileperm.Directory)
			if e != nil {
				if ce := logger.Logger.Check(zap.ErrorLevel, "init filesystem error"); ce != nil {
					ce.Write(zap.Error(e))
				}
				return
			}
		} else {
			if ce := logger.Logger.Check(zap.ErrorLevel, "init filesystem error"); ce != nil {
				ce.Write(zap.Error(e))
			}
			return
		}
	}

	// 臨時 檔案夾
	path := root + "/tmp"
	info, e = os.Stat(path)
	if e == nil {
		e = clearDirectory(path, info)
		if e != nil {
			if ce := logger.Logger.Check(zap.ErrorLevel, "init filesystem error"); ce != nil {
				ce.Write(zap.Error(e))
			}
			return
		}
	} else {
		if os.IsNotExist(e) {
			e = os.MkdirAll(path, fileperm.Directory)
			if e != nil {
				if ce := logger.Logger.Check(zap.ErrorLevel, "init filesystem error"); ce != nil {
					ce.Write(zap.Error(e))
				}
				return
			}
		} else {
			if ce := logger.Logger.Check(zap.ErrorLevel, "init filesystem error"); ce != nil {
				ce.Write(zap.Error(e))
			}
			return
		}
	}

	return
}
func clearDirectory(path string, info os.FileInfo) (e error) {
	if !info.IsDir() {
		e = fmt.Errorf("[%v] is not a directory", path)
		return
	}
	var f *os.File
	f, e = os.Open(path)
	if e != nil {
		return
	}
	var names []string
	names, e = f.Readdirnames(0)
	f.Close()
	if e != nil {
		return
	}

	for _, name := range names {
		e = os.RemoveAll(path + "/" + name)
		if e != nil {
			return
		}
	}
	return
}

// Engine 返回 數據庫 引擎
func Engine() *xorm.Engine {
	return _Engine
}

// NewSession 返回 數據庫 session
func NewSession() *xorm.Session {
	return _Engine.NewSession()
}

// NewTransaction 創建 一個 事務
func NewTransaction() (session *xorm.Session, e error) {
	session = _Engine.NewSession()
	e = session.Begin()
	if e != nil {
		session.Close()
		return
	}
	return
}

// CloseTransaction .
func CloseTransaction(session *xorm.Session, e *error) {
	if *e == nil {
		session.Commit()
	} else {
		session.Rollback()
	}
	session.Close()
}

// Store 套件包 根目錄
func Store() string {
	return _Store
}

// TempFile 返回一個臨時檔案
func TempFile() (filename string, e error) {
	var u uuid.UUID
	u, e = uuid.NewUUID()
	if e != nil {
		return
	}
	filename = _Store + "/tmp/" + u.String()
	return
}

// PackageDir 返回 套件 數據 檔案夾
func PackageDir(pkg string) (path string) {
	return _Store + "/data/" + pkg
}

// VersionFilename 返回 版本 數據檔案名
func VersionFilename(pkg, v string) string {
	return _Store + "/data/" + pkg + "/" + v + ".tar.gz"
}
func moveToVersion(filename, pkg, v string) (e error) {
	dst := _Store + "/data/" + pkg
	os.MkdirAll(dst, fileperm.Directory)
	dst += "/" + v + ".tar.gz"
	e = os.Rename(filename, dst)
	return
}
