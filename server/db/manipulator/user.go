package manipulator

import (
	"errors"

	"gitlab.com/king011/auto-deploy/server/db/data"
)

// User 用戶表
type User struct {
}

// Password 修改密碼
func (User) Password(name, password string) (e error) {
	n, e := Engine().ID(name).Update(&data.User{
		Password: password,
	})
	if e != nil {
		return
	}
	if n == 0 {
		e = errors.New("not changed any data")
	}
	return
}

// Login 登入
func (User) Login(name, password string) (bean *data.User, e error) {
	if name == "" {
		e = errors.New("invalid name")
		return
	}
	if password == "" {
		e = errors.New("invalid password")
		return
	}
	bean = &data.User{
		Name:     name,
		Password: password,
	}
	ok, e := Engine().Get(bean)
	if e != nil {
		bean = nil
		return
	} else if !ok {
		bean = nil
		e = errors.New("user or password not matched")
		return
	}
	return
}
