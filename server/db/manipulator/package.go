package manipulator

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/go-xorm/xorm"
	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/auto-deploy/server/logger"
	"go.uber.org/zap"
)

var errPackageEmpty = errors.New("package empty")
var errPackageAddName = errors.New("package add name can't be empty")
var errPackageAddKey = errors.New("package add key can't be empty")
var errPackageDeleteName = errors.New("package delete name can't be empty")
var errPackageDeleteKey = errors.New("package delete key can't be empty")

// Package 套件包 管理
type Package struct {
}

// Init 創建一個 套件包
func (Package) Init(name, description string) (e error) {
	name = strings.TrimSpace(name)
	description = strings.TrimSpace(description)

	_, e = Engine().InsertOne(&data.Package{
		Name:        name,
		Description: description,
	})

	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
	}
	return
}

// Update 更新 套件包 信息
func (Package) Update(name, description string) (e error) {
	name = strings.TrimSpace(name)
	description = strings.TrimSpace(description)

	var session *xorm.Session
	session, e = NewTransaction()
	defer CloseTransaction(session, &e)

	var n int64
	n, e = session.
		ID(name).
		Cols(data.PackageColDescription).
		Update(&data.Package{
			Description: description,
		})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
	} else if n == 0 {
		var ok bool
		// 驗證 是否存在
		ok, e = session.ID(name).Get(&data.Package{})
		if e != nil {
			if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
				ce.Write(zap.Error(e))
			}
			return
		} else if !ok {
			e = fmt.Errorf("update package [%v] not found", name)
			return
		}
	}
	return
}

// Remove 刪除 套件包
func (Package) Remove(name string) (e error) {
	name = strings.TrimSpace(name)
	if name == "" {
		return
	}

	var session *xorm.Session
	session, e = NewTransaction()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}
	defer func() {
		if e == nil {
			session.Commit()
			Engine().ClearCache(&data.Package{})
		} else {
			session.Rollback()
		}
	}()

	// 刪除 套件包
	_, e = session.Delete(&data.Package{Name: name})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}

	// 刪除 版本
	_, e = session.Delete(&data.Version{Package: name})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}

	// 刪除 套件檔案
	er := os.RemoveAll(PackageDir(name))
	if er != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "remove package disk data error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", name),
			)
		}
	}

	return
}

// Get 返回 套件包 信息
func (Package) Get(name string) (bean *data.Package, e error) {
	bean = &data.Package{
		Name: name,
	}
	var ok bool
	ok, e = Engine().Get(bean)
	if e != nil {
		bean = nil
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	} else if !ok {
		bean = nil
		e = fmt.Errorf("package [%v] not exist", name)
		return
	}

	return
}

// Search 查找 套件包
func (Package) Search(name string) (bean []data.Package, e error) {
	name = strings.TrimSpace(name)
	if name == "" {
		e = Engine().Find(&bean)
	} else {
		e = Engine().Where(data.PackageColName+" like ?", name).Find(&bean)
	}
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}
	return
}

// Add 增加 套件包 下載 key
func (m Package) Add(name, key, description string) (e error) {
	name = strings.TrimSpace(name)
	key = strings.TrimSpace(key)
	description = strings.TrimSpace(description)
	if name == "" {
		e = errPackageAddName
		return
	} else if key == "" {
		e = errPackageAddKey
		return
	}

	bean, e := m.Get(name)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}
	keys := bean.GetKeys()
	if keys == nil {
		keys = make(map[string]string)

	} else if val, ok := keys[key]; ok && val == description {
		return
	}
	keys[key] = description
	update := &data.Package{}
	update.SetKeys(keys)
	_, e = Engine().ID(name).Cols(data.PackageColKeys).Update(update)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}
	return
}

// Delete 刪除 套件包 下載 key
func (m Package) Delete(name, key string) (e error) {
	name = strings.TrimSpace(name)
	key = strings.TrimSpace(key)
	if name == "" {
		e = errPackageDeleteName
		return
	} else if key == "" {
		e = errPackageDeleteKey
		return
	}

	bean, e := m.Get(name)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}

	keys := bean.GetKeys()
	if keys == nil {
		return
	} else if key == "*" {
		keys = nil
	} else if _, ok := keys[key]; !ok {
		return
	} else {
		delete(keys, key)
	}

	update := &data.Package{}
	update.SetKeys(keys)
	_, e = Engine().ID(name).Cols(data.PackageColKeys).Update(update)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}
	return
}

// IsAllow 返回是否 允許 下載
func (Package) IsAllow(keys, pkg string) (yes bool, e error) {
	pkg = strings.TrimSpace(pkg)
	keys = strings.TrimSpace(keys)
	if pkg == "" {
		e = errPackageEmpty
		return
	}

	bean := &data.Package{
		Name: pkg,
	}
	var ok bool
	ok, e = Engine().Get(bean)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	} else if !ok {
		e = fmt.Errorf("package not found [%s]", pkg)
		return
	}

	m := bean.GetKeys()
	if len(m) == 0 {
		yes = true
		return
	} else if keys == "" {
		return
	}
	strs := strings.Split(keys, "-")
	for i := 0; i < len(strs); i++ {
		_, yes = m[strs[i]]
		if yes {
			break
		}
	}
	return
}

// AddUser 爲 套件 添加 用戶
func (m Package) AddUser(name string, users []string) (e error) {
	name = strings.TrimSpace(name)
	if name == "" {
		e = errPackageAddName
		return
	} else if len(users) == 0 {
		e = fmt.Errorf("users empty")
		return
	}

	bean, e := m.Get(name)
	if e != nil {
		return
	}
	keys := bean.GetUsers()
	if keys == nil {
		keys = make(map[string]bool)
	}
	change := false
	for _, key := range users {
		if _, ok := keys[key]; ok {
			continue
		}
		keys[key] = true
		if !change {
			change = true
		}
	}
	if !change {
		return
	}
	update := &data.Package{}
	update.SetUsers(keys)
	_, e = Engine().ID(name).Cols(data.PackageColUsers).Update(update)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}
	return
}

// RemoveUser 從 套件 刪除 用戶
func (m Package) RemoveUser(name string, users []string) (e error) {
	name = strings.TrimSpace(name)
	if name == "" {
		e = errPackageAddName
		return
	} else if len(users) == 0 {
		e = fmt.Errorf("users empty")
		return
	}

	bean, e := m.Get(name)
	if e != nil {
		return
	}
	keys := bean.GetUsers()
	if keys == nil {
		return
	}
	change := false
	for _, key := range users {
		if _, ok := keys[key]; !ok {
			continue
		}
		delete(keys, key)
		if !change {
			change = true
		}
	}
	if !change {
		return
	}
	update := &data.Package{}
	update.SetUsers(keys)
	_, e = Engine().ID(name).Cols(data.PackageColUsers).Update(update)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}
	return
}

// SetUser 爲 套件 設置 用戶
func (m Package) SetUser(name string, users []string) (e error) {
	name = strings.TrimSpace(name)
	if name == "" {
		e = errPackageAddName
		return
	}

	bean, e := m.Get(name)
	if e != nil {
		return
	}
	if bean.Users == nil && len(users) == 0 {
		return
	}
	update := &data.Package{}
	if len(users) != 0 {
		keys := make(map[string]bool)
		for _, key := range users {
			keys[key] = true
		}
		update.SetUsers(keys)
	}
	_, e = Engine().ID(name).Cols(data.PackageColUsers).Update(update)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(zap.Error(e))
		}
		return
	}
	return
}

// IsUser 用戶是否是 套件  擁有者
func (m Package) IsUser(name, user string) (ok bool, e error) {
	bean, e := m.Get(name)
	if e != nil {
		return
	}
	keys := bean.GetUsers()
	if keys == nil {
		ok = true
		return
	}
	_, ok = keys[user]
	return
}
