package manipulator

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/go-xorm/xorm"
	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/auto-deploy/server/logger"
	"go.uber.org/zap"
)

var errParamError = errors.New("params error")

// Version 套件的 發行版本 管理
type Version struct {
}

// CheckAvailable 驗證 套件 版本 是否可用
func (Version) CheckAvailable(pkg, v string) (e error) {
	if pkg == "" || v == "" {
		e = errParamError
		if ce := logger.Logger.Check(zap.ErrorLevel, "system error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	// 驗證 套件是否存在
	findPackage := &data.Package{Name: pkg}
	var ok bool
	ok, e = Engine().Get(findPackage)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if !ok {
		e = fmt.Errorf("package [%v] not exist", pkg)
		return
	}
	// 驗證 版本 是否已經 存在
	findVersion := &data.Version{
		Name:    v,
		Package: pkg,
	}
	ok, e = Engine().Get(findVersion)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if ok {
		e = fmt.Errorf("version [%v %v] already exist", pkg, v)
		return
	}
	return
}

// Push 提交 新 版本
func (Version) Push(bean *data.Version, filename string) (e error) {
	var session *xorm.Session
	session, e = NewTransaction()
	if e != nil {
		return
	}
	defer CloseTransaction(session, &e)
	// 驗證 套件是否存在
	findPackage := &data.Package{Name: bean.Package}
	var ok bool
	ok, e = session.Get(findPackage)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if !ok {
		e = fmt.Errorf("package [%v] not exist", bean.Package)
		return
	}

	// 驗證 版本 是否已經 存在
	findVersion := &data.Version{
		Name:    bean.Name,
		Package: bean.Package,
	}
	ok, e = session.Get(findVersion)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if ok {
		e = fmt.Errorf("version [%v %v] already exist", bean.Package, bean.Name)
		return
	}
	// 更新 數據庫
	_, e = session.InsertOne(bean)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	// 移動 檔案數據
	e = moveToVersion(filename, bean.Package, bean.Name)
	if e != nil {
		return
	}
	return
}

// Update 更新 版本 描述信息
func (Version) Update(pkg, version, description string, dev bool) (e error) {
	var session *xorm.Session
	session, e = NewTransaction()
	defer CloseTransaction(session, &e)

	var n int64
	n, e = session.
		Cols(data.VersionColDescription, data.VersionColDev).
		Where(fmt.Sprintf("%s = ? and %s = ?", data.VersionColPackage, data.VersionColName), pkg, version).
		Update(&data.Version{
			Description: description,
			Dev:         dev,
		})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if n == 0 {
		var ok bool
		// 驗證 是否存在
		ok, e = session.Where(fmt.Sprintf("%s = ? and %s = ?", data.VersionColPackage, data.VersionColName), pkg, version).Get(&data.Version{})
		if e != nil {
			if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		} else if !ok {
			e = fmt.Errorf("update version [%v] [%v] not found", pkg, version)
			return
		}
		return
	}
	return
}

// List 返回指定 套件的 所有 版本信息
func (Version) List(pkg string) (beans []data.Version, e error) {
	e = Engine().Where(data.VersionColPackage+" like ?", pkg).Find(&beans)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	return
}

// Remove 刪除 套件 版本
func (Version) Remove(pkg, version string) (e error) {
	pkg = strings.TrimSpace(pkg)
	version = strings.TrimSpace(version)
	if pkg == "" || version == "" {
		e = errParamError
		return
	}

	var session *xorm.Session
	session, e = NewTransaction()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	defer func() {
		if e == nil {
			session.Commit()
			Engine().ClearCache(&data.Version{})
		} else {
			session.Rollback()
		}
	}()

	var n int64
	n, e = session.Delete(&data.Version{
		Package: pkg,
		Name:    version,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if n == 0 {
		e = fmt.Errorf("package version not found [%v] [%v]", pkg, version)
		return
	}

	// 刪除 數據文件
	e = os.Remove(VersionFilename(pkg, version))
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "system error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	return
}
