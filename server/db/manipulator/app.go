package manipulator

import (
	"fmt"
	"os"
	"regexp"
	"sort"
	"strings"

	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/auto-deploy/server/logger"
	"go.uber.org/zap"
)

// App .
type App struct {
}

// Search 查找 application
func (App) Search(names []string, dev bool) (apps []data.App, e error) {
	if len(names) == 0 {
		return
	}
	keys := make(map[string]bool)
	for _, name := range names {
		name = strings.TrimSpace(name)
		if name == "" {
			continue
		}
		keys[name] = true
	}
	if len(keys) == 0 {
		return
	}
	names = names[:len(keys)]
	i := 0
	for name := range keys {
		names[i] = name
		i++
	}

	// 查找 套件
	var pkgs []data.Package
	engine := Engine()
	e = engine.In(data.PackageColName, names).Find(&pkgs)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if len(pkgs) != len(keys) {
		for i := 0; i < len(pkgs); i++ {
			delete(keys, pkgs[i].Name)
		}
		arrs := make([]string, len(keys))
		i := 0
		for name := range keys {
			arrs[i] = name
			i++
		}
		e = fmt.Errorf("package not found : %v", arrs)
		return
	}

	arrs := make([]data.App, len(pkgs))
	for i := 0; i < len(pkgs); i++ {
		pkg := &pkgs[i]
		var versions []data.Version
		if dev {
			e = engine.Find(&versions, &data.Version{
				Package: pkg.Name,
			})
		} else {
			e = engine.Where(data.VersionColDev+" = ?", false).Find(&versions, &data.Version{
				Package: pkg.Name,
			})
		}
		if e != nil {
			if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		}
		if len(versions) == 0 {
			e = fmt.Errorf("package [%v] not found version", pkg.Name)
			return
		}
		sort.Sort(data.Versions(versions))
		v := &versions[len(versions)-1]
		arrs[i] = data.App{
			Package:     pkg.Name,
			Description: pkg.Description,
			Version:     v.Name,
			Dev:         v.Dev,
			Hash:        v.Hash,
		}
	}
	apps = arrs
	return
}

// DownloadInformation 返回 app 下載 信息
func (App) DownloadInformation(pkg, version string) (description string, dev bool, size int64, hash string, e error) {
	pkg = strings.TrimSpace(pkg)
	version = strings.TrimSpace(version)
	if pkg == "" || version == "" {
		e = fmt.Errorf("app not found [%v] [%v]", pkg, version)
		return
	}

	bean := &data.Package{
		Name: pkg,
	}
	var ok bool
	ok, e = Engine().Get(bean)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if !ok {
		e = fmt.Errorf("package not found [%v]", pkg)
		return
	}
	description = bean.Description
	findVersion := &data.Version{
		Name:    version,
		Package: pkg,
	}
	ok, e = Engine().Get(findVersion)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if !ok {
		e = fmt.Errorf("version not found [%v] [%v]", pkg, version)
		return
	}
	dev = findVersion.Dev

	filename := VersionFilename(pkg, version)
	var info os.FileInfo
	info, e = os.Stat(filename)
	if e != nil {
		return
	} else if info.IsDir() {
		e = fmt.Errorf("app data not found [%v] [%v]", pkg, version)
		return
	}
	size = info.Size()
	hash = findVersion.Hash
	return
}

// FindPackage 查找 套件
func (App) FindPackage(names []string) (arrs []data.Package, e error) {
	if len(names) == 0 {
		return
	}
	keys := make(map[string]*regexp.Regexp)
	for _, name := range names {
		name = strings.TrimSpace(name)
		if name == "" {
			continue
		}
		if _, ok := keys[name]; ok {
			continue
		}
		var r *regexp.Regexp
		r, e = regexp.Compile(name)
		if e == nil {
			keys[name] = r
		} else {
			if ce := logger.Logger.Check(zap.ErrorLevel, "regexp compile error"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.String("str", name),
				)
			}
		}
	}
	if len(keys) == 0 {
		return
	}

	// 查找 套件
	var pkgs []data.Package
	e = Engine().Find(&pkgs)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if len(pkgs) == 0 {
		return
	}
	arrs = make([]data.Package, 0, 10)
	for i := 0; i < len(pkgs); i++ {
		name := pkgs[i].Name
		yes := false
		for _, r := range keys {
			if r.MatchString(name) {
				yes = true
				break
			}
		}
		if yes {
			arrs = append(arrs, pkgs[i])
		}
	}

	return
}

// FindPackageEx 查找 套件
func (App) FindPackageEx(names []string) (infos []*data.PackageInfo, e error) {
	if len(names) == 0 {
		return
	}
	keys := make(map[string]*regexp.Regexp)
	for _, name := range names {
		name = strings.TrimSpace(name)
		if name == "" {
			continue
		}
		if _, ok := keys[name]; ok {
			continue
		}
		var r *regexp.Regexp
		r, e = regexp.Compile(name)
		if e == nil {
			keys[name] = r
		} else {
			if ce := logger.Logger.Check(zap.ErrorLevel, "regexp compile error"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.String("str", name),
				)
			}
		}
	}
	if len(keys) == 0 {
		return
	}

	// 查找 套件
	var pkgs []data.Package
	e = Engine().Find(&pkgs)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if len(pkgs) == 0 {
		return
	}
	arrs := make([]*data.PackageInfo, 0, 10)
	names = names[:0]
	cache := make(map[string]*data.PackageInfo)
	for i := 0; i < len(pkgs); i++ {
		name := pkgs[i].Name
		yes := false
		for _, r := range keys {
			if r.MatchString(name) {
				yes = true
				break
			}
		}
		if yes {
			node := &data.PackageInfo{
				Name:        name,
				Description: pkgs[i].Description,
			}
			arrs = append(arrs, node)
			names = append(names, name)
			cache[name] = node
		}
	}
	// 查找 版本
	var vs []data.Version
	e = Engine().In(data.VersionColPackage, names).Find(&vs)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if len(vs) != 0 {
		sort.Sort(data.Versions(vs))
		for i := 0; i < len(vs); i++ {
			v := &vs[i]
			pkg := v.Package
			if node, ok := cache[pkg]; ok {
				if node.Version == nil {
					node.Version = make([]data.VersionInfo, 1, 10)
					node.Version[0] = data.VersionInfo{
						Name:        v.Name,
						Description: v.Description,
						Dev:         v.Dev,
					}
				} else {
					node.Version = append(node.Version, data.VersionInfo{
						Name:        v.Name,
						Description: v.Description,
						Dev:         v.Dev,
					})
				}
			}
		}
	}
	infos = arrs
	return
}

// Upgrade 查找 可升級 版本
func (App) Upgrade(pkg, version string, dev bool) (bean *data.Version, e error) {
	pkg = strings.TrimSpace(pkg)
	version = strings.TrimSpace(version)
	if pkg == "" || version == "" {
		e = fmt.Errorf("package not found [%v] [%v]", pkg, version)
		return
	}
	if !data.MatchVersion(version) {
		e = fmt.Errorf("package is not a version [%v] [%v]", pkg, version)
		return
	}
	var beans []data.Version
	e = Engine().Find(&beans, &data.Version{Package: pkg})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "db error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if len(beans) == 0 {
		e = fmt.Errorf("package not found [%v] [%v]", pkg, version)
		return
	}
	sort.Sort(data.Versions(beans))
	for i := 0; i < len(beans); i++ {
		if data.VersionLess(version, beans[i].Name) {
			if !dev && beans[i].Dev {
				break
			}
			bean = &beans[i]
			break
		}
	}
	// for i := 0; i < len(beans); i++ {
	// 	if beans[i].Name == version {
	// 		i++
	// 		if i != len(beans) {
	// 			if !dev && beans[i].Dev {
	// 				return
	// 			}
	// 			bean = &beans[i]
	// 		}
	// 		return
	// 	}
	// }
	// e = fmt.Errorf("package not found [%v] [%v]", pkg, version)
	return
}
