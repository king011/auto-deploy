package data

import (
	grpc_data "gitlab.com/king011/auto-deploy/protocol/data"
)

// App 一個 可安裝的 套件包
type App struct {
	// 所屬 套件
	Package string
	// 套件 功能 描述
	Description string
	// 套件 版本
	Version string
	// 是否爲 開發者版本
	Dev bool
	// 套件 當前版本 hash 值
	Hash string
}

// ConvertToPB 轉換到 protocol buffers
func (d *App) ConvertToPB() (clone *grpc_data.App) {
	clone = &grpc_data.App{
		Package:     d.Package,
		Description: d.Description,
		Version:     d.Version,
		Dev:         d.Dev,
		Hash:        d.Hash,
	}
	return
}
