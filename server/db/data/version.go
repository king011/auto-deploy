package data

import (
	"errors"
	grpc_data "gitlab.com/king011/auto-deploy/protocol/data"
	"regexp"
	"strconv"
	"strings"
)

const (
	// VersionColName .
	VersionColName = "name"
	// VersionColDescription .
	VersionColDescription = "description"
	// VersionColPackage .
	VersionColPackage = "package"
	// VersionColDev .
	VersionColDev = "dev"
)

var errVersionName = errors.New("version must be v.X.X.X example v0.0.1 v1.0.0")
var errPackageEmpty = errors.New("package can't be empty")
var matchVersionName = regexp.MustCompile(`^v\d+\.\d+\.\d+$`)
var matchVersionError = regexp.MustCompile(`^v0+\.0+\.0+$`)

// Version 套件的 一個 發行版本
type Version struct {
	ID int64 `json:"-" xorm:"pk autoincr 'id'"`
	// 版本號 必須是 vX.X.X 形式
	Name string `xorm:"index"`

	// 版本 描述
	Description string

	// 所屬套件包
	Package string `xorm:"index"`

	// 是否爲 開發者版本
	Dev bool

	// 套件 當前版本 hash 值
	Hash string `json:"-"`
}

// Format .
func (d *Version) Format() (e error) {
	name := strings.TrimSpace(d.Name)
	description := strings.TrimSpace(d.Description)
	pkg := strings.TrimSpace(d.Package)

	if name == "" || name == "v0.0.0" {
		e = errVersionName
		return
	}
	if pkg == "" {
		e = errPackageEmpty
		return
	}
	if !MatchVersion(name) {
		e = errVersionName
		return
	}

	d.Name = name
	d.Description = description
	d.Package = pkg
	return
}

// ConvertToPB 轉換到 protocol buffers
func (d *Version) ConvertToPB() (clone *grpc_data.Version) {
	clone = &grpc_data.Version{
		ID:          d.ID,
		Name:        d.Name,
		Description: d.Description,
		Package:     d.Package,
		Dev:         d.Dev,
		Hash:        d.Hash,
	}
	return
}

// Versions .
type Versions []Version

// Len .
func (arrs Versions) Len() int {
	return len(arrs)
}

// Swap .
func (arrs Versions) Swap(l, r int) {
	arrs[l], arrs[r] = arrs[r], arrs[l]
}

// VersionLess 比較 版本 字符串
func VersionLess(l, r string) (yes bool) {
	sl := strings.SplitN(l[1:], ".", 3)
	sr := strings.SplitN(r[1:], ".", 3)
	if len(sl) != 3 {
		return
	} else if len(sr) != 3 {
		yes = true
		return
	}
	nl, _ := strconv.ParseUint(sl[0], 10, 64)
	nr, _ := strconv.ParseUint(sr[0], 10, 64)
	if nl < nr {
		yes = true
		return
	} else if nl > nr {
		return
	}

	nl, _ = strconv.ParseUint(sl[1], 10, 64)
	nr, _ = strconv.ParseUint(sr[1], 10, 64)
	if nl < nr {
		yes = true
		return
	} else if nl > nr {
		return
	}

	nl, _ = strconv.ParseUint(sl[2], 10, 64)
	nr, _ = strconv.ParseUint(sr[2], 10, 64)
	if nl < nr {
		yes = true
		return
	} else if nl > nr {
		return
	}
	return
}

// Less .
func (arrs Versions) Less(l, r int) (yes bool) {
	yes = VersionLess(arrs[l].Name, arrs[r].Name)
	return
}

// MatchVersion 驗證 字符串 是否是 version
func MatchVersion(name string) (yes bool) {
	if name == "" || name == "v0.0.0" {
		return
	}
	if !matchVersionName.MatchString(name) {
		return
	}
	if matchVersionError.MatchString(name) {
		return
	}
	yes = true
	return
}
