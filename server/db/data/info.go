package data

// VersionInfo 版本 詳情
type VersionInfo struct {
	// 套件 版本
	Name string

	// 套件 功能 描述
	Description string

	// 是否爲 開發者版本
	Dev bool
}

// PackageInfo 套件包 詳情
type PackageInfo struct {
	// 套件 名稱
	Name string `xorm:"pk"`

	// 套件 功能 描述
	Description string

	Version []VersionInfo
}
