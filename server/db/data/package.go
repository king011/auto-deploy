package data

import (
	"bytes"
	"encoding/gob"

	grpc_data "gitlab.com/king011/auto-deploy/protocol/data"
	"gitlab.com/king011/auto-deploy/server/logger"
	"go.uber.org/zap"
)

const (
	// PackageColName .
	PackageColName = "name"
	// PackageColDescription .
	PackageColDescription = "description"
	// PackageColKeys .
	PackageColKeys = "keys"
	// PackageColUsers .
	PackageColUsers = "users"
)

// Package 一個 套件包
type Package struct {
	// 套件 名稱
	Name string `xorm:"pk"`

	// 套件 所屬 用戶
	Users []byte

	// 套件 功能 描述
	Description string

	// 允許下載 套件的 授權列表 如果爲空
	// 則允許任何人下載 如果要阻止任何人下載 可以設置一個無效的 key (比如 m["-"]="disable" )
	Keys []byte
}

// ConvertToPB 轉換到 protocol buffers
func (d *Package) ConvertToPB() (clone *grpc_data.Package) {
	clone = &grpc_data.Package{
		Name:        d.Name,
		Description: d.Description,
	}
	if len(d.Keys) != 0 {
		var m map[string]string
		dec := gob.NewDecoder(bytes.NewBuffer(d.Keys))
		e := dec.Decode(&m)
		if e == nil {
			if m != nil {
				clone.Keys = make([]*grpc_data.PackageKey, 0, len(m))
				for k, v := range m {
					clone.Keys = append(clone.Keys,
						&grpc_data.PackageKey{
							Key:         k,
							Description: v,
						},
					)
				}
			}
		} else {
			if ce := logger.Logger.Check(zap.ErrorLevel, "Package ConvertToPB error"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
		}
	}
	return
}

// GetKeys 返回 keys 內存 結構
func (d *Package) GetKeys() (m map[string]string) {
	if len(d.Keys) == 0 {
		return
	}
	dec := gob.NewDecoder(bytes.NewBuffer(d.Keys))
	e := dec.Decode(&m)
	if e != nil {
		m = nil
		if ce := logger.Logger.Check(zap.ErrorLevel, "Package GetKeys error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	}
	return
}

// SetKeys 將 內存數據 編碼到 二進制 以便存儲到數據庫
func (d *Package) SetKeys(m map[string]string) {
	if len(m) == 0 {
		d.Keys = nil
	} else {
		var buf bytes.Buffer
		enc := gob.NewEncoder(&buf)
		e := enc.Encode(m)
		if e == nil {
			d.Keys = buf.Bytes()
		} else {
			if ce := logger.Logger.Check(zap.ErrorLevel, "Package SetKeys error"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
		}
	}
}

// GetUsers 返回 users 內存 結構
func (d *Package) GetUsers() (m map[string]bool) {
	if len(d.Users) == 0 {
		return
	}
	dec := gob.NewDecoder(bytes.NewBuffer(d.Users))
	e := dec.Decode(&m)
	if e != nil {
		m = nil
		if ce := logger.Logger.Check(zap.ErrorLevel, "Package GetUsers error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	}
	return
}

// SetUsers  將 內存數據 編碼到 二進制 以便存儲到數據庫
func (d *Package) SetUsers(m map[string]bool) {
	if len(m) == 0 {
		d.Users = nil
	} else {
		var buf bytes.Buffer
		enc := gob.NewEncoder(&buf)
		e := enc.Encode(m)
		if e == nil {
			d.Users = buf.Bytes()
		} else {
			if ce := logger.Logger.Check(zap.ErrorLevel, "Package SetUsers error"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
		}
	}
}
