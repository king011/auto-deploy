package data_test

import (
	"gitlab.com/king011/auto-deploy/server/db/data"
	"testing"
)

func TestLess(t *testing.T) {
	l := "v0.0.1"
	r := "v10.0.0"
	if !data.VersionLess(l, r) {
		t.Fatalf("[%s] not less [%s]", l, r)
	}
}
func TestMatch(t *testing.T) {
	v := data.Version{
		Name:    "v0.0.0",
		Package: "pkg",
	}
	e := v.Format()
	if e == nil {
		t.Fatalf("[%v] match", v.Name)
	}

	v.Name = "v1234.456.1"
	e = v.Format()
	if e != nil {
		t.Fatalf("[%v] %v", v.Name, e)
	}

	v.Name = "v1234.456.7655"
	e = v.Format()
	if e != nil {
		t.Fatalf("[%v] %v", v.Name, e)
	}

	v.Name = "v00.00.00"
	e = v.Format()
	if e == nil {
		t.Fatalf("[%v] matched", v.Name)
	}
}
