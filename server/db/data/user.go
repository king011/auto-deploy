package data

// User 用戶表
type User struct {
	// 用戶名
	Name string `xorm:"pk"`
	// 密碼 md5
	Password string

	// 昵稱
	Nickname string

	// 是否是 管理員
	Root bool
}
