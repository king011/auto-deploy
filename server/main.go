package main

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/king011/auto-deploy/server/cmd"
)

func main() {
	cmd.Execute()
}
