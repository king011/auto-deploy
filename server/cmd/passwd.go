package cmd

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"log"
	"os"
	"time"

	"github.com/spf13/cobra"
	grpc_session "gitlab.com/king011/auto-deploy/protocol/session"
	"gitlab.com/king011/auto-deploy/server/cmd/client"
)

func init() {
	var filename string

	var user, password string
	cmd := &cobra.Command{
		Use:   "passwd",
		Short: "change password",
		Run: func(cmd *cobra.Command, args []string) {
			if password == "" {
				cmd.Help()
				os.Exit(1)
				return
			}
			b := md5.Sum([]byte(password))
			password = hex.EncodeToString(b[:])

			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
			}
			defer conn.Close()
			client := grpc_session.NewUserClient(conn)
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
			_, e = client.Password(ctx,
				&grpc_session.PasswordRequest{
					User:     user,
					Password: password,
				},
			)
			cancel()
			if e != nil {
				log.Fatalln(e)
			}
			log.Println("change password success")
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.StringVarP(&user,
		"user", "u",
		"",
		"user name",
	)
	flags.StringVarP(&password,
		"password", "p",
		"",
		"user password",
	)
	rootCmd.AddCommand(cmd)
}
