package cmd

import (
	"fmt"
	"os"
	"runtime"

	"github.com/spf13/cobra"
	"gitlab.com/king011/auto-deploy/utils"
	"gitlab.com/king011/auto-deploy/version"
)

const (
	// App 程式名
	App = "server"
)

var v bool
var rootCmd = &cobra.Command{
	Use:   App,
	Short: "auto-deploy server",
	Run: func(cmd *cobra.Command, args []string) {
		if v {
			fmt.Println(runtime.GOOS, runtime.GOARCH)
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
		} else {
			fmt.Println(runtime.GOOS, runtime.GOARCH)
			fmt.Println(App)
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
			fmt.Printf(`Use "%v --help" for more information about this program.
`, App)
		}
	},
}
var _ConfigureFile string

// ConfigureFile .
func ConfigureFile() string {
	if _ConfigureFile == "" {
		_ConfigureFile = utils.BasePath() + "/etc/server.jsonnet"
	}
	return _ConfigureFile
}
func init() {
	flags := rootCmd.Flags()
	flags.BoolVarP(&v,
		"version",
		"v",
		false,
		"show version",
	)
}

// Execute 執行命令
func Execute() error {
	return rootCmd.Execute()
}
func abort() {
	os.Exit(1)
}
