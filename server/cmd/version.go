package cmd

import (
	"archive/tar"
	"bufio"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	grpc_data "gitlab.com/king011/auto-deploy/protocol/data"
	grpc_version "gitlab.com/king011/auto-deploy/protocol/version"
	"gitlab.com/king011/auto-deploy/server/cmd/client"
	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/king-go/os/fileperm"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

func init() {
	var filename string

	var init, update, push bool
	var remove, list, pkg string

	cmd := &cobra.Command{
		Use:   "version",
		Short: "package version management",
		Long: `package version management
  # create a new package version on current directory
  version -i

  # push new version to server
  version -p

  # update version info to server
  version -u

  # remove version 
  version -r vX.X.X --package pkg

  # list package all version
  version -l pkg
`,
		Run: func(cmd *cobra.Command, args []string) {
			remove = strings.TrimSpace(remove)
			list = strings.TrimSpace(list)
			if init {
				var v data.Version
				r := bufio.NewReader(os.Stdin)
				fmt.Print("input package name >")
				b, _, _ := r.ReadLine()
				v.Package = strings.TrimSpace(string(b))
				fmt.Print("input version name [vX.X.X] >")
				b, _, _ = r.ReadLine()
				v.Name = strings.TrimSpace(string(b))
				fmt.Print("input version description >")
				b, _, _ = r.ReadLine()
				v.Description = strings.TrimSpace(string(b))
				fmt.Print("input dev [no] >")
				b, _, _ = r.ReadLine()
				str := strings.ToLower(strings.TrimSpace(string(b)))
				if str == "true" || str == "yes" || str == "1" {
					v.Dev = true
				}
				var e error
				b, e = json.MarshalIndent(&v, "", "    ")
				if e != nil {
					log.Fatalln(e)
				}
				e = ioutil.WriteFile("auto-deploy.json", b, fileperm.File)
				if e != nil {
					log.Fatalln(e)
				}
				if _, e = os.Stat("process.js"); os.IsNotExist(e) {
					ioutil.WriteFile("process.js", []byte(`
// this scripts define package's process

// variable __dirname is package installed path
// named __dirname for cosplay nodejs, but environment not nodejs, it's duktape
// https://duktape.org/api.html
var dir = __dirname;

// exports cosplay nodejs
// export Service array, it's process group.
exports.Service = [
	// every element is a process
	{
		// how to start process
		Start: [dir + "/main.js"],
		//Start: [dir + "/main.js" , "daemon"],
		// how to stop process, if invalid will kill process
		Stop: undefined,
		//Stop: [dir + "/main.js" , "kill"],
		// work directory, if invalid will use parent's work directory.
		Dir: dir,
	},
]
`), fileperm.File)
				}
				fmt.Printf("init success\n%s\n", b)
			} else if push {
				versionPush(filename)
			} else if update {
				versionUpdate(filename)
			} else if remove != "" {
				pkg = strings.TrimSpace(pkg)
				versionRemove(filename, pkg, remove)
			} else if list != "" {
				versionList(filename, list)
			} else {
				cmd.Help()
			}
		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.BoolVarP(&init,
		"init", "i",
		false,
		"create a new package version on current directory",
	)
	flags.BoolVarP(&push,
		"push", "p",
		false,
		"push new version to server",
	)
	flags.BoolVarP(&update,
		"update", "u",
		false,
		"update version info to server",
	)

	flags.StringVarP(&remove,
		"remove", "r",
		"",
		"remove a package version",
	)
	flags.StringVar(&pkg,
		"package",
		"",
		"package name olny use on remove",
	)
	flags.StringVarP(&list,
		"list", "l",
		"",
		"list package all version",
	)

	rootCmd.AddCommand(cmd)
}
func versionRemove(filename, pkg, v string) {
	pkg = strings.TrimSpace(pkg)
	if pkg == "" {
		log.Fatalln("package can't be empty")
	}

	// 創建 grpc 客戶端
	conn, e := client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()

	client := grpc_version.NewVersionClient(conn)
	_, e = client.Remove(context.Background(),
		&grpc_version.RemoveRequest{
			Package: pkg,
			Version: v,
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Printf("remove version success [%v] [%v]\n", pkg, v)
}
func versionList(filename, pkg string) {
	// 創建 grpc 客戶端
	conn, e := client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()

	client := grpc_version.NewVersionClient(conn)
	var reply *grpc_version.ListReply
	reply, e = client.List(context.Background(),
		&grpc_version.ListRequest{
			Package: pkg,
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	if len(reply.Data) == 0 {
		fmt.Println("not found any version")
		return
	}
	sort.Sort(versionSort(reply.Data))
	fmt.Println("package", reply.Data[0].Package)
	for i := 0; i < len(reply.Data); i++ {
		fmt.Println("\n  version =", reply.Data[i].Name)
		fmt.Println("  description =", reply.Data[i].Description)
		fmt.Println("  dev =", reply.Data[i].Dev)
		fmt.Println("  hash =", reply.Data[i].Hash)
	}
}

type versionSort []*grpc_data.Version

func (s versionSort) Len() int {
	return len(s)
}
func (s versionSort) Less(l, r int) (yes bool) {
	if s[l] == nil {
		yes = true
		return
	} else if s[r] == nil {
		return
	}

	sl := strings.SplitN(s[l].Name, ".", 3)
	sr := strings.SplitN(s[r].Name, ".", 3)
	if len(sl) != 3 {
		return
	} else if len(sr) != 3 {
		yes = true
		return
	}
	nl, _ := strconv.ParseUint(sl[0], 10, 64)
	nr, _ := strconv.ParseUint(sr[0], 10, 64)
	if nl < nr {
		yes = true
		return
	} else if nl > nr {
		return
	}

	nl, _ = strconv.ParseUint(sl[1], 10, 64)
	nr, _ = strconv.ParseUint(sr[1], 10, 64)
	if nl < nr {
		yes = true
		return
	} else if nl > nr {
		return
	}

	nl, _ = strconv.ParseUint(sl[2], 10, 64)
	nr, _ = strconv.ParseUint(sr[2], 10, 64)
	if nl < nr {
		yes = true
		return
	} else if nl > nr {
		return
	}
	return
}
func (s versionSort) Swap(l, r int) {
	s[l], s[r] = s[r], s[l]
}
func versionUpdate(filename string) {
	// 讀取 版本 信息
	b, e := ioutil.ReadFile("auto-deploy.json")
	if e != nil {
		log.Fatalln(e)
	}
	var v data.Version
	e = json.Unmarshal(b, &v)
	if e != nil {
		log.Fatalln(e)
	}
	e = v.Format()
	if e != nil {
		log.Fatalln(e)
	}

	// 創建 grpc 客戶端
	var conn *grpc.ClientConn
	conn, e = client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	client := grpc_version.NewVersionClient(conn)

	_, e = client.Update(context.Background(),
		&grpc_version.UpdateRequest{
			Package:     v.Package,
			Name:        v.Name,
			Description: v.Description,
			Dev:         v.Dev,
		},
	)
	if e != nil {
		log.Fatalln(e)
		return
	}
	fmt.Println("update success", v.Package, v.Name)
}
func versionPush(filename string) {
	// 讀取 版本 信息
	b, e := ioutil.ReadFile("auto-deploy.json")
	if e != nil {
		log.Fatalln(e)
	}
	var v data.Version
	e = json.Unmarshal(b, &v)
	if e != nil {
		log.Fatalln(e)
	}
	e = v.Format()
	if e != nil {
		log.Fatalln(e)
	}

	// 創建 grpc 客戶端
	var conn *grpc.ClientConn
	conn, e = client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	client := grpc_version.NewVersionClient(conn)
	var stream grpc_version.Version_PushClient
	stream, e = client.Push(context.Background())
	if e != nil {
		log.Fatalln(e)
	}

	// 驗證 版本 信息
	e = stream.Send(
		&grpc_version.PushRequest{
			Package:     v.Package,
			Name:        v.Name,
			Description: v.Description,
			Dev:         v.Dev,
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	_, e = stream.Recv()
	if e != nil {
		log.Fatalln(e)
	}

	// 上傳 檔案 數據
	e = versionPushData(stream)
	if e != nil {
		log.Fatalln(e)
	}
	_, e = stream.Recv()
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println("push success", v.Package, v.Name)
}

type _PushRequestWriter struct {
	buffer []byte
	pos    int
	stream grpc_version.Version_PushClient
	e      error
}

func (w *_PushRequestWriter) Write(data []byte) (n int, e error) {
	if w.e != nil {
		e = w.e
		return
	}
	send := len(data)
	for send != 0 {
		free := len(w.buffer) - w.pos
		if send > free {
			send = free
		}
		copy(w.buffer[w.pos:], data[:send])
		w.pos += send
		data = data[send:]
		send = len(data)
		n += send

		if w.pos == len(w.buffer) {
			// 發送 數據
			e = w.stream.Send(&grpc_version.PushRequest{
				Data: w.buffer,
			})
			if e != nil {
				w.e = e
				return
			}
			w.pos = 0
		}
	}
	return
}
func (w *_PushRequestWriter) Close() (e error) {
	if w.e != nil {
		e = w.e
		return
	}
	if w.pos != 0 {
		e = w.stream.Send(&grpc_version.PushRequest{
			Data: w.buffer[:w.pos],
		})
		if e != nil {
			w.e = e
			return
		}
		w.pos = 0
	}
	return
}
func versionPushData(stream grpc_version.Version_PushClient) (e error) {
	// 創建 grpc writer
	w := &_PushRequestWriter{
		buffer: make([]byte, 1024*16),
		stream: stream,
	}
	// 使用 gzip壓縮
	gw := gzip.NewWriter(w)
	// 返回 *Writer
	tw := tar.NewWriter(gw)

	// 查找 檔案
	e = filepath.Walk(".", func(path string, info os.FileInfo, err error) (e error) {
		if err != nil {
			e = err
			return
		}
		if info.IsDir() {
			return nil
		}
		// if info.Name() == "auto-deploy.json" {
		// 	return nil
		// }
		path = strings.Replace(path, "\\", "/", -1)
		fmt.Println("push file :", path)
		e = tarAddFile(tw, path, info)
		return
	})
	if e != nil {
		return
	}
	tw.Close()
	gw.Close()
	w.Close()

	e = stream.CloseSend()
	return
}
func tarAddFile(w *tar.Writer, name string, info os.FileInfo) (e error) {
	f, e := os.Open(name)
	if e != nil {
		return
	}
	defer f.Close()

	header := &tar.Header{
		Typeflag: tar.TypeReg,
		Name:     name,
		Mode:     int64(info.Mode()),
		// Uid:      os.Getuid(), // win32 not work
		// Gid:      os.Getgid(),// win32 not work
		Size:    info.Size(),
		ModTime: info.ModTime(),
	}
	if e = w.WriteHeader(header); e != nil {
		return
	}

	_, e = io.Copy(w, f)

	return
}
