package cmd

import (
	"fmt"
	"log"
	"strings"

	"github.com/spf13/cobra"
	grpc_package "gitlab.com/king011/auto-deploy/protocol/package"
	"gitlab.com/king011/auto-deploy/server/cmd/client"
	"golang.org/x/net/context"
)

func init() {
	var filename string

	var search, get, init, remove, update bool
	var name, description, addKey, deleteKey string
	var setUser, addUser, removeUser []string
	var user bool
	cmd := &cobra.Command{
		Use:   "package",
		Short: "package management",
		Long: `package management
  # create a new package named npkg
  server package -i -n npkg -d "init example"

  # update package description
  server package -i -u npkg -d "update example"

  # search package name like ?
  server package -s -n %pkg%"

  # remove package 
  server package -r -n npkg

  # disable everyone download package npkg
  server package -n npkg --delete *
  server package -n npkg --add "-"
`,
		Run: func(cmd *cobra.Command, args []string) {
			addKey = strings.TrimSpace(addKey)
			deleteKey = strings.TrimSpace(deleteKey)
			name = strings.TrimSpace(name)
			description = strings.TrimSpace(description)
			if init {
				if name == "" {
					log.Fatalln("init package name can't be empty")
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_package.NewPackageClient(conn)
				_, e = client.Init(context.Background(),
					&grpc_package.InitRequest{
						Name:        name,
						Description: description,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}

				fmt.Printf("init package [%s] [%s]\n", name, description)
			} else if update {
				if name == "" {
					log.Fatalln("update package name can't be empty")
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_package.NewPackageClient(conn)
				_, e = client.Update(context.Background(),
					&grpc_package.UpdateRequest{
						Name:        name,
						Description: description,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}

				fmt.Printf("update package [%v] [%v]\n", name, description)
			} else if remove {
				if name == "" {
					log.Fatalln("remove package name can't be empty")
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_package.NewPackageClient(conn)
				_, e = client.Remove(context.Background(),
					&grpc_package.RemoveRequest{
						Name: name,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}

				fmt.Printf("remove package [%v]\n", name)
			} else if get {
				if name == "" {
					log.Fatalln("get package name can't be empty")
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_package.NewPackageClient(conn)
				var reply *grpc_package.GetReply
				reply, e = client.Get(context.Background(),
					&grpc_package.GetRequest{
						Name: name,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Println("package :", reply.Data.Name)
				fmt.Println(" --", reply.Data.Description)
				fmt.Println(" **", reply.Data.Keys)
			} else if search {
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_package.NewPackageClient(conn)
				var reply *grpc_package.SearchReply
				reply, e = client.Search(context.Background(),
					&grpc_package.SearchRequest{
						Name: name,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}
				if len(reply.Data) == 0 {
					fmt.Println("not found any package")
				} else {
					for i, node := range reply.Data {
						if i == 0 {
							fmt.Println("package :", node.Name)
						} else {
							fmt.Println("\npackage :", node.Name)
						}
						fmt.Println(" --", node.Description)
						fmt.Println(" **", node.Keys)
					}
				}
			} else if addKey != "" {
				if name == "" {
					log.Fatalln("add key package name can't be empty")
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_package.NewPackageClient(conn)
				_, e = client.Add(context.Background(),
					&grpc_package.AddRequest{
						Name:        name,
						Key:         addKey,
						Description: description,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Printf("add key [%v] [%v] [%v]\n", name, addKey, description)
			} else if deleteKey != "" {
				if name == "" {
					log.Fatalln("delete key package name can't be empty")
				}
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_package.NewPackageClient(conn)
				_, e = client.Delete(context.Background(),
					&grpc_package.DeleteRequest{
						Name: name,
						Key:  deleteKey,
					},
				)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Printf("delete [%v] [%v]\n", name, deleteKey)
			} else if user {
				packageUser(filename, name)
			} else if len(setUser) != 0 {
				packageSetUser(filename, name, setUser)
			} else if len(addUser) != 0 {
				packageAddUser(filename, name, addUser)
			} else if len(removeUser) != 0 {
				packageRemoveUser(filename, name, removeUser)
			} else {
				cmd.Help()
			}
		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.BoolVarP(&search,
		"search", "s",
		false,
		"search package name like ?",
	)
	flags.BoolVarP(&get,
		"get", "g",
		false,
		"get package info",
	)
	flags.BoolVarP(&init,
		"init", "i",
		false,
		"init a new package",
	)
	flags.BoolVarP(&remove,
		"remove", "r",
		false,
		"remove a package",
	)
	flags.BoolVarP(&update,
		"update", "u",
		false,
		"update package description",
	)
	flags.StringVarP(&name,
		"name", "n",
		"",
		"package name",
	)
	flags.StringVarP(&description,
		"description", "d",
		"",
		"if (init) package description else if (add) key description",
	)
	flags.StringVar(&addKey,
		"add",
		"",
		"add a key allow download package key",
	)
	flags.StringVar(&deleteKey,
		"delete",
		"",
		"delete all match key",
	)
	flags.StringSliceVar(&setUser,
		"set-user",
		nil,
		"set user for package",
	)
	flags.StringSliceVar(&addUser,
		"add-user",
		nil,
		"add user for package",
	)
	flags.StringSliceVar(&removeUser,
		"remove-user",
		nil,
		"remove user for package",
	)

	flags.BoolVar(&user,
		"user",
		false,
		"display package's user",
	)
	rootCmd.AddCommand(cmd)
}
func packageUser(filename, name string) {
	if name == "" {
		log.Fatalln("display package's user, name can't be empty")
	}
	conn, e := client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	client := grpc_package.NewPackageClient(conn)
	reply, e := client.User(context.Background(),
		&grpc_package.UserRequest{
			Name: name,
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("user  :", reply.Data)
}
func packageSetUser(filename, name string, strs []string) {
	if name == "" {
		log.Fatalln("set users for package, name can't be empty")
	}
	conn, e := client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	client := grpc_package.NewPackageClient(conn)
	_, e = client.SetUser(context.Background(),
		&grpc_package.SetUserRequest{
			Name: name,
			Data: strs,
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("set user success :", name, strs)
}
func packageAddUser(filename, name string, strs []string) {
	if name == "" {
		log.Fatalln("add users for package, name can't be empty")
	}
	conn, e := client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	client := grpc_package.NewPackageClient(conn)
	_, e = client.AddUser(context.Background(),
		&grpc_package.AddUserRequest{
			Name: name,
			Data: strs,
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("add user success :", name, strs)
}
func packageRemoveUser(filename, name string, strs []string) {
	if name == "" {
		log.Fatalln("remove users for package, name can't be empty")
	}
	conn, e := client.InitAndGet(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	client := grpc_package.NewPackageClient(conn)
	_, e = client.RemoveUser(context.Background(),
		&grpc_package.RemoveUserRequest{
			Name: name,
			Data: strs,
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("remove user success :", name, strs)
}
