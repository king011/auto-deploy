package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/king011/auto-deploy/server/cmd/daemon"
)

func init() {
	var filename string
	cmd := &cobra.Command{
		Use:   "daemon",
		Short: "run server as daemon",
		Run: func(cmd *cobra.Command, args []string) {
			daemon.Run(filename)
		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)

	rootCmd.AddCommand(cmd)
}
