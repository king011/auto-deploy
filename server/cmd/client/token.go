package client

import (
	"gitlab.com/king011/auto-deploy/server/cookie"
	"golang.org/x/net/context"
	"google.golang.org/grpc/credentials"
)

// token 自定義 token 結構
type token struct {
	safe   bool
	m      map[string]string
	cookie string
}

// NewToken 返回 一個 token
func NewToken(safe bool, appkey, cookieStr string) credentials.PerRPCCredentials {
	return token{
		safe: safe,
		m: map[string]string{
			"appkey":          appkey,
			cookie.CookieName: cookieStr,
		},
	}
}

// RequireTransportSecurity 返回 是否 需要 安全傳輸 token
// 如果 返回 true 則必須 使用 WithTransportCredentials 創建 客戶端 (既安全的 加密通道)
func (t token) RequireTransportSecurity() bool {
	return t.safe
}

// GetRequestMetadata 返回 token 值
func (t token) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	//返回 token
	return t.m, nil
}
