package cmd

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/spf13/cobra"
	grpc_session "gitlab.com/king011/auto-deploy/protocol/session"
	"gitlab.com/king011/auto-deploy/server/cmd/client"
	"gitlab.com/king011/auto-deploy/utils"
)

func init() {
	var filename string

	var user, password string
	var refresh bool
	cmd := &cobra.Command{
		Use:   "login",
		Short: "login and create session file",
		Run: func(cmd *cobra.Command, args []string) {
			if refresh {
				conn, e := client.InitAndGet(filename)
				if e != nil {
					log.Fatalln(e)
				}
				defer conn.Close()
				client := grpc_session.NewUserClient(conn)
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
				reply, e := client.Refresh(ctx,
					&grpc_session.RefreshRequest{},
				)
				cancel()
				if e != nil {
					log.Fatalln(e)
				}
				e = ioutil.WriteFile(utils.BasePath()+"/etc/cookie", []byte(reply.Cookie), 0666)
				if e != nil {
					log.Fatalln(e)
				}
				log.Println("refresh cookie success")
				return
			}
			if user == "" || password == "" {
				cmd.Help()
				os.Exit(1)
				return
			}
			b := md5.Sum([]byte(password))
			password = hex.EncodeToString(b[:])

			conn, e := client.InitAndGet(filename)
			if e != nil {
				log.Fatalln(e)
			}
			defer conn.Close()
			client := grpc_session.NewUserClient(conn)
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
			reply, e := client.Login(ctx,
				&grpc_session.LoginRequest{
					User:     user,
					Password: password,
				},
			)
			cancel()
			if e != nil {
				log.Fatalln(e)
			}
			e = ioutil.WriteFile(utils.BasePath()+"/etc/cookie", []byte(reply.Cookie), 0666)
			if e != nil {
				log.Fatalln(e)
			}
			log.Println("login success")
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		ConfigureFile(),
		"configure file",
	)
	flags.StringVarP(&user,
		"user", "u",
		"",
		"user name",
	)
	flags.StringVarP(&password,
		"password", "p",
		"",
		"user password",
	)
	flags.BoolVarP(&refresh,
		"refresh", "r",
		false,
		"refresh cookie",
	)
	rootCmd.AddCommand(cmd)
}
