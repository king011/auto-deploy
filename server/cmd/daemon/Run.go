package daemon

import (
	"log"

	"gitlab.com/king011/auto-deploy/server/configure"
	"gitlab.com/king011/auto-deploy/server/cookie"
	"gitlab.com/king011/auto-deploy/server/db/manipulator"
	"gitlab.com/king011/auto-deploy/server/logger"
	"gitlab.com/king011/auto-deploy/utils"
)

// Run 運行 指令
func Run(filename string) {
	// 加載 配置
	cnf, e := configure.Init(filename)
	if e != nil {
		log.Fatalln(e)
	}
	// 初始化 日誌
	logger.Init(utils.BasePath(), &cnf.Logger)

	// 初始化 cookie
	if cnf.Server.User {
		cookie.Init(cnf.Server.Secure, cnf.Server.MaxAge)
	}

	// 初始化 數據庫
	e = manipulator.Init()
	if e != nil {
		log.Fatalln(e)
	}

	// 運行 grpc 服務器
	runGRPC(&cnf.Server)
}
