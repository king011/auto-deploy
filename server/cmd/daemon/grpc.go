package daemon

import (
	"log"
	"net"
	"os"
	"strings"

	"go.uber.org/zap"

	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	grpc_package "gitlab.com/king011/auto-deploy/protocol/package"
	grpc_session "gitlab.com/king011/auto-deploy/protocol/session"
	grpc_version "gitlab.com/king011/auto-deploy/protocol/version"
	"gitlab.com/king011/auto-deploy/server/cmd/daemon/server"
	"gitlab.com/king011/auto-deploy/server/cmd/daemon/utils"
	"gitlab.com/king011/auto-deploy/server/configure"
	"gitlab.com/king011/auto-deploy/server/db/manipulator"
	"gitlab.com/king011/auto-deploy/server/logger"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

func grpcInterceptor(ctx context.Context,
	request interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler) (reply interface{}, e error) {
	if strings.HasPrefix(info.FullMethod, "/king011_autodeploy.package.") ||
		strings.HasPrefix(info.FullMethod, "/king011_autodeploy.version.") {
		e = utils.CheckPassword(ctx)
		if e != nil {
			return
		}
	}
	reply, e = handler(ctx, request)
	return
}
func grpcInterceptorStream(srv interface{},
	ss grpc.ServerStream,
	info *grpc.StreamServerInfo,
	handler grpc.StreamHandler) (e error) {
	if strings.HasPrefix(info.FullMethod, "/king011_autodeploy.package.") ||
		strings.HasPrefix(info.FullMethod, "/king011_autodeploy.version.") {
		e = utils.CheckPassword(ss.Context())
		if e != nil {
			return
		}
	}
	e = handler(srv, ss)
	return
}
func runGRPC(cnf *configure.Server) {
	// 創建 grpc 服務器
	var gs *grpc.Server
	safe := cnf.TLS()
	if safe {
		creds, e := credentials.NewServerTLSFromFile(cnf.Cert, cnf.Key)
		if e != nil {
			if ce := logger.Logger.Check(zap.FatalLevel, "NewServerTLSFromFile"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			os.Exit(1)
			return
		}
		gs = grpc.NewServer(
			// 設置 tls
			grpc.Creds(creds),
			//為服務 設置 攔截器
			grpc.UnaryInterceptor(grpcInterceptor),
			grpc.StreamInterceptor(grpcInterceptorStream),
		)
	} else {
		gs = grpc.NewServer(
			//為服務 設置 攔截器
			grpc.UnaryInterceptor(grpcInterceptor),
			grpc.StreamInterceptor(grpcInterceptorStream),
		)
	}

	//創建 監聽 Listener
	l, e := net.Listen("tcp", cnf.LAddr)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "ListenTCP"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
		return
	}
	defer l.Close()

	// 初始化 檔案系統
	e = manipulator.InitFilesystem()
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "InitFilesystem"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
		return
	}
	if safe {
		if !logger.Logger.OutConsole() {
			log.Println("grpc h2 work at " + cnf.LAddr)
		}
		if ce := logger.Logger.Check(zap.InfoLevel, "grpc h2 work"); ce != nil {
			ce.Write(
				zap.String("addr", cnf.LAddr),
			)
		}
	} else {
		if !logger.Logger.OutConsole() {
			log.Println("grpc h2c work at " + cnf.LAddr)
		}
		if ce := logger.Logger.Check(zap.InfoLevel, "grpc h2c work"); ce != nil {
			ce.Write(
				zap.String("addr", cnf.LAddr),
			)
		}
	}

	// 註冊 grpc 服務
	grpc_package.RegisterPackageServer(gs, server.Package{})
	grpc_version.RegisterVersionServer(gs, server.Version{})
	grpc_app.RegisterAppServer(gs, server.App{})
	grpc_session.RegisterUserServer(gs, server.Session{})

	// 註冊 反射 到 服務 路由
	reflection.Register(gs)

	// 讓 rpc 在 Listener 上 工作
	if e := gs.Serve(l); e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "grpc Serve"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	}
}
