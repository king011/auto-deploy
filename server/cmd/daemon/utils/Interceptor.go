package utils

import (
	"gitlab.com/king011/auto-deploy/server/configure"
	"gitlab.com/king011/auto-deploy/server/logger"
	"go.uber.org/zap"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

// CheckPassword 驗證 密碼
func CheckPassword(ctx context.Context) (e error) {
	// 解析metada中的信息并验证
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		e = grpc.Errorf(codes.Unauthenticated, "unknow token")
		if ce := logger.Logger.Check(zap.WarnLevel, "interceptor error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if pwd, ok := md["appkey"]; !ok {
		e = grpc.Errorf(codes.Unauthenticated, "unknow token")
		if ce := logger.Logger.Check(zap.WarnLevel, "interceptor error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if len(pwd) == 0 || pwd[0] != configure.Single().Server.Password {
		e = grpc.Errorf(codes.PermissionDenied, "token password not match")
		if ce := logger.Logger.Check(zap.WarnLevel, "interceptor error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	return
}
