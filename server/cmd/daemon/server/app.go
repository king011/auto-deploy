package server

import (
	"bytes"
	"crypto/md5"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"sync/atomic"

	grpc_app "gitlab.com/king011/auto-deploy/protocol/app"
	grpc_data "gitlab.com/king011/auto-deploy/protocol/data"
	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/auto-deploy/server/db/manipulator"
	"gitlab.com/king011/auto-deploy/server/logger"
	"gitlab.com/king011/auto-deploy/utils"
	"go.uber.org/zap"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

var errCancel = errors.New("action cancel")

// App 提供 套件包 查詢/下載
type App struct {
}

func (App) getAppkey(ctx context.Context) (key string, e error) {
	// 解析metada中的信息并验证
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		e = grpc.Errorf(codes.Unauthenticated, "unknow token")
		if ce := logger.Logger.Check(zap.WarnLevel, "interceptor error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if pwd, ok := md["appkey"]; !ok {
		e = grpc.Errorf(codes.Unauthenticated, "unknow token")
		if ce := logger.Logger.Check(zap.WarnLevel, "interceptor error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else if len(pwd) == 0 {
		e = grpc.Errorf(codes.PermissionDenied, "unknow token")
		if ce := logger.Logger.Check(zap.WarnLevel, "interceptor error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	} else {
		key = pwd[0]
	}
	return
}

// Search 查找 套件
func (App) Search(ctx context.Context, request *grpc_app.SearchRequest) (reply *grpc_app.SearchReply, e error) {
	var mApp manipulator.App
	var apps []data.App
	apps, e = mApp.Search(request.Name, request.Dev)
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "search app error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.Strings("name", request.Name),
				zap.Bool("dev", request.Dev),
			)
		}
		return
	}

	reply = &grpc_app.SearchReply{}
	if len(apps) != 0 {
		reply.Data = make([]*grpc_data.App, len(apps))
		for i := 0; i < len(apps); i++ {
			reply.Data[i] = apps[i].ConvertToPB()
		}
	}
	return
}

// Info 返回 app 下載 信息
func (s App) Info(ctx context.Context, request *grpc_app.InfoRequest) (reply *grpc_app.InfonReply, e error) {
	// 驗證 是否可下載
	var key string
	key, e = s.getAppkey(ctx)
	if e != nil {
		return
	}
	var mPackage manipulator.Package
	var yes bool
	yes, e = mPackage.IsAllow(key, request.Package)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "info app error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", request.Package),
				zap.String("dev", request.Version),
			)
		}
		return
	} else if !yes {
		e = grpc.Errorf(codes.PermissionDenied, "info app error")

		if ce := logger.Logger.Check(zap.WarnLevel, "interceptor error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	var mApp manipulator.App
	var size int64
	var description, hash string
	var dev bool
	description, dev, size, hash, e = mApp.DownloadInformation(request.Package, request.Version)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "info app error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Package),
				zap.String("dev", request.Version),
			)
		}
		return
	}

	reply = &grpc_app.InfonReply{
		Size:        size,
		Hash:        hash,
		Description: description,
		Dev:         dev,
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "info app"); ce != nil {
		ce.Write(
			zap.String("package", request.Package),
			zap.String("dev", request.Version),
		)
	}
	return
}

// Download 下載 app
func (s App) Download(stream grpc_app.App_DownloadServer) (e error) {
	var request *grpc_app.DownloadRequest
	request, e = stream.Recv()
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "grpc error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	pkg := strings.TrimSpace(request.Package)
	version := strings.TrimSpace(request.Version)
	// 驗證 是否可下載
	ctx := stream.Context()
	var key string
	key, e = s.getAppkey(ctx)
	if e != nil {
		return
	}
	var mPackage manipulator.Package
	var yes bool
	yes, e = mPackage.IsAllow(key, pkg)
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "download app error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", pkg),
				zap.String("version", version),
			)
		}
		return
	} else if !yes {
		e = grpc.Errorf(codes.PermissionDenied, "info app error")

		if ce := logger.Logger.Check(zap.WarnLevel, "interceptor error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	if request.Hash == nil || request.Offset == 0 {
		e = s.downloadV0_1_9(stream, pkg, version, key)
	} else {
		e = s.downloadV0_1_9after(stream, pkg, version, key,
			request.Offset, request.Hash,
		)
	}
	return
}
func (s App) downloadV0_1_9after(stream grpc_app.App_DownloadServer,
	pkg, version, key string,
	offset int64, hash []byte,
) (e error) {
	// 監聽 取消
	var cancel int32
	go func() {
		_, e := stream.Recv()
		if e != io.EOF && grpc.Code(e) != codes.Canceled {
			if ce := logger.Logger.Check(zap.ErrorLevel, "grpc"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
		}
		atomic.StoreInt32(&cancel, 1)
	}()

	// 打開 檔案
	var f *os.File
	defer func() {
		if f != nil {
			f.Close()
		}
	}()
	path := manipulator.VersionFilename(pkg, version)
	f, e = os.Open(path)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", pkg),
				zap.String("version", version),
			)
		}
		return
	}
	// 返回 檔案信息
	var info os.FileInfo
	info, e = f.Stat()
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", pkg),
				zap.String("version", version),
			)
		}
		return
	}
	size := info.Size()

	var changed bool
	if offset != 0 && hash != nil {
		if offset > size || len(hash) == 0 {
			changed = true
		} else {
			// 計算 hash
			m := md5.New()
			_, e = io.CopyN(m, f, offset)
			if e != nil {

				if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
					ce.Write(
						zap.Error(e),
						zap.String("name", pkg),
						zap.String("version", version),
					)
				}
				return
			}
			// 驗證 hash
			if bytes.Compare(hash, m.Sum(nil)) == 0 {
				if ce := logger.Logger.Check(zap.DebugLevel, "download hit"); ce != nil {
					ce.Write(
						zap.Error(e),
						zap.String("name", pkg),
						zap.String("version", version),
						zap.String("offset", utils.SizeString(offset)),
					)
				}
			} else {
				if ce := logger.Logger.Check(zap.DebugLevel, "download changed"); ce != nil {
					ce.Write(
						zap.Error(e),
						zap.String("name", pkg),
						zap.String("version", version),
						zap.String("offset", utils.SizeString(offset)),
					)
				}
				changed = true
				_, e = f.Seek(0, os.SEEK_SET)
				if e != nil {
					if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
						ce.Write(
							zap.Error(e),
							zap.String("name", pkg),
							zap.String("version", version),
						)
					}
					return
				}
			}
		}
	}
	// 驗證 取消
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel

		if ce := logger.Logger.Check(zap.WarnLevel, "download app cancel"); ce != nil {
			ce.Write(
				zap.String("package", pkg),
				zap.String("version", version),
			)
		}
		return
	}

	// 返回 檔案大小
	var reply grpc_app.DownloadReply
	reply.Code = grpc_app.DownloadReply_Info
	reply.Size = size
	e = stream.Send(&reply)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", pkg),
				zap.String("version", version),
			)
		}
		return
	}

	// 驗證 取消
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel

		if ce := logger.Logger.Check(zap.WarnLevel, "download app cancel"); ce != nil {
			ce.Write(
				zap.String("package", pkg),
				zap.String("version", version),
			)
		}
		return
	}
	// 緩存 不匹配 通知 重建檔案
	if changed {
		var reply grpc_app.DownloadReply
		reply.Code = grpc_app.DownloadReply_Changed
		e = stream.Send(&reply)
		if e != nil {

			if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.String("name", pkg),
					zap.String("version", version),
				)
			}
			return
		}

		// 驗證 取消
		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			if ce := logger.Logger.Check(zap.WarnLevel, "download app cancel"); ce != nil {
				ce.Write(
					zap.String("package", pkg),
					zap.String("version", version),
				)
			}
			return
		}
	}

	// 返回 檔案數據
	reply.Size = 0
	reply.Code = grpc_app.DownloadReply_Bytes
	b := make([]byte, 1024*16)
	var n int
	for {
		// 驗證 取消
		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			if ce := logger.Logger.Check(zap.WarnLevel, "download app cancel"); ce != nil {
				ce.Write(
					zap.String("package", pkg),
					zap.String("version", version),
				)
			}
			return
		}
		n, e = f.Read(b)
		if e != nil {
			if e == io.EOF {
				e = nil
				break
			}
			if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.String("name", pkg),
					zap.String("version", version),
				)
			}
			return
		}

		// 驗證 取消
		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			if ce := logger.Logger.Check(zap.WarnLevel, "download app cancel"); ce != nil {
				ce.Write(
					zap.String("package", pkg),
					zap.String("version", version),
				)
			}
			return
		}
		reply.Data = b[:n]
		e = stream.Send(&reply)
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "download app cancel"); ce != nil {
				ce.Write(
					zap.String("package", pkg),
					zap.String("version", version),
				)
			}
			return
		}
	}

	// 驗證 取消
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		if ce := logger.Logger.Check(zap.WarnLevel, "download app cancel"); ce != nil {
			ce.Write(
				zap.String("package", pkg),
				zap.String("version", version),
			)
		}
		return
	}
	// 通知 檔案 傳輸 完成
	reply.Data = nil
	reply.Code = grpc_app.DownloadReply_Completed
	e = stream.Send(&reply)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", pkg),
				zap.String("version", version),
			)
		}
		return
	}
	return
}
func (s App) downloadV0_1_9(stream grpc_app.App_DownloadServer,
	pkg, version, key string,
) (e error) {
	// 監聽 取消
	var cancel int32
	go func() {
		_, e := stream.Recv()
		if e != io.EOF && grpc.Code(e) != codes.Canceled {

			if ce := logger.Logger.Check(zap.ErrorLevel, "grpc"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
		}
		atomic.StoreInt32(&cancel, 1)
	}()
	// 開始 返回 下載 數據
	filename := manipulator.VersionFilename(pkg, version)
	var f *os.File
	f, e = os.Open(filename)
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", pkg),
				zap.String("version", version),
			)
		}
		return
	}
	defer f.Close()

	// 返回 檔案 大小
	var info os.FileInfo
	info, e = f.Stat()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "system"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", pkg),
				zap.String("version", version),
			)
		}
		return
	}
	e = stream.Send(&grpc_app.DownloadReply{
		Size: info.Size(),
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "grpc error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", pkg),
				zap.String("version", version),
			)
		}
		return
	}

	b := make([]byte, 1024*16)
	var n int
	for {
		// 驗證 取消
		if atomic.LoadInt32(&cancel) != 0 {
			e = errCancel
			if ce := logger.Logger.Check(zap.WarnLevel, "download app cancel"); ce != nil {
				ce.Write(
					zap.String("package", pkg),
					zap.String("version", version),
				)
			}
			return
		}

		// 讀取 檔案 數據
		n, e = f.Read(b)
		if e != nil {
			if e == io.EOF {
				break
			}

			if ce := logger.Logger.Check(zap.ErrorLevel, "system error"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		}

		// 傳輸 檔案 數據
		e = stream.Send(&grpc_app.DownloadReply{
			Data: b[:n],
		})
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "grpc error"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.String("name", pkg),
					zap.String("version", version),
				)
			}
			return
		}
	}
	// 通知 檔案 傳輸 完成
	if atomic.LoadInt32(&cancel) != 0 {
		e = errCancel
		if ce := logger.Logger.Check(zap.WarnLevel, "download app cancel"); ce != nil {
			ce.Write(
				zap.String("package", pkg),
				zap.String("version", version),
			)
		}
		return
	}
	e = stream.Send(&grpc_app.DownloadReply{
		Complete: true,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "grpc error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("name", pkg),
				zap.String("version", version),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "download app complete"); ce != nil {
		ce.Write(
			zap.String("package", pkg),
			zap.String("version", version),
			zap.String("keys", key),
		)
	}
	return
}

// Find 查找 服務器 存在的 套件
func (App) Find(ctx context.Context, request *grpc_app.FindRequest) (reply *grpc_app.FindReply, e error) {
	var mApp manipulator.App
	var pkgs []data.Package
	pkgs, e = mApp.FindPackage(request.Name)
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "find package error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.Strings("name", request.Name),
			)
		}
		return
	}

	reply = &grpc_app.FindReply{}
	if len(pkgs) != 0 {
		reply.Data = make([]*grpc_app.PackageInfo, len(pkgs))
		for i := 0; i < len(pkgs); i++ {
			reply.Data[i] = &grpc_app.PackageInfo{
				Name:        pkgs[i].Name,
				Description: pkgs[i].Description,
			}
		}
	}
	return
}

// FindEx  查找 服務器 存在的 套件 並且 包含 全部 信息
func (App) FindEx(ctx context.Context, request *grpc_app.FindExRequest) (reply *grpc_app.FindExReply, e error) {
	var mApp manipulator.App
	var pkgs []*data.PackageInfo
	pkgs, e = mApp.FindPackageEx(request.Name)
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "find package error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.Strings("name", request.Name),
			)
		}
		return
	}
	reply = &grpc_app.FindExReply{}
	if len(pkgs) != 0 {
		reply.Data = make([]*grpc_app.PackageInfoEx, len(pkgs))
		for i := 0; i < len(pkgs); i++ {
			node := &grpc_app.PackageInfoEx{
				Name:        pkgs[i].Name,
				Description: pkgs[i].Description,
			}
			reply.Data[i] = node

			vs := pkgs[i].Version
			if len(vs) != 0 {
				node.Data = make([]*grpc_app.VersionInfo, len(vs))
				for j := 0; j < len(vs); j++ {
					node.Data[j] = &grpc_app.VersionInfo{
						Name:        vs[j].Name,
						Description: vs[j].Description,
						Dev:         vs[j].Dev,
					}
				}
			}
		}
	}
	return
}

// Upgrade 返回 指定套件 的 升級 版本
func (App) Upgrade(ctx context.Context, request *grpc_app.UpgradeRequest) (reply *grpc_app.UpgradeReply, e error) {
	var mApp manipulator.App
	var dst *data.Version
	dst, e = mApp.Upgrade(request.Package, request.Version, request.Dev)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "upgrade package error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Package),
				zap.String("version", request.Version),
				zap.Bool("dev", request.Dev),
			)
		}
		return
	}

	reply = &grpc_app.UpgradeReply{}
	if dst != nil {

		filename := manipulator.VersionFilename(dst.Package, dst.Name)
		var info os.FileInfo
		info, e = os.Stat(filename)
		if e != nil {
			return
		} else if info.IsDir() {
			e = fmt.Errorf("app data not found [%v] [%v]", dst.Package, dst.Name)
			return
		}

		reply.Version = dst.Name
		reply.Dev = dst.Dev
		reply.Hash = dst.Hash
		reply.Size = info.Size()
	}
	return
}

var appPingReply grpc_app.PingReply

// Ping 測試 服務器 響應 速度
func (App) Ping(ctx context.Context, request *grpc_app.PingRequest) (reply *grpc_app.PingReply, e error) {
	reply = &appPingReply
	return
}
