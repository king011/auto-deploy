package server

import (
	"time"

	grpc_session "gitlab.com/king011/auto-deploy/protocol/session"
	"gitlab.com/king011/auto-deploy/server/configure"
	"gitlab.com/king011/auto-deploy/server/cookie"
	"gitlab.com/king011/auto-deploy/server/db/manipulator"
	"gitlab.com/king011/auto-deploy/server/logger"
	"go.uber.org/zap"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// Session 提供 用戶登入
type Session struct {
}

// Login 登入 創建新的 session
func (Session) Login(ctx context.Context, request *grpc_session.LoginRequest) (reply *grpc_session.LoginReply, e error) {
	if !configure.Single().Server.User {
		e = grpc.Errorf(codes.Unimplemented, "not enabled")
		if ce := logger.Logger.Check(zap.WarnLevel, "Login"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if request.User == "" {
		e = grpc.Errorf(codes.InvalidArgument, "user not defined")
		if ce := logger.Logger.Check(zap.WarnLevel, "Login"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", request.User),
			)
		}
		return
	}
	if request.Password == "" {
		e = grpc.Errorf(codes.InvalidArgument, "password not defined")
		if ce := logger.Logger.Check(zap.WarnLevel, "Login"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", request.User),
			)
		}
		return
	}
	var mUser manipulator.User
	bean, e := mUser.Login(request.User, request.Password)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Login"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", request.User),
			)
		}
		return
	}
	session := cookie.Session{
		Name: bean.Name,
		Root: bean.Root,
		Time: time.Now(),
	}
	str, e := session.Cookie()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Login Cookie"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", request.User),
			)
		}
		return
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "Login success"); ce != nil {
		ce.Write(
			zap.String("user", request.User),
		)
	}
	reply = &grpc_session.LoginReply{
		Cookie: str,
	}
	return
}

// Refresh 刷新 session 只能非管理員 用戶刷新
func (Session) Refresh(ctx context.Context, request *grpc_session.RefreshRequest) (reply *grpc_session.RefreshReply, e error) {
	if !configure.Single().Server.User {
		e = grpc.Errorf(codes.Unimplemented, "not enabled")
		if ce := logger.Logger.Check(zap.WarnLevel, "Refresh"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	session, e := getSession(ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "getSession"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if session.Root {
		if ce := logger.Logger.Check(zap.WarnLevel, "root not support Refresh"); ce != nil {
			ce.Write(
				zap.String("user", session.Name),
			)
		}
		return
	}
	str, e := session.Cookie()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Cookie"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", session.Name),
			)
		}
		return
	}
	reply = &grpc_session.RefreshReply{
		Cookie: str,
	}
	return
}

var sessionPasswordReply grpc_session.PasswordReply

// Password 修改密碼
func (Session) Password(ctx context.Context, request *grpc_session.PasswordRequest) (reply *grpc_session.PasswordReply, e error) {
	if !configure.Single().Server.User {
		e = grpc.Errorf(codes.Unimplemented, "not enabled")
		if ce := logger.Logger.Check(zap.WarnLevel, "Password"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	session, e := getSession(ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "getSession"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if request.Password == "" {
		e = grpc.Errorf(codes.InvalidArgument, "password not support empty")
		if ce := logger.Logger.Check(zap.WarnLevel, "getSession"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	var user string
	if request.User == "" {
		user = session.Name
	} else {
		if !session.Root {
			e = grpc.Errorf(codes.PermissionDenied, "not root")
			if ce := logger.Logger.Check(zap.WarnLevel, "password other"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.String("user", request.User),
				)
			}
			return
		}
		user = request.User
	}
	var mUser manipulator.User
	e = mUser.Password(user, request.Password)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "password error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", request.User),
				zap.String("name", user),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "password success"); ce != nil {
		ce.Write(
			zap.String("user", request.User),
			zap.String("name", user),
		)
	}
	reply = &sessionPasswordReply
	return
}
