package server

import (
	"gitlab.com/king011/auto-deploy/server/configure"
	"gitlab.com/king011/auto-deploy/server/cookie"
	"gitlab.com/king011/auto-deploy/server/db/manipulator"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

func getSession(ctx context.Context) (session *cookie.Session, e error) {
	session, e = cookie.FromContext(ctx)
	if e != nil {
		return
	}
	if session == nil {
		e = grpc.Errorf(codes.PermissionDenied, "session nil")
		return
	}
	return
}
func checkUserInit(ctx context.Context, pkg string) (name string, e error) {
	if !configure.Single().Server.User {
		return
	}
	session, e := cookie.FromContext(ctx)
	if e != nil {
		return
	}
	if session == nil {
		e = grpc.Errorf(codes.PermissionDenied, "session nil")
		return
	}
	name = session.Name
	return
}
func checkUser(ctx context.Context, pkg string) (name string, e error) {
	if !configure.Single().Server.User {
		return
	}
	session, e := cookie.FromContext(ctx)
	if e != nil {
		return
	}
	if session == nil {
		e = grpc.Errorf(codes.PermissionDenied, "session nil")
		return
	}
	if session.Root {
		name = session.Name
		return
	}
	var mPackage manipulator.Package
	ok, e := mPackage.IsUser(pkg, session.Name)
	if e != nil {
		return
	} else if !ok {
		e = grpc.Errorf(codes.PermissionDenied, "not package's user [%v]", session.Name)
		return
	}
	name = session.Name
	return
}
