package server

import (
	grpc_data "gitlab.com/king011/auto-deploy/protocol/data"
	grpc_package "gitlab.com/king011/auto-deploy/protocol/package"
	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/auto-deploy/server/db/manipulator"
	"gitlab.com/king011/auto-deploy/server/logger"
	"go.uber.org/zap"
	"golang.org/x/net/context"
)

// Package 提供 套件包 管理功能
type Package struct {
}

var packageInitReply grpc_package.InitReply

// Init 創建一個 新的 套件包
func (Package) Init(ctx context.Context, request *grpc_package.InitRequest) (reply *grpc_package.InitReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUserInit(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	e = mPackage.Init(request.Name, request.Description)
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "init package error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
				zap.String("description", request.Description),
			)
		}
		return
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "init package"); ce != nil {
		ce.Write(
			zap.String("user", user),
			zap.String("package", request.Name),
			zap.String("description", request.Description),
		)
	}
	reply = &packageInitReply
	return
}

var packageUpdateReply grpc_package.UpdateReply

// Update 更新 套件包 信息
func (Package) Update(ctx context.Context, request *grpc_package.UpdateRequest) (reply *grpc_package.UpdateReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	e = mPackage.Update(request.Name, request.Description)
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "update package error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
				zap.String("description", request.Description),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "update package"); ce != nil {
		ce.Write(
			zap.String("user", user),
			zap.String("package", request.Name),
			zap.String("description", request.Description),
		)
	}
	reply = &packageUpdateReply
	return
}

var packageRemoveReply grpc_package.RemoveReply

// Remove 刪除 套件包
func (Package) Remove(ctx context.Context, request *grpc_package.RemoveRequest) (reply *grpc_package.RemoveReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	e = mPackage.Remove(request.Name)
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "remove package error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Name),
				zap.String("user", user),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "remove package"); ce != nil {
		ce.Write(
			zap.String("package", request.Name),
			zap.String("user", user),
		)
	}
	reply = &packageRemoveReply
	return
}

// Get 返回 套件包 信息
func (Package) Get(ctx context.Context, request *grpc_package.GetRequest) (reply *grpc_package.GetReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	var bean *data.Package
	bean, e = mPackage.Get(request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "get package error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Name),
				zap.String("user", user),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.DebugLevel, "get package"); ce != nil {
		ce.Write(
			zap.String("package", request.Name),
			zap.String("user", user),
		)
	}
	reply = &grpc_package.GetReply{
		Data: bean.ConvertToPB(),
	}
	return
}

// Search 查找 套件包
func (Package) Search(ctx context.Context, request *grpc_package.SearchRequest) (reply *grpc_package.SearchReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	var beans []data.Package
	beans, e = mPackage.Search(request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "search package error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Name),
				zap.String("user", user),
			)
		}
		return
	}
	if ce := logger.Logger.Check(zap.DebugLevel, "search package"); ce != nil {
		ce.Write(
			zap.String("package", request.Name),
			zap.String("user", user),
		)
	}
	reply = &grpc_package.SearchReply{}
	if len(beans) != 0 {
		reply.Data = make([]*grpc_data.Package, len(beans))
		for i := 0; i < len(beans); i++ {
			reply.Data[i] = beans[i].ConvertToPB()
		}
	}
	return
}

var packageAddReply grpc_package.AddReply

// Add 增加 套件包 下載 key
func (Package) Add(ctx context.Context, request *grpc_package.AddRequest) (reply *grpc_package.AddReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	e = mPackage.Add(request.Name, request.Key, request.Description)
	if e != nil {

		if ce := logger.Logger.Check(zap.WarnLevel, "package add key error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Name),
				zap.String("user", user),
				zap.String("key", request.Key),
				zap.String("description", request.Description),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "package add key"); ce != nil {
		ce.Write(
			zap.String("package", request.Name),
			zap.String("user", user),
			zap.String("key", request.Key),
			zap.String("description", request.Description),
		)
	}
	reply = &packageAddReply
	return
}

var packageDeleteReply grpc_package.DeleteReply

// Delete 刪除 套件包 下載 key
func (Package) Delete(ctx context.Context, request *grpc_package.DeleteRequest) (reply *grpc_package.DeleteReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	e = mPackage.Delete(request.Name, request.Key)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "package delete key error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Name),
				zap.String("user", user),
				zap.String("key", request.Key),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "package delete key"); ce != nil {
		ce.Write(
			zap.String("package", request.Name),
			zap.String("user", user),
			zap.String("key", request.Key),
		)
	}
	reply = &packageDeleteReply
	return
}

// User 返回 套件所屬 用戶
func (Package) User(ctx context.Context, request *grpc_package.UserRequest) (reply *grpc_package.UserReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	bean, e := mPackage.Get(request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "package get users"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Name),
				zap.String("user", user),
			)
		}
		return
	}
	m := bean.GetUsers()
	reply = &grpc_package.UserReply{}
	if len(m) != 0 {
		reply.Data = make([]string, 0, len(m))
		for k := range m {
			reply.Data = append(reply.Data, k)
		}
	}
	return
}

var packageAddUserReply grpc_package.AddUserReply

// AddUser 爲 套件 添加 用戶
func (Package) AddUser(ctx context.Context, request *grpc_package.AddUserRequest) (reply *grpc_package.AddUserReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	e = mPackage.AddUser(request.Name, request.Data)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "package add users error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Name),
				zap.String("user", user),
				zap.Strings("data", request.Data),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "package add users"); ce != nil {
		ce.Write(
			zap.String("package", request.Name),
			zap.String("user", user),
			zap.Strings("data", request.Data),
		)
	}
	reply = &packageAddUserReply
	return
}

var packageRemoveUserReply grpc_package.RemoveUserReply

// RemoveUser 從 套件 刪除 用戶
func (Package) RemoveUser(ctx context.Context, request *grpc_package.RemoveUserRequest) (reply *grpc_package.RemoveUserReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	e = mPackage.RemoveUser(request.Name, request.Data)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "package remove users error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Name),
				zap.String("user", user),
				zap.Strings("data", request.Data),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "package remove users"); ce != nil {
		ce.Write(
			zap.String("package", request.Name),
			zap.String("user", user),
			zap.Strings("data", request.Data),
		)
	}
	reply = &packageRemoveUserReply
	return
}

var packageSetUserReply grpc_package.SetUserReply

// SetUser 爲 套件 設置 用戶
func (Package) SetUser(ctx context.Context, request *grpc_package.SetUserRequest) (reply *grpc_package.SetUserReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Name),
			)
		}
		return
	}

	var mPackage manipulator.Package
	e = mPackage.SetUser(request.Name, request.Data)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "package set users error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("package", request.Name),
				zap.String("user", user),
				zap.Strings("data", request.Data),
			)
		}
		return
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "package set users"); ce != nil {
		ce.Write(
			zap.String("package", request.Name),
			zap.String("user", user),
			zap.Strings("data", request.Data),
		)
	}
	reply = &packageSetUserReply
	return
}
