package server

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"os"
	"strings"

	grpc_data "gitlab.com/king011/auto-deploy/protocol/data"
	grpc_version "gitlab.com/king011/auto-deploy/protocol/version"
	"gitlab.com/king011/auto-deploy/server/db/data"
	"gitlab.com/king011/auto-deploy/server/db/manipulator"
	"gitlab.com/king011/auto-deploy/server/logger"
	"go.uber.org/zap"
	"golang.org/x/net/context"
)

// Version 提供 套件包 發佈版本 管理功能
type Version struct {
}

var versionPushReply grpc_version.PushReply

// Push 推送 一個 新版本 到服務器
// request 先發送一個 只有 版本信息的 數據 如果服務器 通過驗證 返回 reply
// 得到 reply 之後 使用 request 流的 Data 發送 多個 數據上傳 檔案 服務器 在所有操作 成功了 再 返回一個 reply 代表 成功
func (Version) Push(stream grpc_version.Version_PushServer) (e error) {
	// 獲取 版本 信息
	var request *grpc_version.PushRequest
	request, e = stream.Recv()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "grpc error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	// 驗證 包 修改權限
	user, e := checkUser(stream.Context(), request.Package)
	if e != nil {
		return
	}
	bean := &data.Version{
		Package:     request.Package,
		Name:        request.Name,
		Description: request.Description,
		Dev:         request.Dev,
	}
	e = bean.Format()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "version push error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
			)
		}
		return
	}
	// 驗證 版本號是否 可用
	var mVersion manipulator.Version
	e = mVersion.CheckAvailable(bean.Package, bean.Name)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "version push error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
			)
		}
		return
	}
	e = stream.Send(&versionPushReply)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "grpc error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
			)
		}
		return
	}

	// 接收 數據
	var filename string
	filename, e = manipulator.TempFile()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "TempFile error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
			)
		}
		return
	}
	var f *os.File
	f, e = os.Create(filename)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "create file error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("filename", filename),
				zap.String("user", user),
			)
		}
		return
	}
	hash := md5.New()
	for {
		request, e = stream.Recv()
		if e == io.EOF {
			f.Close()
			break
		} else if e != nil {
			f.Close()
			os.Remove(filename)
			if ce := logger.Logger.Check(zap.WarnLevel, "grpc error"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.String("user", user),
				)
			}
			return
		}
		if len(request.Data) != 0 {
			// 寫入 檔案
			_, e = f.Write(request.Data)
			if e != nil {
				f.Close()
				os.Remove(filename)

				if ce := logger.Logger.Check(zap.ErrorLevel, "write file error"); ce != nil {
					ce.Write(
						zap.Error(e),
						zap.String("filename", filename),
						zap.String("user", user),
					)
				}
			}
			// 計算 hash
			_, e = hash.Write(request.Data)
			if e != nil {
				f.Close()
				os.Remove(filename)
				if ce := logger.Logger.Check(zap.ErrorLevel, "write hash error"); ce != nil {
					ce.Write(
						zap.Error(e),
						zap.String("filename", filename),
						zap.String("user", user),
					)
				}

			}
		}
	}
	// 提交版本到 數據庫
	bean.Hash = hex.EncodeToString(hash.Sum(nil))
	e = mVersion.Push(bean, filename)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "version push error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
			)
		}
		os.Remove(filename)
		return
	}

	// 返回 成功
	e = stream.Send(&versionPushReply)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "grpc error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
			)
		}
		return
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "push new version"); ce != nil {
		ce.Write(
			zap.String("user", user),
			zap.String("package", bean.Package),
			zap.String("version", bean.Name),
			zap.String("description", bean.Description),
			zap.Bool("dev", bean.Dev),
		)
	}
	return
}

var versionUpdateReply grpc_version.UpdateReply

// Update 更新 版本 描述 信息
func (Version) Update(ctx context.Context, request *grpc_version.UpdateRequest) (reply *grpc_version.UpdateReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Package)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Package),
			)
		}
		return
	}

	var mVersion manipulator.Version
	e = mVersion.Update(strings.TrimSpace(request.Package),
		strings.TrimSpace(request.Name),
		strings.TrimSpace(request.Description),
		request.Dev,
	)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "update version error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Package),
				zap.String("version", request.Name),
				zap.String("description", request.Description),
				zap.Bool("dev", request.Dev),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "update version"); ce != nil {
		ce.Write(
			zap.String("user", user),
			zap.String("package", request.Package),
			zap.String("version", request.Name),
			zap.String("description", request.Description),
			zap.Bool("dev", request.Dev),
		)
	}
	reply = &versionUpdateReply
	return
}

// List 返回指定 套件的 所有 版本信息
func (Version) List(ctx context.Context, request *grpc_version.ListRequest) (reply *grpc_version.ListReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Package)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Package),
			)
		}
		return
	}

	var mVersion manipulator.Version
	var beans []data.Version
	beans, e = mVersion.List(strings.TrimSpace(request.Package))
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "list package version error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Package),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "list package version"); ce != nil {
		ce.Write(
			zap.String("user", user),
			zap.String("package", request.Package),
		)
	}

	reply = &grpc_version.ListReply{}
	n := len(beans)
	if n != 0 {
		reply.Data = make([]*grpc_data.Version, n)
		for i := 0; i < n; i++ {
			reply.Data[i] = beans[i].ConvertToPB()
		}
	}
	return
}

var versionRemoveReply grpc_version.RemoveReply

// Remove 刪除 套件 版本
func (Version) Remove(ctx context.Context, request *grpc_version.RemoveRequest) (reply *grpc_version.RemoveReply, e error) {
	// 驗證 包 修改權限
	user, e := checkUser(ctx, request.Package)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "checkUser"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Package),
			)
		}
		return
	}

	var mVersion manipulator.Version
	e = mVersion.Remove(request.Package, request.Version)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "remove package version error"); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", user),
				zap.String("package", request.Package),
				zap.String("version", request.Version),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, "remove package version error"); ce != nil {
		ce.Write(
			zap.String("user", user),
			zap.String("package", request.Package),
			zap.String("version", request.Version),
		)
	}
	reply = &versionRemoveReply
	return
}
