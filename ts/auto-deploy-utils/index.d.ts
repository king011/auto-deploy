declare module "auto-deploy-utils" {
    class Utils {
        /**
         *  if ture, test *.js
         */
        static readonly DEV: boolean;
        /**
         *  windows linux darwin freebsd
         */
        static readonly OS: string;
        /**
         *  386 amd64 arm
         */
        static readonly ARCH: string;

        /**
         *  返回 檔案 是否 存在
         * 
         * @param filename 檔案 路徑
         * 
         * @exception Error
         */
        static IsFileExist(filename: string): boolean;

        /**
         *  返回 檔案夾 是否 存在
         * 
         * @param filename 檔案夾 路徑
         * 
         * @exception Error
         */
        static IsDirExist(filename: string): boolean;

        /**
         *  返回 檔案/檔案夾 是否 存在
         * 
         * @param filename 檔案/檔案夾 路徑
         * 
         * @exception Error
         */
        static IsExist(filename: string): boolean;

        /**
         *  返回 filename 的最短 名稱
         * 
         * @param filename 檔案/檔案夾 路徑
         * 
         * @exception Error
         */
        static Clean(filename: string): string;

        /**
         *  返回 filename 是否是絕對路徑
         * 
         * @param filename 檔案/檔案夾 路徑
         * 
         * @exception Error
         */
        static IsAbs(filename: string): boolean;

        /**
         *  返回 絕對路徑 Abs 會在結果前 調用 Clean
         * 
         * @param filename 檔案/檔案夾 路徑
         * 
         * @exception Error
         */
        static Abs(filename: string): string;

        /**
         *  返回 路徑的 檔案夾名稱
         * 
         * @param filename 檔案/檔案夾 路徑
         * 
         * @exception Error
         */
        static Dir(filename: string): string;

        /**
         *  返回 路徑的 最後一個元素
         * 
         * @param filename 檔案/檔案夾 路徑
         * 
         * @exception Error
         */
        static Base(filename: string): string;

        /**
         *  返回 檔案 擴展名 .txt .sh
         * 
         * @param filename 檔案/檔案夾 路徑
         * 
         * @exception Error
         */
        static Ext(filename: string): string;

        /**
         *  創建 檔案夾
         * 
         * @param dirname 檔案夾 路徑
         * @param mode 可選參數 指定 默認權限
         * @param mkall 如果爲 true 則 自動創建 父目錄
         * 
         * @exception Error
         */
        static Mkdir(dirname: string, mode?: number, mkall?: boolean): void

        /**
         *  複製 檔案/檔案夾
         * 
         * @param dst 目標 路徑
         * @param src 源 路徑
         * @param all 是否 複製 子目錄
         * 
         * @exception Error
         */
        static Copy(dst: string, src: string, all?: boolean): void

        /**
         *  複製 檔案/檔案夾
         * 
         * @param name 要刪除的 檔案/檔案夾
         * @param all 爲ture 則 遞歸刪除 檔案夾
         * 
         * @exception Error
         */
        static Remove(name: string, all?: boolean): void

        /**
         *  加載 檔案 並轉化爲 字符串
         * 
         * @param filename 要加載的 檔案名稱
         * 
         * @exception Error
         */
        static LoadFile(filename: string): string

        /**
         *  保存 檔案
         * 
         * @param filename 要保存的 檔案名稱
         * @param data 要保存的 檔案數據
         * @param mode 可選的 檔案保存 權限
         * 
         * @exception Error
         */
        static SaveFile(filename: string, data: string, mode?: number): void

        /**
         *  將 jsonnet 字符串 編譯爲 json 字符串
         * 
         * @param src 源 jsonnet 字符串
         * 
         * @exception Error
         */
        static CompileJSONNET(src: string): string

        /**
         *  將 xml 字符串 編譯爲 json 字符串
         * 
         * @param src 源 xml 字符串
         * 
         * @exception Error
         */
        static CompileXML(src: string): string

        /**
         *  將 ini 字符串 編譯爲 json 字符串
         * 
         * @param src 源 ini 字符串
         * 
         * @exception Error
         */
        static CompileConf(src: string): string


        /**
         *  執行 指定進程並 等待其運行結束
         * 
         * @param src 源 jsonnet 字符串
         * 
         * @exception Error
         */
        static Exec(exec: string, ...args: string[]): void

        /**
        *  將 jsonnet 字符串 轉爲 js object
        * 
        * @param src 源 jsonnet 字符串
        * 
        * @exception Error
        */
        static ParseJSONNET(src: string): Object

        /**
         *  將 xml 字符串 轉爲 js object
         * 
         * @param src 源 xml 字符串
         * 
         * @exception Error
         */
        static ParseXML(src: string): Object

        /** 
         * 將 ini 字符串 轉爲 js object
         * 
         * @param src 源 ini 字符串
         * 
         * @exception Error
         * 
         * Section = {  
         * 	Comment:XXX:stirng,  
         * 	Item:{  
         * 		Keyname...:{  
         * 			Val:XXX:stirng,  
         * 			Comment:XXX:stirng,  
         * 		},  
         * 		...  
         * 	}  
         * }  
         * object = {  
         * 	main:Section,  
         * 	section:{  
         * 		sessionName...:Section,  
         * 		...  
         * 	}  
         * }  
         **/
        static ParseConf(src: string): Object

    }
}