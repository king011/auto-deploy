package duktape

/*
#include "duktape.h"
#include "api_node_module_c.h"
*/
import "C"
import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	xj "github.com/basgys/goxml2json"
	jsonnet "github.com/google/go-jsonnet"
	"gitlab.com/king011/auto-deploy/utils"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
)

//export _g_resolve_module
func _g_resolve_module(cBase, cID *C.char) *C.char {
	base := C.GoString(cBase)
	id := C.GoString(cID)

	// fmt.Println("resolve_module")
	// fmt.Println(base)
	// fmt.Println(id)

	filename := filepath.Clean(filepath.Dir(base) + "/" + id)
	info, e := os.Stat(filename)
	if e != nil || info.IsDir() {
		if filepath.Ext(filename) == "" {
			filename += ".js"
			info, e = os.Stat(filename)
			if e != nil || info.IsDir() {
				return nil
			}
		}
	}
	return C.CString(filename)
}

//export _g_load_module
func _g_load_module(cFile *C.char) *C.char {
	filename := C.GoString(cFile)
	//fmt.Println("load:", filename)
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		return nil
	}

	var bf, bd []byte
	bf, e = json.Marshal(filename)
	if e != nil {
		return nil
	}
	bd, e = json.Marshal(filepath.Dir(filename))
	if e != nil {
		return nil
	}
	str := fmt.Sprintf(`var __dirname=%s;var __filename=%s;(function(){%s})(this);`,
		bd, bf,
		b,
	)
	//fmt.Println("source:", str)
	return C.CString(str)
}

//export _g_f_IsFileExist
func _g_f_IsFileExist(cFile *C.char, cError **C.char) C.duk_bool_t {
	filename := C.GoString(cFile)
	info, e := os.Stat(filename)
	if e != nil {
		if !os.IsNotExist(e) {
			// 設置 異常
			str := e.Error()
			*cError = C.CString(str)
		}
		return (C.duk_bool_t)(0)
	}
	if info.IsDir() {
		return (C.duk_bool_t)(0)
	}
	return (C.duk_bool_t)(1)
}

//export _g_f_IsDirExist
func _g_f_IsDirExist(cFile *C.char, cError **C.char) C.duk_bool_t {
	filename := C.GoString(cFile)
	info, e := os.Stat(filename)
	if e != nil {
		if !os.IsNotExist(e) {
			// 設置 異常
			str := e.Error()
			*cError = C.CString(str)
		}
		return (C.duk_bool_t)(0)
	}
	if info.IsDir() {
		return (C.duk_bool_t)(1)
	}
	return (C.duk_bool_t)(0)
}

//export _g_f_IsExist
func _g_f_IsExist(cFile *C.char, cError **C.char) C.duk_bool_t {
	filename := C.GoString(cFile)
	_, e := os.Stat(filename)
	if e != nil {
		if !os.IsNotExist(e) {
			// 設置 異常
			str := e.Error()
			*cError = C.CString(str)
		}
		return (C.duk_bool_t)(0)
	}

	return (C.duk_bool_t)(1)
}

//export _g_f_Clean
func _g_f_Clean(cFile *C.char) *C.char {
	src := C.GoString(cFile)
	dst := filepath.Clean(src)
	if src == dst {
		return cFile
	}
	return C.CString(dst)
}

//export _g_f_IsAbs
func _g_f_IsAbs(cFile *C.char) C.duk_bool_t {
	filename := C.GoString(cFile)
	if filepath.IsAbs(filename) {
		return (C.duk_bool_t)(1)
	}
	return (C.duk_bool_t)(0)
}

//export _g_f_Abs
func _g_f_Abs(cFile *C.char, cError **C.char) *C.char {
	src := C.GoString(cFile)
	var dst string
	if filepath.IsAbs(src) {
		dst = filepath.Clean(src)
	} else {
		var e error
		dst, e = filepath.Abs(src)
		if e != nil {
			// 設置 異常
			str := e.Error()
			*cError = C.CString(str)
			return cFile
		}
	}

	if src == dst {
		return cFile
	}
	return C.CString(dst)
}

//export _g_f_Dir
func _g_f_Dir(cFile *C.char) *C.char {
	src := C.GoString(cFile)
	dst := filepath.Dir(src)
	if src == dst {
		return cFile
	}
	return C.CString(dst)
}

//export _g_f_Base
func _g_f_Base(cFile *C.char) *C.char {
	src := C.GoString(cFile)
	dst := filepath.Base(src)
	if src == dst {
		return cFile
	}
	return C.CString(dst)
}

//export _g_f_Ext
func _g_f_Ext(cFile *C.char) *C.char {
	src := C.GoString(cFile)
	dst := filepath.Ext(src)
	if src == dst {
		return cFile
	}
	return C.CString(dst)
}

//export _g_f_Mkdir
func _g_f_Mkdir(cFile *C.char, mode uint32, mkall C.duk_bool_t, cError **C.char) {
	var e error
	name := C.GoString(cFile)
	if mkall == 0 {
		e = os.Mkdir(name, os.FileMode(mode))
	} else {
		e = os.MkdirAll(name, os.FileMode(mode))
	}
	if e != nil {
		str := e.Error()
		*cError = C.CString(str)
	}
}

//export _g_f_Copy
// 複製 檔案/檔案夾
// 如果 all 爲ture 則 遞歸複製 檔案夾
func _g_f_Copy(cDst, cSrc *C.char, all C.duk_bool_t, cError **C.char) {
	src := C.GoString(cSrc)
	info, e := os.Stat(src)
	if e != nil {
		str := e.Error()
		*cError = C.CString(str)
		return
	}
	dst := C.GoString(cDst)
	if info.IsDir() {
		if all == 0 {
			e = os.Mkdir(dst, info.Mode())
		} else {
			e = utils.CopyDir(dst, src)
		}
	} else {
		e = utils.CopyFile(dst, src, info.Mode())
	}
	if e != nil {
		str := e.Error()
		*cError = C.CString(str)
	}
}

//export _g_f_Remove
// 刪除 檔案/檔案夾
// 如果 all 爲ture 則 遞歸刪除 檔案夾
func _g_f_Remove(cFile *C.char, all C.duk_bool_t, cError **C.char) {
	filename := C.GoString(cFile)
	var e error
	if all == 0 {
		e = os.Remove(filename)
	} else {
		e = os.RemoveAll(filename)
	}
	if e != nil {
		str := e.Error()
		*cError = C.CString(str)
	}
}

//export _g_f_SaveFile
// 保存 檔案
func _g_f_SaveFile(cFile, cData *C.char, mode uint32, cError **C.char) {
	filename := C.GoString(cFile)
	data := C.GoString(cData)
	e := ioutil.WriteFile(filename, []byte(data), os.FileMode(mode))
	if e != nil {
		str := e.Error()
		*cError = C.CString(str)
	}
}

//export _g_f_LoadFile
// 加載 檔案 並轉化爲 字符串
func _g_f_LoadFile(cFile *C.char, cError **C.char) *C.char {
	filename := C.GoString(cFile)
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		str := e.Error()
		*cError = C.CString(str)
		return nil
	} else if b == nil {
		return nil
	}
	str := string(b)
	return C.CString(str)
}

//export _g_f_CompileJSONNET
// 將 jsonnet 字符串 編譯爲 json 字符串
func _g_f_CompileJSONNET(cFile *C.char, cError **C.char) *C.char {
	str := C.GoString(cFile)
	vm := jsonnet.MakeVM()
	jsonStr, e := vm.EvaluateSnippet("", str)
	if e != nil {
		str := e.Error()
		*cError = C.CString(str)
		return nil
	}
	jsonStr = strings.TrimSpace(jsonStr)
	if jsonStr == str {
		return cFile
	}
	if jsonStr == "" {
		return nil
	}
	return C.CString(jsonStr)
}

//export _g_f_Exec
func _g_f_Exec(cName *C.char, cArgs **C.char, cCount C.int, cError **C.char) {
	name := C.GoString(cName)
	count := int(cCount)
	var args []string
	if count > 0 {
		args = make([]string, count)
		for i := 0; i < count; i++ {
			args[i] = C.GoString(
				C._cc_args_get_string(cArgs, (C.int)(i)),
			)
		}
	}
	var cmd *exec.Cmd
	if args == nil {
		cmd = exec.Command(name)
	} else {
		cmd = exec.Command(name, args...)
	}
	e := cmd.Run()
	if e != nil {
		str := e.Error()
		*cError = C.CString(str)
		return
	}
}

//export _g_f_CompileXML
// 將 xml 字符串 轉爲 js 字符串
func _g_f_CompileXML(cData *C.char, cError **C.char) *C.char {
	str := strings.TrimSpace(C.GoString(cData))
	if str == "" {
		return nil
	}
	r := bytes.NewBufferString(str)
	jsonStr, e := xj.Convert(r)
	if e != nil {
		str := e.Error()
		*cError = C.CString(str)
		return nil
	}
	return C.CString(jsonStr.String())
}

type _ConfItem struct {
	Key     string `json:"-"`
	Val     string
	Comment string
}
type _ConfSection struct {
	Name    string                `json:"-"`
	Items   map[string]*_ConfItem `json:"Item"`
	Comment string
}

func (s *_ConfSection) Insert(k, v, c string) {
	if s.Items == nil {
		s.Items = make(map[string]*_ConfItem)
	}
	s.Items[k] = &_ConfItem{
		Key:     k,
		Val:     v,
		Comment: c,
	}
}

type _ConfRoot struct {
	Main    *_ConfSection            `json:"main"`
	Section map[string]*_ConfSection `json:"section"`
}

var matchConfSection = regexp.MustCompile(`^\s*\[(\w+)\]\s*$`)
var matchConfItem = regexp.MustCompile(`^\s*([\.\-\w]+)\s*=(.*)$`)

//export _g_f_CompileConf
// 將 ini 字符串 轉爲 js 字符串
func _g_f_CompileConf(cData *C.char, cError **C.char) *C.char {
	str := strings.TrimSpace(C.GoString(cData))
	if str == "" {
		return nil
	}
	r := bufio.NewReader(bytes.NewBufferString(str))
	sections := make([]*_ConfSection, 1, 10)
	current := &_ConfSection{}
	sections[0] = current
	comment := ""
	keys := make(map[string]*_ConfSection)
	first := true
	var ok bool
	for {
		b, _, e := r.ReadLine()
		if e != nil {
			if e == io.EOF {
				break
			}
			*cError = C.CString(e.Error())
			return nil
		}
		str = string(b)
		// 新的 section
		if matchConfSection.MatchString(str) {
			if first {
				current.Comment = comment
				comment = ""
				first = false
			}
			name := matchConfSection.ReplaceAllString(str, "$1")
			current, ok = keys[name]
			if ok {
				// 已經存在 節 直接使用
				if comment != "" {
					current.Comment += comment
					comment = ""
				}
			} else {
				// 不存在 節 添加
				current = &_ConfSection{
					Name:    name,
					Comment: comment,
				}
				comment = ""
				sections = append(sections, current)
			}
			continue
		}
		// 新的 節點
		if matchConfItem.MatchString(str) {
			if first {
				current.Comment = comment
				comment = ""
				first = false
			}

			k := matchConfItem.ReplaceAllString(str, "$1")
			v := matchConfItem.ReplaceAllString(str, "$2")
			current.Insert(k, v, comment)
			comment = ""
			continue
		}

		// 註解
		comment += str + "\n"
	}

	root := _ConfRoot{
		Section: make(map[string]*_ConfSection),
	}

	for i := 0; i < len(sections); i++ {
		session := sections[i]
		if session.Name == "" {
			root.Main = session
		} else {
			root.Section[session.Name] = session
		}
	}
	//b, e := json.MarshalIndent(root, "", "\t")
	b, e := json.Marshal(root)
	if e != nil {
		*cError = C.CString(e.Error())
		return nil
	}
	return C.CString(string(b))
}
