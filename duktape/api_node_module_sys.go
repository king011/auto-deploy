package duktape

/*
#include "duktape.h"
#include "api_node_module_const.h"
extern duk_bool_t _g_f_IsFileExist(const char* filename,char** e);
extern duk_bool_t _g_f_IsDirExist(const char* filename,char** e);
extern duk_bool_t _g_f_IsExist(const char* filename,char** e);
extern char* _g_f_Clean(const char* filename);
extern duk_bool_t _g_f_IsAbs(const char* filename);
extern char* _g_f_Abs(const char* filename,char** e);
extern char* _g_f_Dir(const char* filename);
extern char* _g_f_Base(const char* filename);
extern char* _g_f_Ext(const char* filename);
extern void _g_f_Mkdir(const char* filename,duk_uint32_t mode,duk_bool_t mkall,char** e);
extern void _g_f_Copy(const char* dst,const char* src,duk_bool_t all,char** e);
extern void _g_f_Remove(const char* filename,duk_bool_t all,char** e);
extern char* _g_f_LoadFile(const char* filename,char** e);
extern void _g_f_SaveFile(const char* filename,const char* data,duk_uint32_t mode,char** e);
extern char* _g_f_CompileJSONNET(const char* data,char** e);
extern void _g_f_Exec(const char* name,const char** args,int count,char** e);
extern char* _g_f_CompileXML(const char* data,char** e);
extern char* _g_f_CompileConf(const char* data,char** e);
static duk_ret_t _c_native_IsFileExist(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "IsFileExist need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	char* e = NULL;
	duk_bool_t rs = _g_f_IsFileExist(filename,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	if(rs){
		duk_push_true(ctx);
	}else{
		duk_push_false(ctx);
	}
	return 1;
}
static duk_ret_t _c_native_IsDirExist(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "IsDirExist need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	char* e = NULL;
	duk_bool_t rs = _g_f_IsDirExist(filename,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	if(rs){
		duk_push_true(ctx);
	}else{
		duk_push_false(ctx);
	}
	return 1;
}
static duk_ret_t _c_native_IsExist(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "IsExist need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	char* e = NULL;
	duk_bool_t rs = _g_f_IsExist(filename,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	if(rs){
		duk_push_true(ctx);
	}else{
		duk_push_false(ctx);
	}
	return 1;
}
static duk_ret_t _c_native_Clean(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "Clean need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	char* dst = _g_f_Clean(filename);
	if(dst == filename){
		return 1;
	}
	duk_push_string(ctx,dst);
	free(dst);
	return 1;
}
static duk_ret_t _c_native_IsAbs(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "IsAbs need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	duk_bool_t rs = _g_f_IsAbs(filename);
	if(rs){
		duk_push_true(ctx);
	}else{
		duk_push_false(ctx);
	}
	return 1;
}
static duk_ret_t _c_native_Abs(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "Abs need filename");
	}
	char* e = NULL;
	const char* filename = duk_require_string(ctx,0);
	char* dst = _g_f_Abs(filename,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	if(dst == filename){
		return 1;
	}
	duk_push_string(ctx,dst);
	free(dst);
	return 1;
}
static duk_ret_t _c_native_Dir(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "Dir need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	char* dst = _g_f_Dir(filename);
	if(dst == filename){
		return 1;
	}
	duk_push_string(ctx,dst);
	free(dst);
	return 1;
}
static duk_ret_t _c_native_Base(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "Base need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	char* dst = _g_f_Base(filename);
	if(dst == filename){
		return 1;
	}
	duk_push_string(ctx,dst);
	free(dst);
	return 1;
}
static duk_ret_t _c_native_Ext(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "Ext need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	char* dst = _g_f_Ext(filename);
	if(dst == filename){
		return 1;
	}
	duk_push_string(ctx,dst);
	free(dst);
	return 1;
}
static duk_ret_t _c_native_Mkdir(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "Mkdir need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	duk_uint32_t mode = 0x800001ED;
	if(n>1 && duk_is_number(ctx,1)){
		mode = duk_to_uint32(ctx,1);
	}
	duk_bool_t mkall = 0;
	if(n>2 && duk_is_boolean(ctx,2)&& duk_get_boolean(ctx,2)){
		mkall = 1;
	}
	char* e = NULL;
	_g_f_Mkdir(filename,mode,mkall,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	return 0;
}
static duk_ret_t _c_native_Copy(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "Copy need dst filename");
	}
	if(n<2){
		duk_type_error(ctx, "Copy need src filename");
	}
	const char* dst = duk_require_string(ctx,0);
	const char* src = duk_require_string(ctx,1);
	duk_bool_t all = 0;
	if(n>2 && duk_is_boolean(ctx,2) && duk_get_boolean(ctx,2)){
		all = 1;
	}
	char* e = NULL;
	_g_f_Copy(dst,src,all,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	return 0;
}
static duk_ret_t _c_native_Remove(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "Remove need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	duk_bool_t all = 0;
	if(n>1 && duk_is_boolean(ctx,1) && duk_get_boolean(ctx,1)){
		all = 1;
	}
	char* e = NULL;
	_g_f_Remove(filename,all,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	return 0;
}
static duk_ret_t _c_native_LoadFile(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "LoadFile need filename");
	}
	const char* filename = duk_require_string(ctx,0);
	char* e = NULL;
	char* data = _g_f_LoadFile(filename,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
		return 0;
	}else if(!data){
		duk_push_string(ctx,"");
	}else{
		duk_push_string(ctx,data);
		free(data);
	}
	return 1;
}
static duk_ret_t _c_native_SaveFile(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "SaveFile need dst filename");
	}
	const char* filename = duk_require_string(ctx,0);
	const char* data = NULL;
	if(n>1 && duk_is_string(ctx,1)){
		data = duk_require_string(ctx,1);
	}
	duk_uint32_t mode = 0x1A4;
	if(n>1 && duk_is_number(ctx,1)){
		mode = duk_to_uint32(ctx,1);
	}
	char* e = NULL;
	_g_f_SaveFile(filename,data,mode,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	return 0;
}
static duk_ret_t _c_native_CompileJSONNET(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "CompileJSONNET need data");
	}
	char* e = NULL;
	const char* data = duk_require_string(ctx,0);
	char* dst = _g_f_CompileJSONNET(data,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	if(dst == data){
		return 1;
	}
	duk_push_string(ctx,dst);
	free(dst);
	return 1;
}
static duk_ret_t _c_native_ParseJSONNET(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "ParseJSONNET need data");
	}
	char* e = NULL;
	const char* data = duk_require_string(ctx,0);
	char* dst = _g_f_CompileJSONNET(data,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}

	if(dst == data){
		duk_json_decode(ctx,-1);
	}else{
		duk_push_string(ctx,dst);
		free(dst);
		duk_json_decode(ctx,-1);
	}
	return 1;
}

static duk_ret_t _c_native_Exec(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "Exec need command name");
	}
	char* e = NULL;
	const char* name = duk_require_string(ctx,0);
	const char** args = NULL;
	int count = 0;
	if(n>1){
		count = n-1;
		args = malloc(sizeof(char*)*count);
		for(int i=0;i<count;i++){
			args[i] = duk_safe_to_string(ctx,i+1);
		}
	}
	_g_f_Exec(name,args,count,&e);
	if(args!=NULL){
		free(args);
	}
	if(e!=NULL){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	return 0;
}
static duk_ret_t _c_native_ParseXML(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "ParseXML need data");
	}
	char* e = NULL;
	const char* data = duk_require_string(ctx,0);
	char* dst = _g_f_CompileXML(data,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	if(dst){
		duk_push_string(ctx,dst);
		free(dst);
		duk_json_decode(ctx,-1);
	}else{
		duk_push_null(ctx);
	}
	return 1;
}
static duk_ret_t _c_native_CompileXML(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "CompileXML need data");
	}
	char* e = NULL;
	const char* data = duk_require_string(ctx,0);
	char* dst = _g_f_CompileXML(data,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	if(dst){
		duk_push_string(ctx,dst);
		free(dst);
	}else{
		duk_push_null(ctx);
	}
	return 1;
}
static duk_ret_t _c_native_ParseConf(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "ParseConf need data");
	}
	char* e = NULL;
	const char* data = duk_require_string(ctx,0);
	char* dst = _g_f_CompileConf(data,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	if(dst){
		duk_push_string(ctx,dst);
		free(dst);
		duk_json_decode(ctx,-1);
	}else{
		duk_push_null(ctx);
	}
	return 1;
}
static duk_ret_t _c_native_CompileConf(duk_context *ctx) {
	// 驗證 參數
	duk_idx_t n = duk_get_top(ctx);
	if(n<1){
		duk_type_error(ctx, "CompileConf need data");
	}
	char* e = NULL;
	const char* data = duk_require_string(ctx,0);
	char* dst = _g_f_CompileConf(data,&e);
	if(e){
		duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", e);
		free(e);
		duk_throw(ctx);
	}
	if(dst){
		duk_push_string(ctx,dst);
		free(dst);
	}else{
		duk_push_null(ctx);
	}
	return 1;
}

static void _g_duk_module_node_init_sys(duk_context *ctx) {
	duk_push_c_function(ctx, _c_native_IsFileExist, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "IsFileExist");

	duk_push_c_function(ctx, _c_native_IsDirExist, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "IsDirExist");

	duk_push_c_function(ctx, _c_native_IsExist, DUK_VARARGS);
    duk_put_prop_string(ctx, -2, "IsExist");

	duk_push_c_function(ctx, _c_native_Clean, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "Clean");

	duk_push_c_function(ctx, _c_native_IsAbs, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "IsAbs");

	duk_push_c_function(ctx, _c_native_Abs, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "Abs");

	duk_push_c_function(ctx, _c_native_Dir, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "Dir");

	duk_push_c_function(ctx, _c_native_Base, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "Base");

	duk_push_c_function(ctx, _c_native_Ext, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "Ext");

	duk_push_c_function(ctx, _c_native_Mkdir, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "Mkdir");

	duk_push_c_function(ctx, _c_native_Copy, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "Copy");

	duk_push_c_function(ctx, _c_native_Remove, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "Remove");

	duk_push_c_function(ctx, _c_native_LoadFile, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "LoadFile");

	duk_push_c_function(ctx, _c_native_SaveFile, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "SaveFile");

	duk_push_c_function(ctx, _c_native_CompileJSONNET, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "CompileJSONNET");

	duk_push_c_function(ctx, _c_native_ParseJSONNET, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "ParseJSONNET");

	duk_push_c_function(ctx, _c_native_Exec, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "Exec");

	duk_push_c_function(ctx, _c_native_ParseXML, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "ParseXML");

	duk_push_c_function(ctx, _c_native_CompileXML, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "CompileXML");

	duk_push_c_function(ctx, _c_native_ParseConf, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "ParseConf");

	duk_push_c_function(ctx, _c_native_CompileConf, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "CompileConf");
}
*/
import "C"

// moduleNodeInitSys 初始化 擴展 模塊
func (c *Context) moduleNodeInitSys() {
	c.GetGlobalString(_ModuleNode)
	c.GetPropString(-1, _ModuleNodeMUtils)
	c.GetPropString(-1, "Utils")
	C._g_duk_module_node_init_sys(c.duk_context)
	c.Pop3()

}
