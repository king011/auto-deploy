package duktape

/*
#include "duktape.h"
#include "duk_module_node.h"
#include "api_node_module_const.h"
static duk_ret_t _c_cb_resolve_module(duk_context *ctx);
static duk_ret_t _c_cb_load_module(duk_context *ctx);
extern char* _g_resolve_module(const char*,const char*);
extern char* _g_load_module(const char*);
static void _g_duk_module_node_init(duk_context *ctx) {
	duk_push_object(ctx);
	duk_push_c_function(ctx, _c_cb_resolve_module, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "resolve");
	duk_push_c_function(ctx, _c_cb_load_module, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "load");
	duk_module_node_init(ctx);
}
// 返回 當前 腳本 路徑
static const char* _c_cb_resolve_filename(duk_context *ctx)
{
    duk_inspect_callstack_entry(ctx, -3);
    duk_get_prop_string(ctx, -1, "function");
    duk_get_prop_string(ctx, -1, "fileName");
    const char *name = duk_require_string(ctx, -1);
    duk_pop_3(ctx);
    return name;
}

static duk_ret_t _c_cb_resolve_module(duk_context *ctx) {
	const char* id = duk_require_string(ctx, 0);
	if(id == NULL || strlen(id) == 0){
		duk_type_error(ctx, "can't resolve module null");
	}

	if(strcmp(id,"auto-deploy-utils")==0){
		duk_push_string(ctx, "auto-deploy-utils");
		return 1;
	}else if(id[0] == '.'){
		// 加載 相對 位置 模塊
		const char* file = _c_cb_resolve_filename(ctx);
		char* resolved_id = _g_resolve_module(file,id);
		if(resolved_id==NULL){
			duk_type_error(ctx, "can't resolve module [%s]",id);
		}
		duk_push_string(ctx, resolved_id);
		free(resolved_id);
		return 1;
	}else if(id[0] == '/'){
		duk_get_global_string(ctx,ModuleNode);
		if(!duk_is_object(ctx,-1) || duk_is_array(ctx,-1)){
			duk_type_error(ctx, "can't resolve [%s], ModuleNode not init");
		}
		duk_get_prop_string(ctx,-1,ModuleNodeRoot);
		if(!duk_is_array(ctx,-1)){
			duk_type_error(ctx, "can't resolve module [%s],ModuleNode fault",id);
		}
		duk_uarridx_t n = (duk_uarridx_t)(duk_get_length(ctx,-1));
		for(duk_uarridx_t i=0;i<n;i++){
			duk_get_prop_index(ctx,-1,i);
			if(duk_is_string(ctx,-1)){
				const char* file = duk_require_string(ctx,-1);
				char* resolved_id = _g_resolve_module(file,id);
				if(resolved_id != NULL){
					duk_push_string(ctx, resolved_id);
					free(resolved_id);
					return 1;
				}
			}
			duk_pop(ctx);
		}
	}
	duk_type_error(ctx, "can't resolve module [%s]",id);
	return 0;
}
static duk_ret_t _c_cb_load_module(duk_context *ctx) {
	const char* filename = duk_require_string(ctx, 0);
	if(strcmp(filename,"auto-deploy-utils")==0){
		// 加載 全局 模塊
		duk_get_global_string(ctx,ModuleNode);
		if(!duk_is_object(ctx,-1) || duk_is_array(ctx,-1)){
			duk_type_error(ctx, "can't resolve [%s], ModuleNode not init");
		}
		// 泛湖 auto-deploy-utils 模塊
		duk_get_prop_string(ctx,-1,ModuleNodeMUtils);

		// 將 模塊 設置 到 exports
		duk_put_prop_string(ctx, -3, "exports");
		return 0;
	}
	char* source = _g_load_module(filename);
	if(source == NULL){
		duk_type_error(ctx, "can't load module [%s]",filename);
	}
	duk_push_string(ctx,source);
	free(source);
    return 1;
}
*/
import "C"
import (
	"path/filepath"
	"runtime"
)

const (
	_ModuleNode       = "gitlab.com/king011/auto-deploy-ModuleNode"
	_ModuleNodeRoot   = "root"
	_ModuleNodeMUtils = "m-utils"
)

func nativeModuleNode(ctx *Context) (_n int) {
	ctx.GetGlobalString(_ModuleNode)
	return 1
}

// ModuleNodeInit 初始化 nodejs 模塊
//
// root 全局 模板 加載 路徑
//
// dev 是否 調試模式
func (c *Context) ModuleNodeInit(root []string, dev bool) {
	// 已經 初始化
	if c.GetGlobalString(_ModuleNode) && c.IsObject(-1) {
		c.Pop()
		return
	}
	c.Pop()

	// c.PushGoFunction(nativeModuleNode)
	// c.PutGlobalString("ModuleNode")

	// 入棧 全局 模塊信息
	c.PushObject()
	{
		// 系統模塊路徑
		c.PushArray()
		keys := make(map[string]bool)
		for i := 0; i < len(root); i++ {
			path := root[i] + "/node_modules"
			if filepath.IsAbs(path) {
				path = filepath.Clean(path)
			} else {
				var e error
				path, e = filepath.Abs(path)
				if e != nil {
					continue
				}
			}
			if keys[path] {
				continue
			}

			keys[path] = true
			c.PushString(path)
			c.PutPropIndex(-2, uint(i))
		}
		c.PutPropString(-2, _ModuleNodeRoot)

		// 系統 屬性
		c.PushObject()
		c.PushObject()
		{
			c.PushBoolean(dev)
			c.PutPropString(-2, "DEV")

			c.PushString(runtime.GOOS)
			c.PutPropString(-2, "OS")

			c.PushString(runtime.GOARCH)
			c.PutPropString(-2, "ARCH")
		}
		c.PutPropString(-2, "Utils")
		c.PutPropString(-2, _ModuleNodeMUtils)
	}
	c.PutGlobalString(_ModuleNode)

	// 初始化 node 模塊
	C._g_duk_module_node_init(c.duk_context)
	c.moduleNodeInitSys()
}
