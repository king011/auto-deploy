package utils_test

import (
	"gitlab.com/king011/auto-deploy/utils"
	"testing"
)

func TestSizeString(t *testing.T) {
	var n int64
	n = 1024 * 10
	str := utils.SizeString(n)
	wnat := "10 k"
	if wnat != str {
		t.Fatalf("want %v, but %v", wnat, str)
	}

	n = utils.MB * 10
	str = utils.SizeString(n)
	wnat = "10 m"
	if wnat != str {
		t.Fatalf("want %v, but %v", wnat, str)
	}

	n = utils.GB * 10
	str = utils.SizeString(n)
	wnat = "10 g"
	if wnat != str {
		t.Fatalf("want %v, but %v", wnat, str)
	}
}
