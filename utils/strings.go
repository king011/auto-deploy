package utils

import (
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// ABS 如果 路徑不是 絕對路徑 以程式所在目錄 轉爲絕對路徑
func ABS(src string) string {
	src = strings.TrimSpace(src)
	if src == "" {
		var e error
		src, e = filepath.Abs(os.Args[0])
		if e != nil {
			log.Fatalln(e)
		}
	} else {
		if filepath.IsAbs(src) {
			src = filepath.Clean(src)
		} else {
			str, e := exec.LookPath(os.Args[0])
			if e != nil {
				log.Fatalln(e)
			}
			str, e = filepath.Abs(str)
			if e != nil {
				log.Fatalln(e)
			}
			src = filepath.Clean(filepath.Dir(str) + "/" + src)
		}
	}
	src = strings.Replace(src, "\\", "/", -1)
	return src
}
