package utils

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
)

const (
	// KB .
	KB = 1024
	// MB .
	MB = 1024 * KB
	// GB .
	GB = 1024 * MB
	// TB .
	TB = 1024 * GB
)

// ErrVersionName .
var ErrVersionName = errors.New("version must be v.X.X.X example v0.0.1 v1.0.0")

// ErrPackageEmpty .
var ErrPackageEmpty = errors.New("package can't be empty")
var matchVersionName = regexp.MustCompile(`^v\d+\.\d+\.\d+$`)

// CheckVersion .
func CheckVersion(v string) (e error) {
	if v == "" || v == "v0.0.0" {
		e = ErrVersionName
		return
	}
	if matchVersionName.MatchString(v) {
		return
	}
	e = ErrVersionName
	return
}

// SizeString 返回 數據大小 字符串
func SizeString(size int64) (str string) {
	if size < 1 {
		return
	}

	if size > TB {
		str += fmt.Sprintf("%vT", size/TB)
		size %= TB
	}
	if size > GB {
		str += fmt.Sprintf("%vG", size/GB)
		size %= GB
	}
	if size > MB {
		str += fmt.Sprintf("%vM", size/MB)
		size %= MB
	}
	if size > KB {
		str += fmt.Sprintf("%vK", size/KB)
		size %= KB
	}
	if size != 0 {
		str += fmt.Sprintf("%v", size)
	}

	return
}

// HashFile .
func HashFile(filename string) (hash string, e error) {
	var f *os.File
	f, e = os.Open(filename)
	if e != nil {
		if os.IsNotExist(e) {
			e = nil
		}
		return
	}
	defer f.Close()

	m := md5.New()
	_, e = io.Copy(m, f)
	if e != nil {
		return
	}
	hash = hex.EncodeToString(m.Sum(nil))
	return
}

// Rprintf \r Printf
func Rprintf(last int, format string, a ...interface{}) int {
	str := fmt.Sprintf(format, a...)
	if last != 0 {
		format := fmt.Sprintf("\r%%-%vs", last)
		fmt.Printf(format, "")
	}
	fmt.Printf("\r%s", str)
	last = len(str)
	return last
}

// Rprint \r Print
func Rprint(last int, a ...interface{}) int {
	str := fmt.Sprint(a...)
	if last != 0 {
		format := fmt.Sprintf("\r%%-%vs", last)
		fmt.Printf(format, "")
	}
	fmt.Printf("\r%s", str)
	last = len(str)
	return last
}

// CopyFile 複製 檔案
func CopyFile(dst, src string, mode os.FileMode) (e error) {
	var fr *os.File
	fr, e = os.Open(src)
	if e != nil {
		return
	}
	var fw *os.File
	fw, e = os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_TRUNC, mode)
	if e != nil {
		fr.Close()
		return
	}

	_, e = io.Copy(fw, fr)
	fr.Close()
	fw.Close()
	return
}

// CopyDir 遞歸 複製 檔案夾
func CopyDir(dst, src string) (e error) {
	// 驗證 源
	var fr *os.File
	fr, e = os.Open(src)
	if e != nil {
		return
	}
	defer fr.Close()
	var info os.FileInfo
	info, e = fr.Stat()
	if e != nil {
		return
	} else if !info.IsDir() {
		e = fmt.Errorf("%v is not a dir", src)
		return
	}

	// 創建 目標檔案夾
	e = os.Mkdir(dst, info.Mode())
	if e != nil {
		return
	}
	var infos []os.FileInfo
	infos, e = fr.Readdir(-1)
	// 複製 子檔案
	for i := 0; i < len(infos); i++ {
		if infos[i].IsDir() {
			continue
		}
		name := infos[i].Name()
		e = CopyFile(dst+"/"+name, src+"/"+name, infos[i].Mode())
		if e != nil {
			return
		}
	}

	// 複製 子 檔案夾
	for i := 0; i < len(infos); i++ {
		if !infos[i].IsDir() {
			continue
		}
		name := infos[i].Name()
		e = CopyDir(dst+"/"+name, src+"/"+name)
		if e != nil {
			return
		}
	}
	return
}

var _BasePath string

// BasePath 返回 可執行檔案 所在 檔案夾
func BasePath() string {
	if _BasePath != "" {
		return _BasePath
	}
	filename, e := exec.LookPath(os.Args[0])
	if e != nil {
		log.Fatalln(e)
	}

	filename, e = filepath.Abs(filename)
	if e != nil {
		log.Fatalln(e)
	}
	_BasePath = filepath.Dir(filename)
	return _BasePath
}
