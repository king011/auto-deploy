package utils

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"fmt"
	"gitlab.com/king011/king-go/os/fileperm"
	"io"
	"os"
	"path/filepath"
)

// UnpackTarGzipFile 返回 tar 中 指定檔案 的 數據
func UnpackTarGzipFile(src, name string) (b []byte, e error) {
	//打開tar
	var f *os.File
	f, e = os.Open(src)
	if e != nil {
		return
	}
	var gf *gzip.Reader
	defer func() {
		if gf != nil {
			gf.Close()
		}
		f.Close()
	}()

	gf, e = gzip.NewReader(f)
	if e != nil {
		return
	}

	r := tar.NewReader(gf)
	b, e = untarFindFile(r, name)
	return
}
func untarFindFile(r *tar.Reader, name string) (b []byte, e error) {
	for {
		var header *tar.Header
		header, e = r.Next()
		if e != nil {
			if e == io.EOF {
				e = nil
			}
			break
		}
		switch header.Typeflag {
		case tar.TypeReg:
			if header.Name == name {
				var buf bytes.Buffer
				_, e = io.Copy(&buf, r)
				if e != nil {
					return
				}
				b = buf.Bytes()
				return
			}
		}
	}
	e = fmt.Errorf("file [%s] not found in tar", name)
	return
}

// UnpackTarGzip 解開 tar 包
func UnpackTarGzip(src, dst string) (e error) {
	//打開tar
	var f *os.File
	f, e = os.Open(src)
	if e != nil {
		return
	}
	var gf *gzip.Reader
	defer func() {
		if gf != nil {
			gf.Close()
		}
		f.Close()
		if e != nil {
			os.RemoveAll(dst)
		}
	}()
	gf, e = gzip.NewReader(f)
	if e != nil {
		return
	}

	r := tar.NewReader(gf)
	e = untar(r, dst)
	return
}
func untar(r *tar.Reader, dst string) (e error) {
	for {
		var header *tar.Header
		header, e = r.Next()
		if e != nil {
			if e == io.EOF {
				e = nil
			}
			break
		}
		switch header.Typeflag {
		case tar.TypeDir:
			if e = os.MkdirAll(header.Name, os.FileMode(header.Mode)); e != nil {
				return
			}
		case tar.TypeReg:
			if e = untarFile(header, r, dst); e != nil {
				return
			}
		}
	}
	return
}
func untarFile(header *tar.Header, r *tar.Reader, dst string) (e error) {
	dst = dst + "/" + header.Name
	os.MkdirAll(filepath.Dir(dst), fileperm.Directory)
	w, e := os.OpenFile(dst, os.O_TRUNC|os.O_CREATE|os.O_RDWR, os.FileMode(header.Mode))
	if e != nil {
		return
	}
	_, e = io.Copy(w, r)
	w.Close()
	return
}
